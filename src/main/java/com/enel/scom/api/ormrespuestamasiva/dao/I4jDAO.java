package com.enel.scom.api.ormrespuestamasiva.dao;

import java.util.List;
import java.util.Map;

public interface I4jDAO {

	Map<String, Object> getDatosdeRegistro(String cNroOrdORM);

	void updateRegistroCabecera(String vCodOperacion, String vObservacion, String vCodErrASY000, Integer cOD_EST_RECEP,
			Integer cOD_EST_RECER, long vIdOrdTransfer);

	void updateRegistroDetalle(String vCodOperacion, long vIdOrdTransfer, int vNroRecepciones);

	Long getidMotNoeje(String pCodMotNoEje);

	Map<String, Object> bfnExisteOrden(String pOrden);

	int bfnExisteCuentaORM(Long pCuenta);

	int bfnExisteContratistaORM(String pCodContra);

	Map<String, Object> bfnExisteTipoDocum(String pTipoDocum);

	Map<String, Object> bfnExisteTarea(String pNroTarea);

	String bfnValidarIdentificador(String pNroTarea, String pIdentif);

	long getCodigoContratista(String cCodContra);

	long getCodigoEjecutor(String cNombreTec);

	long getCuenta(long lNroCuenta);

	void updateOrdOrdenSetLeidoAndFechaIngresoEstadoActual(long IdOrden);

	String getFechaVencimientoOrdOrden(long idOrden);

	String getCursorDatosFechaVencimiento(long idOrden);

	void updateOrdOrdenSetFechaVencimiento(String ccFechaVenc, long idOrden);

	void updateWFKWorkflowSetIdStateAndParentTypeAndIdEmpresa(Integer idEmpresa, long idWorkflow);

	void updateOrmOrdenSetIdContratistaAndIEjecutorAndAtendioSeguroAndIdParenNotificado(long lNronotif,
			long gIdContratista, long gIdEjecutor, long idOrden);

	long selectNextValSQAUDITEVENT();

	long selectNextValSQMAGNITUDIMPL();


	void insertFWK_AUDITEVENT(long id_audit_event, Integer gIdEmprGlob, long idOrden, Integer pIdUser);

	void insertORDHISTORICO(long id_audit_event, long idBuzon);

	void insertORDOBSERVACION(Integer pIDEmpr, String cObservacion, String gFechaEjecLarga, Integer pIdUser,
			long idOrden);

	long selectNextValSQRESPUESTAORDENMANTENIMIENTO();

	void insertORMRESPUESTA(long gSeqOrmIdResp, Integer pIDEmpr, long idOrden, Integer pIdUser, String gEjecuto,
			String gFechaEjecLarga, String ccFechaVenc, long lNroNotif, long gIdContratista, String cNombreTec,
			String cNombreCli, long cParent, long gIdServicio, int gIdTipDocPersona, String cNroDocum, String string,
			long idMotiNoEjecuc);

	long selectNextValSQINNERRAIZORDENMANTENIMIENTO();

	void insertORMINRAIZXML(long lid_raiz_xml, Integer pIDEmpr);

	void insertORMINTIPDOC(Integer pIDEmpr, String gTipDocCodInter);

	long selectNextValSQINNERPARENTESCO();

	void insertORMINPARENTESCO(long lid_parentesco, Integer pIDEmpr, long cParent);

	long selectNextValSQINNERCONTRATISTA();

	void insertORMINCONTRATISTA(long lid_contratista, Integer pIDEmpr, String cCodContra);

	long selectNextValSQINNERMOTIVONOEJECUCIONORDENM();

	void insertORMINMOTIVO(long lid_motivo, Integer pIDEmpr, String iMotiNoEjecuc);

	long selectNextValSQINNERRESPUESTAORDENMANTENIMI();

	void insertORMINRESP(long gSeqOrmInResp, Integer pIDEmpr, String gEjecuto, String gFechaEjecLarga, long lNroNotif,
			long lid_contratista, String cNombreTec, long lid_parentesco, String cNombreCli, int gIdTipDocPersona,
			String cNroDocum, String string, long lid_motivo);

	long selectNextValSQINNERSERVICIOASOCIADO();

	void insertORMINSERVICIO(long lid_servicio, Integer pIDEmpr, long gIdServicio);

	long selectNextValSQINNERORDENMANTENIMIENTO();

	void insertORMINORDEN(long gSeqOrmIdOrden, Integer pIDEmpr, String cNroOrden, long gSeqOrmInResp, long lid_servicio, long lid_raiz_xml);

	void insertORMINSERV(Integer pIDEmpr, String cObservacion, long gSeqOrmIdOrden);

	long selectCOUNT_ORMTAREAEJEC(long idTarea, long idOrden);

	void insertORMTAREAEJEC(Integer pIDEmpr, String cEjecutado, double dValor, int iCantidad, String cCambioMed,
			String cContraMed, String cCostoSis, String cCondTrabajo, long idTarea, long idOrden);

	void updateORMTAREAEJEC(String cEjecutado, double dValor, String cCostoSis, String cCambioMed, String cContraMed,
			String cCondTrabajo, int iCantidad, long idTarea, long idOrden);

	long selectNextValSQINNERCONDICIONTRABAJOORDENMA();

	void insertORMINCONDTRAB(long lid_cond_trabajo, Integer pIDEmpr, String cCondTrabajo);

	long selectNextValSQINNERTAREAORDENMANTENIMIENTO();

	void insertORMINTAREA(long lid_tarea, Integer pIDEmpr, String cNroTarea);

	void insertORMINTAREAEJE(Integer pIDEmpr, String cEjecutado, int iCantidad, long lid_tarea, long lid_cond_trabajo,
			long gSeqOrmIdOrden);

	int bfnValidarAIC(Long pIdOrden);

	long getSEGUSUARIOID(long idOrden);

	long getSEGRESPONSABLEID(long idUserRespEtapa);

	void updateORDORDDERIV(long idResponsable, long idOrden);

	void updateORDORDDENFINAL(long idOrden);

	void updateWKFWORKFLOWFINAL(long idWorkflow);

	void insertFWKAUDITEVENTFINAL(long id_audit_event, Integer gIdEmprGlob, long idOrden, Integer pIdUser);

	void insertORDHISTORICOFINAL(long lid_auditevent, long idBuzon);

	long bfnExisteMarcaMedidor2(String chMarcaMedidorInst);

	long bfnExisteModeloMedidor2(long pidMedMarca, String pCodModeloMed);

	Map<String, Object> selectDataInNroComponente(String chMedidorInst, long idMedModeloInst, Integer pIDEmpr);

	void insertORDMEDICAMBIO(long idComponenteInst, long idOrden, Integer pIDEmpr);

	long selectNextValSQINNERPROPIEDADCOMPONENTE();

	void insertORMINPROPCOMP(long lid_prop_comp, Integer pIDEmpr, String cCodPropMedInst);

	long selectNextValSQINNERORMMEDIDORINSTALACION();

	void insertORMINMEDINST(long lid_medidor_instalacion, Integer pIDEmpr, String chMedidorInst,
			String chModeloMedidorInst, String chMarcaMedidorInst, long gSeqOrmInResp, long lid_prop_comp);

	void insertORMINMEDIDA(Integer pIDEmpr, String vcCodigo, double vdValor, long lid_medidor_instalacion);

	int bfnExisteNumeroMedidor(String pNroMedidor, long pidMedModelo, Integer pIDEmpresa, long pIDServicio,
			Integer pOpc);

	Map<String, Object> selectDataInMedComponente(String chMedidorRet, long idMedModeloRet, Integer pIDEmpr,
			long gIdServicio);

	void insertORDMEDICAMBIO2(long idComponenteRet, long idOrden, Integer pIDEmpr);

	long selectNextValSQINNERORMMEDIDORRETIRO();

	void insertORMINMEDRETIRO(long gSeqOrmInMedRet, Integer pIDEmpr, String chMedidorRet, String chModeloMedidorRet,
			String chMarcaMedidorRet, long gSeqOrmInResp);

	void insertORMINMEDIDA2(Integer pIDEmpr, String vcCodigo, double vdValor, long gSeqOrmInMedRet);

	long getIdComponenteFromMedComponente(String pNroMedidor, long pidMedModelo, Integer pIDEmpresa, long pIDServicio);

	long getIdComponenteFromMedComponente(String pNroMedidor, long pidMedModelo, Integer pIDEmpresa);

	List<Map<String, Object>> selectCursorqLect2(long idTmpComponente);

	long extractIdEmpresaGlobal();

	long extractIdMotivoEstado();
	
	Map<String, Object> selectDataInMedComponente2(String cNroMedRet, long pidMedModelo, Integer pIDEmpr,
			long pIDServicio);

	long selectNextValSQDYNAMICBUSINESSOBJECT();

	void insertDYOOBJECT(long idObject, Integer pIDEmpr);

	void updateMEDCOMPONENTE(long idDynamicObject, Integer medPropiedadCliente, long id_cliente, long id_contratista,
			long idObject, String string2, long pidMedModelo, Integer pIDEmpr, long pIDServicio);

	long selectMAXMedHisComponete(long idComponente);

	void updateMEDHISPONENTE(long idHisComponente, long idOrden);

	void insertMEDHISCOMPONENTE(long idHisComp, long idComponente, long idPropiedad, Integer medPropiedadCliente,
			long id_cliente, long id_contratista, long idOrden);

	void insertFWKAUDITEVENT(long idHisComp, long idComponente, Integer pIdUser);

	long extractIdMedTipMagnitud();

	long extractIdFactEstMagnitud();
	
	Map<String, Object> selectDataInMoreTables(long idComponente, String vcCodigo);

	Map<String, Object> selectDataInNucCuentaServicio(long lNroCuenta);

	void updateSRV_Electrico(long vIdSecMagnitud, long xid_servicio);

	void insertMEDMAGNITUD(long idMagnitud, Integer pIDEmpr, long idComponente, long pIDServicio, long vIdMedida, double vdValor,
			double vIdFactor, long vIdTipMagnitud, String cFechaLectura, long vIdTipCalculo, long vIdEstMagnitud,
			long vIdTipo_cons, long vIdSecMagnitud, long vICantEnteros, long vICantDecimales);

	void deleteMEDLECULTIMA(long idComponente, long vIdMedida);

	void insertMEDLECULTIMA(long idLectura, Integer pIDEmpr, long vIdMedida, long idComponente);

	void updateSELSELLO(long idMotivoEstado, String cCodEjecutor, long idComponente);

	long extractIdServicioInstalado(long lNroCuenta);

	long extractIdMotivoEstadoInstalado();

	Map<String, Object> selectDataInMedComponenteInstalar(String cNroMedInst, long pidMedModelo, Integer pIDEmpr);

	void updateMEDCOMPONENTE_Instalar(long idDynamicObject, long lNroCuenta, String string, String string2,
			long idObject, String string3, long pidMedModelo);

	long selectMAXMedHisComponente_Inst(long idComponente);

	void updateMEDHISPONENTE_INST(String cFechaLectura, long idHisComponente, long idOrden);

	void insertMEDHISCOMPONENTE_INST(long idHisComp, long idComponente, String string, long idUbiServicio, long idOrden);

	void insertFWKAUDITEVENT_INST(long idHisComp, long idComponente, Integer pIdUser);

	long extractIdMedTipoMagnitud_INST();

	long extractIdFacEstMagnitud_INST();

	Map<String, Object> selectDataInSELCOLOR(String gColSello1);
	
	long extractIdSelSello(long idNroSello1);

	void updateSELSELLO(long idMotivoEstado,String cUbiSello1, long idUbiServicio, long idComponente, String cCodEjecutor,
			long idNroSello1, long idBolsaSello1);

	void insertSELHISSELLO(Integer pIDEmpr, long idBolsaSello1, String cDesSello1, long idNroSello1,
			String cCodContratista, String string, String cUbiSello1, long lNroCuenta, String string2, String string3,
			 long idColor1);

	int selectCOUNTSELSELLO(long idBolsaSello1);

	void updateSELBOLSASELLOS(String cCodEjecutor, long idBolsaSello1);

	void insertSELHISSELLO2(Integer pIDEmpr, long idBolsaSello2, String cDesSello2, long idNroSello2,
			String cCodContratista, String gCodEjecutor, String cUbiSello2, long lNroCuenta, String gCodMarMedInst, String gCodModMedInst,
			long idColor2);

	int bfnExisteCodigoLectura(String pCodigo, Integer pIDEmpr);

	int bfnExisteCuenta(Integer pIDEmpr, Long pCuenta);

	int bfnCuentaEnFacturacion(Long pCuenta);

	Map<String, Object> bfnTieneServicioElectrico(Long pCuenta);

	int bfnSE_VerificaVenta(Long pCuenta);

	int verifyVentaPorModificaciónEjecutado(Long pCuenta);

	int bfnExisteContratista(String pCodContra, Integer pIDEmpresa);

	int bfnExisteEjecutor(String pCodEjecutor, Integer pIDEmpr);

	String carga_FechaHora1();

	String carga_FechaHora2();

	String bfnObtenerFechaEjecucionVta(Long pCuenta);

	int bfnExisteCodigoPropiedadMedidor(String pCodPropMed);

	int bfnExisteNroSello(Long pNroSello);

	int bfnExisteColorSello(String pColorSello);

	int bfnExisteUbicacionSello(String pUbiSello);

	long getIdCliente(Long lNroCuenta);

	long getIdContratista(String gCodContratista);

	  
    //INICIO - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA
 	List<Map<String, Object>> selectRegistroMedidores(String nro_orden_legacy);

 	void updateRegistroMedidores(String medidorXml, String modeloXml, String marcaXml, String factorXml);
 	// FIN - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA

 	String selectMedidorEncontrado(String v_modelo, String v_marca, String v_componente);

}
