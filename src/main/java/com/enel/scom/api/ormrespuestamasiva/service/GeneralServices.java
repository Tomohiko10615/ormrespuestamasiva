package com.enel.scom.api.ormrespuestamasiva.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.ormrespuestamasiva.util.Others;
import com.enel.scom.api.ormrespuestamasiva.dao.IOthersDAO;



@Service
public class GeneralServices {

	 @Autowired
	 IOthersDAO othersDAO;
	 
	private static final Logger logger = LoggerFactory.getLogger(GeneralServices.class);

	 
	public String carga_FechaHora(int opc) {
		String cadena="";
		switch (opc) {
			case 1:
				cadena=othersDAO.carga_FechaHora1();
            break;
	    	case 2:
	    		cadena=othersDAO.carga_FechaHora2();
            break;
		default: cadena="";
			break;
		}
		return cadena;
	}

	public boolean bfnObtenerEmpresa(String gc_Empresa) {
		try {
			Others.gIDEmpresa=othersDAO.bfnVerificaEmpresa(gc_Empresa);
			 // Empresa Global
			Others.gIdEmprGlob=othersDAO.bfnVerificaEmpresaGlobal();
			return true;
		} catch (Exception e) {
			logger.info("Error al ejecutar GeneralServices.bfnObtenerEmpresa:{}",e);
			return false;
		}
	}

	public boolean bfnObtenerIdUsuario(String gcUser) {
		try {
			Others.gIdUser=othersDAO.bfnObtenerIdUsuario(gcUser);
			return true;
		} catch (Exception e) {
			logger.info("Error al ejecutar GeneralServices.bfnObtenerIdUsuario:{}",e);
			return false;
		}
	}

	public boolean inicializarVariables() {
		try {
			/*** Valor_Num correspondiente a Estado “Pendiente de Recepción” ***/
			Others.COD_EST_PRECE=othersDAO.getCODESTPRECE();
			
			
			/***  Valor_Num correspondiente a Estado “Recepcionado”  ***/
			Others.COD_EST_RECEP=othersDAO.getCODESTRECEP();
			
			
			/***  Valor_Num correspondiente a Estado “Recepcionado con Error”  ***/
			Others.COD_EST_RECER=othersDAO.getCODESTRECER();
			
			 //Cargar Codigos de Error
			if(!bfnObtenerErrores("ASY000")){
				  logger.info("Error en fnObtenerErrores(ASY000...");  
			      return false;
			 }
			if(!bfnObtenerErrores("ASY002")){
				  logger.info("Error en fnObtenerErrores(ASY002...");  
			      return false;
			 }
			if(!bfnObtenerErrores("ASY099")){
				  logger.info("Error en fnObtenerErrores(ASY099...");  
			      return false;
			 }
			
			return true;
		} catch (Exception e) {
			logger.info("Error al ejecutar GeneralServices.bfnObtenerIdUsuario:{}",e);
			return false;
		}
	}

	private boolean bfnObtenerErrores(String pkey) {
		try {
			Map<String, String> error = new HashMap<>();
			if(pkey.equalsIgnoreCase("ASY000")) {
				error=othersDAO.bfnObtenerErrores(pkey);
				
				Others.vCodErrASY000=error.get("VALOR_ALF");
				Others.vDesErrASY000=error.get("DESCRIPCION");
			}
			if(pkey.equalsIgnoreCase("ASY002")) {
				error=othersDAO.bfnObtenerErrores(pkey);
				
				Others.vCodErrASY002=error.get("VALOR_ALF");
				Others.vDesErrASY002=error.get("DESCRIPCION");
			}
			if(pkey.equalsIgnoreCase("ASY099")) {
				error=othersDAO.bfnObtenerErrores(pkey);
				Others.vCodErrASY099=error.get("VALOR_ALF");
				Others.vDesErrASY099=error.get("DESCRIPCION");
				
			}
			return true;
		} catch (Exception e) {
			logger.info("Error al ejecutar GeneralServices.bfnObtenerErrores:{}",e);
			return false;
		}
	}

	public boolean bfnObtenerPath(String pKey, Integer gIDEmpresa) {
		try {
			if(pKey.equalsIgnoreCase("ORM_REPORTES")) {
				Others.cPathSalida=othersDAO.bfnObtenerPath(pKey,gIDEmpresa);
			}
			if(pKey.equalsIgnoreCase("ORM_REPORTES_IN")) {
				Others.cPathEntrada=othersDAO.bfnObtenerPath(pKey,gIDEmpresa);
			}
			return true;
		} catch (Exception e) {
			logger.info("Error al ejecutar GeneralServices.bfnObtenerPath:{}",e);
			return false;
		}
	}
	
	public boolean bfnCargaGrupos(String gcLinea) {
		Others.linRespuesta="";
		Others.linPartidas="";
		Others.linMedidor="";
		Others.linContraste="";
		String cCadena="";
		String cGrupo="";
		int  iFlag=0;
		Others.cMnsjErr="";
		cCadena=gcLinea;
        String[] cadena_spliteada = cCadena.split("#");
        	for (int i=0; i<cadena_spliteada.length; i++) {
        		if(i==0) { //Para cargar la variable linRespuesta
        			cGrupo="Respuesta";
        			/*if(cadena_spliteada[i].equals("null")) {
        				iFlag=1;
        				break;
        			}*/
        			Others.linRespuesta=cadena_spliteada[i];
        		}
        		if(i==1) { //Para cargar las variables linPartidas
        			cGrupo="Partidas";
        			/*if(cadena_spliteada[i].equals("null")) {
        				iFlag=1;
        				break;
        			}*/
        			Others.linPartidas=cadena_spliteada[i];
        		}
        		if(i==2) { //Para cargar las variables linMedidor
        			cGrupo="Cambio de Medidor";
        			/*if(cadena_spliteada[i].equals("null")) {
        				iFlag=1;
        				break;
        			}*/
        			Others.linMedidor=cadena_spliteada[i];
        		
        		}
        		if(i==3) { //Para cargar las variables linContraste
        			cGrupo="Contraste de Medidor";
        			/*if(cadena_spliteada[i].equals("null")) {
        				cGrupo="Contraste de Medidor";
        				iFlag=1;
        				break;
        			}*/
        			Others.linContraste=cadena_spliteada[i];
        		}
        	}	
		
		if(iFlag==1) {
			Others.printErrorinFile1.printf("%s\tLínea [%d], no se pudo cargar el grupo [%s] desde el registro de entrada.\n",gcLinea, Others.glNroLinea, cGrupo);
			Others.cMnsjErr=String.format("No se pudo cargar el grupo [%s] desde el registro de entrada.", cGrupo);
			return false;
		}
		if(!bfnHayValor(Others.linRespuesta)) {
			Others.printErrorinFile1.printf("%s\tLínea [%d], el grupo de datos 'Respuesta' es de ingreso obligatorio.\n",gcLinea, Others.glNroLinea);
			Others.cMnsjErr="El grupo de datos 'Respuesta' es de ingreso obligatorio.";
			return false;
		}
		
		return true;
	}
	
	public boolean bfnHayValor(String pVar) {
		if(pVar.equalsIgnoreCase("") || pVar.length()<1) 
		{
			return false;
		}
		return true;
	}
}
