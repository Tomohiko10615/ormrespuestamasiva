package com.enel.scom.api.ormrespuestamasiva.util;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public final class Others {
	
	
	public static String gc_Empresa;
	public static Integer gIDEmpresa;
	public static String gcArchivo;
	public static String gcODT;
	public static String gcUser;
	public static Integer gIdUser;
	
	public static String cMnsjErr;
	public static String cMnsjErrObs;
	public static String cNroOrdTmp;
	public static String gcOrigen;
	
	public static int lRegProcesados;
	public static int lRegActualizados;
	public static int lRegRechazados;
	
	public static String gFechaHora;
	public static Integer gIdEmprGlob;
	
	public static Integer COD_EST_PRECE;
	public static Integer COD_EST_RECEP;
	public static Integer COD_EST_RECER;
	
	public static String vCodErrASY000;
	public static String vDesErrASY000;
	public static String vCodErrASY002;
	public static String vDesErrASY002;
	public static String vCodErrASY099;
	public static String vDesErrASY099;
	
	public static String cPathSalida;
	public static String cPathEntrada;
	public static String cArchEntrada;
	public static String cArchErrores;
	public static String cArchResumen;
	
	public static FileWriter fileErrorWriter;
	public static PrintWriter printErrorinFile1;
	public static FileWriter fileResumenWriter;
	public static PrintWriter printResumeninFile2;
	public static BufferedReader fArchivo;
	
	
	public static String linRespuesta;
	public static String linPartidas;
	public static String linMedidor;
	public static String linContraste;
	public static String linMediFull;
	
	public static int glNroLinea;
	public static String gcLinea;
	public static String vObservacion;
	public static String vCodOperacion;
	
	public static int iContTarea;
	public static int iContReti;
	public static int iContInst;
	public static Timestamp dEjecuc;
	public static Timestamp dActual;
	public static int iFlagSaleTar;
	public static int iCantMedIndvRet;
	public static int iCantMedIndvInst;

	public static Map<String, Object> stRptaMasiva;
	public static Map<String, Object> stRptaMasivaMedidores;
	public static List<Map<String, Object>> stTarea;
	public static Map<String, Object> stMedidor;
	
	
	public static String stTitMedidor[] = {
			" ",
			"Código de Contratista",
			"Código de Ejecutor",
			"Número de Cuenta",
			"Fecha de Lectura",
			"Código Marca Medidor a Retirar",
			"Código Modelo Medidor a Retirar",
			"Número Medidor a Retirar",
			"Lecturas Medidor a Retirar",
			"Código Marca Medidor a Instalar",
			"Código Modelo Medidor a Instalar",
			"Número Medidor a Instalar",
			"Lecturas Medidor a Instalar",
			"Código de Propiedad Medidor a Instalar",
			"Número de Sello 1",
			"Color Sello 1",
			"Ubicación Sello 1",
			"Número de Sello 2",
			"Color Sello 2",
			"Ubicación Sello 2"};
	
	public static String stTitRespuesta[] = {
			" ",
			"Número de Orden",
			"Número de Cuenta",
			"Número de Notificación",
			"Código Contratista",
			"Nombre del Técnico",
			"Fecha de Ejecución ",
			"Hora",
			"Minutos",
			"Nombre del Cliente",
			"Código Tipo Documento",
			"Número Documento",
			"Parentesco",
			"OT Ejecutada",
			"Motivo No Ejecución",
			"Observación"};
	
	public static String stTitPartidas[] = {
			" ",
			"Número de Tarea",
			"Identificador",
			"Cantidad",
			"Ejecutado"};
	
	public static String stTitContraste[] = {
			" ",
			"Medidor",
			"Modelo Medidor",
			"Marca Medidor",
			"Clase",
			"Cinco in_ens1",
			"Cinco in_ens2",
			"Cinco in_ens3",
			"Cien in_ens1",
			"Cien in_ens2",
			"Cien in_ens3",
			"Imax ens1",
			"Imax ens2",
			"Imax ens3",
			"Giro Vacío",
			"Aisla R",
			"Aisla S",
			"Aisla T",
			"Estado Disco",
			"Genera Cambio"};
	
	public static String cIdentActivo;
	public static int gIdTipDocPersona;
	public static String gTipDocCodInter;
	
	public static long gIdContratista;
	public static long gIdEjecutor;
	public static long gIdServicio;
	public static String gEjecuto;
	public static String gFechaEjecLarga;
	public static int gFlagTarNoEjecu;
	public static long gSeqOrmIdOrden;
	public static String cMnsj;
	
	public static List<Map<String, Object>> ArrLecturaRet;
	
	public static List<Map<String, Object>> ArrLecturaInst;
	public static long gSeqOrmInResp;
	public static String cSE_Estado_wkf;
	public static long lSE_IDEstado;
	public static int idSE_IDServicioRet;
	public static String cFechaEjecuVta;

	// public static String gcMensjFacturacion; /* ANL_20220920 */
	// public static int giFlagFacturacion; /* ANL_20220920 */
	
}
