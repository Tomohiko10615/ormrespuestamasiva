package com.enel.scom.api.ormrespuestamasiva.config;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class SynergiaDataSourceJDBCConfiguration {

	public static final BeanDataSource beanConn = DataSourceConfig.getConnectionDatos("ORCL");
	
/*	@Bean(name = "oracleDatasource")
	@ConfigurationProperties(prefix = "oracle.datasource")
	public DataSource oracleDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "oracleJdbcTemplate")
	public JdbcTemplate jdbcTemplate(@Qualifier("oracleDb") DataSource dsOracle) {
		return new JdbcTemplate(dsOracle);	
	} */

	
	@Bean(name = "orclDataSource") 
	@Primary
	public DataSource postgresDataSource() {
		
		HikariConfig dataSourceBuilder = new HikariConfig();
        dataSourceBuilder.setDriverClassName(beanConn.getDbDriver());
        dataSourceBuilder.setJdbcUrl(beanConn.getDbURL());
        dataSourceBuilder.setUsername(beanConn.getDbUser());
        dataSourceBuilder.setPassword(beanConn.getDbPass());        
        dataSourceBuilder.setMaximumPoolSize(beanConn.getDbMax());
        dataSourceBuilder.setMinimumIdle(beanConn.getDbMin());
        
        HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
        return dataSource;
	}
	
	@Autowired
	DataSourceConfig dataSourcePost;

	@Bean(name = "orclTemplate")
	@DependsOn("orclDataSource")
	public JdbcTemplate jdbcTemplate(@Qualifier("connORCL")Connection conn) throws SQLException {
		conn.setAutoCommit(false);
		return new JdbcTemplate(new SingleConnectionDataSource(conn, true));
	}
			
	@Bean(name = "connORCL")
	Connection registrarConnectionORCL(@Qualifier("orclDataSource") DataSource orclDataSource) throws SQLException {
		return orclDataSource.getConnection();
	}

	
}
