package com.enel.scom.api.ormrespuestamasiva.dao;

import java.util.Map;

public interface IOthersDAO {

	
	Integer bfnVerificaEmpresa(String gc_Empresa);
	
	Integer bfnVerificaEmpresaGlobal();

	Integer bfnObtenerIdUsuario(String gcUser);

	Integer getCODESTPRECE();

	Integer getCODESTRECEP();

	Integer getCODESTRECER();

	Map<String, String> bfnObtenerErrores(String pkey);

	String bfnObtenerPath(String pKey, Integer gIDEmpresa);

	String carga_FechaHora1();

	String carga_FechaHora2();

}
