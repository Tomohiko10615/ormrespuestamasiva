package com.enel.scom.api.ormrespuestamasiva.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.scom.api.ormrespuestamasiva.util.Others;
import com.enel.scom.api.ormrespuestamasiva.dao.I4jDAO;
import com.enel.scom.api.ormrespuestamasiva.dao.ISCOMDAO;
import com.enel.scom.api.ormrespuestamasiva.util.Constants;
import com.enel.scom.api.ormrespuestamasiva.util.MedidoresXML;

@Service
public class S4jServices {

//	 @Autowired
//	 @Qualifier("connORCL")
//	 Connection conn;
	@Qualifier("connPGS") // AGREGADO JAAG INDEPE-5569 09/06/2023
	Connection connPg; // AGREGADO JAAG INDEPE-5569 09/06/2023

	@Autowired
	I4jDAO i4jdao;

	// R.I. Correctivo incidencia INC000110799119 19/07/2023 INICIO
	@Autowired
	ISCOMDAO scomdao;
	// R.I. Correctivo incidencia INC000110799119 19/07/2023 FIN

	private static final Logger logger = LoggerFactory.getLogger(S4jServices.class);

	public void guardarTransferenciaOrden(String pnum_orden, String pError) throws SQLException {
		// connPg.setAutoCommit(false); // AGREGADO JAAG INDEPE-5569 09/06/2023
		long vIdOrdTransfer = 0;
		int vNroRecepciones = 0;
		String vCodTipoTDC = "";
		String cNroOrdORM = "";
		String cError = "";

		if (Others.gcOrigen.equalsIgnoreCase("EORDER")) {
			cNroOrdORM = pnum_orden;
			cError = pError;

			if (cError.length() > 0) {
				setErrGen(cError);
				logger.error(String.format("Orden nro[%s] - [%s]  \n", cNroOrdORM, Others.vObservacion));
			} else {
				Others.vCodOperacion = Others.vCodErrASY000;
				Others.vObservacion = "";
				// /* ANL_20220920 */
				// if(Others.giFlagFacturacion==1)
				// {
				// Others.vObservacion = Others.gcMensjFacturacion;
				// }
				// /* ANL_20220920 */
			}
			vIdOrdTransfer = -1;

			/* Obtener datos del registro */
			try {
				Map<String, Object> mapDatosDeRegistro = new HashMap<>();
				mapDatosDeRegistro = i4jdao.getDatosdeRegistro(cNroOrdORM);
				if (!mapDatosDeRegistro.isEmpty()) {
					vIdOrdTransfer = mapDatosDeRegistro.get("ID_ORD_TRANSFER") != null
							? (Long) mapDatosDeRegistro.get("ID_ORD_TRANSFER")
							: -1;
					vNroRecepciones = mapDatosDeRegistro.get("NRO_RECEPCIONES") != null
							? Integer.parseInt(mapDatosDeRegistro.get("NRO_RECEPCIONES").toString())
							: null;
					vCodTipoTDC = mapDatosDeRegistro.get("COD_TIPO_ORDEN_EORDER") != null
							? (String) mapDatosDeRegistro.get("COD_TIPO_ORDEN_EORDER")
							: null;
				}

			} catch (Exception e) {
				logger.error("Error: ERROR SELECT ID_ORD_TRANSFER, Exception:{}", e);
				// logger.info("Ejecutando rollback"); // AGREGADO JAAG INDEPE-5569 09/06/2023
				// connPg.rollback(); // AGREGADO AGREGADO JAAG INDEPE-5569 09/06/2023
				return;
			}

			if (vIdOrdTransfer != -1) {
				String messageIfExistError = "";
				try {
					/* Actualizar registro de cabecera */
					messageIfExistError = "ERROR UPDATE EOR_ORD_TRANSFER";
					i4jdao.updateRegistroCabecera(Others.vCodOperacion, Others.vObservacion, Others.vCodErrASY000,
							Others.COD_EST_RECEP, Others.COD_EST_RECER, vIdOrdTransfer);

					/* Actualizar registro de detalle */
					messageIfExistError = "ERROR UPDATE EOR_ORD_TRANSFER_DET";
					i4jdao.updateRegistroDetalle(Others.vCodOperacion, vIdOrdTransfer, vNroRecepciones);
				} catch (Exception e) {
					logger.error(messageIfExistError + ", Exception {}", e);
					// logger.info("Ejecutando rollback"); // AGREGADO JAAG INDEPE-5569 09/06/2023
					// connPg.rollback(); // AGREGADO JAAG INDEPE-5569 09/06/2023
					return;
				}
			}
		}
//        logger.info("Ejecutando commit"); // AGREGADO JAAG INDEPE-5569 09/06/2023
		// connPg.commit(); // AGREGADO JAAG INDEPE-5569 09/06/2023
	}

	public void setErrGen(String pErrorNCat) {
		Others.vCodOperacion = Others.vCodErrASY099;
		Others.vObservacion = Others.vDesErrASY099 + " " + pErrorNCat;
	}

	public boolean cargarDatosDeRespuesta(String plinRespu) throws Exception {
		Others.stRptaMasiva = new HashMap<>();
		int iVacio = 0, iNoNumero = 0, col = 0;
		String cCadena = plinRespu;
		String[] cadena_split = cCadena.split("\t");
		for (int i = 0; i < cadena_split.length; i++) {
			col++;
			cadena_split[i] = cadena_split[i].equals("null") ? "" : cadena_split[i];
			switch (i) {
			case 0:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stRptaMasiva.put("rNroOrden", cadena_split[i]);
				break;

			case 1:
				if (bfnHayValor(cadena_split[i])) {
					if (!esNumero(cadena_split[i])) {
						iNoNumero = 1;
						break;
					}
					Others.stRptaMasiva.put("rNroCuenta", Long.parseLong(cadena_split[i]));
				}
				break;

			case 2:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				if (!esNumero(cadena_split[i])) {
					iNoNumero = 1;
					break;
				}
				Others.stRptaMasiva.put("rNroNotif", cadena_split[i].trim());
				break;

			case 3:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stRptaMasiva.put("rCodContratista", cadena_split[i]);
				break;

			case 4:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stRptaMasiva.put("rNombreTecnico", cadena_split[i]);
				break;

			case 5:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stRptaMasiva.put("rFechaEjecucion", cadena_split[i]);
				break;

			case 6:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				if (!esNumero(cadena_split[i])) {
					iNoNumero = 1;
					break;
				}
				Others.stRptaMasiva.put("rHora", Integer.parseInt(cadena_split[i]));
				break;

			case 7:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				if (!esNumero(cadena_split[i])) {
					iNoNumero = 1;
					break;
				}
				Others.stRptaMasiva.put("rMinutos", Integer.parseInt(cadena_split[i]));
				break;

			case 8:
				if (bfnHayValor(cadena_split[i])) {
					Others.stRptaMasiva.put("rNombreCliente", cadena_split[i]);
				}
				break;

			case 9:
				if (bfnHayValor(cadena_split[i])) {
					Others.stRptaMasiva.put("rCodigoTipoDocum", cadena_split[i]);
				}
				break;

			case 10:
				if (bfnHayValor(cadena_split[i])) {
					Others.stRptaMasiva.put("rNroDocumento", cadena_split[i]);
				}
				break;

			case 11:
				if (bfnHayValor(cadena_split[i])) {
					Others.stRptaMasiva.put("rParentesco", cadena_split[i]);
				}
				break;

			case 12:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stRptaMasiva.put("rOT_Ejecutada", cadena_split[i]);
				break;

			case 13:
				if (bfnHayValor(cadena_split[i])) {
					Others.stRptaMasiva.put("rMotivoNoEjecuc", cadena_split[i]);
				}
				break;

			case 14:
				if (bfnHayValor(cadena_split[i])) {
					Others.stRptaMasiva.put("rObservacion", cadena_split[i]);
				}
				break;
			default:
				Others.printErrorinFile1.printf(
						"%s\tLínea [%d], la información para el grupo de datos 'Respuesta' contiene más campos de lo esperado.\n",
						Others.gcLinea, Others.glNroLinea);
				break;
			}

			if (iVacio == 1) {
				Others.printErrorinFile1.printf("%s\tLínea [%d], dato obligatorio Vacío en el campo '%s'.\n",
						Others.gcLinea, Others.glNroLinea, Others.stTitRespuesta[col]);
				Others.cMnsjErr = String.format("Dato obligatorio Vacío en el campo '%s'.", Others.stTitRespuesta[col]);
				return false;

			}
			if (iNoNumero == 1) {
				Others.printErrorinFile1.printf(
						"%s\tLínea [%d], el valor en el campo '%s' de Respuesta debe ser un número válido [%s].\n",
						Others.gcLinea, Others.glNroLinea, Others.stTitRespuesta[col], cadena_split[i]);
				Others.cMnsjErr = String.format(
						"El valor en el campo '%s' de Respuesta debe ser un número válido [%s].",
						Others.stTitRespuesta[col], cadena_split[i]);
				return false;
			}
		}

		if (col != 15) {
			Others.printErrorinFile1.printf(
					"%s\tLínea [%d], la información para el grupo de datos 'Respuesta' se encuentra incompleta.\n",
					Others.gcLinea, Others.glNroLinea);
			Others.cMnsjErr = "la información para el grupo de datos 'Respuesta' se encuentra incompleta";
			return false;
		}

		if (ifnValidate((String) Others.stRptaMasiva.get("rFechaEjecucion")) == 0) {
			Others.printErrorinFile1.printf(
					"%s\tLínea [%d], el formato de la fecha de ejecución de 'Respuesta' es incorrecto [%s].\n",
					Others.gcLinea, Others.glNroLinea, (String) Others.stRptaMasiva.get("rFechaEjecucion"));
			Others.cMnsjErr = String.format("El formato de la fecha de ejecución de 'Respuesta' es incorrecto [%s]",
					(String) Others.stRptaMasiva.get("rFechaEjecucion"));
			return false;
		}
		if ((Integer) Others.stRptaMasiva.get("rHora") < 0 || (Integer) Others.stRptaMasiva.get("rHora") > 23) {
			Others.printErrorinFile1.printf("%s\tLínea [%d], la Hora de Ejecución [%d] no es Válida.\n", Others.gcLinea,
					Others.glNroLinea, (Integer) Others.stRptaMasiva.get("rHora"));
			Others.cMnsjErr = String.format("La Hora de Ejecución [%d] no es Válida",
					(Integer) Others.stRptaMasiva.get("rHora"));
			return false;
		}

		if ((Integer) Others.stRptaMasiva.get("rMinutos") < 0 || (Integer) Others.stRptaMasiva.get("rMinutos") > 59) {
			Others.printErrorinFile1.printf("%s\tLínea [%d], Minuto de Ejecución [%d] no Válido.\n", Others.gcLinea,
					Others.glNroLinea, (Integer) Others.stRptaMasiva.get("rMinutos"));
			Others.cMnsjErr = String.format("Minuto de Ejecución [%d] no Válido",
					(Integer) Others.stRptaMasiva.get("rMinutos"));
			return false;
		}

		Others.gFechaEjecLarga = String.format("%s %d:%d:00", (String) Others.stRptaMasiva.get("rFechaEjecucion"),
				(Integer) Others.stRptaMasiva.get("rHora"), (Integer) Others.stRptaMasiva.get("rMinutos"));

//		Others.dEjecuc=tASegundos(Others.gFechaEjecLarga);
//		
//		if(Others.dEjecuc.after(Others.dActual))
//		{
//			Others.printErrorinFile1.printf("%s\tLínea [%d], La fecha de ejecución de 'Respuesta', en conjunto con la Hora y Minutos, [%s] debe ser menor o igual a la fecha actual [%s]\n",
//								Others.gcLinea, Others.glNroLinea, Others.gFechaEjecLarga, Others.gFechaHora);
//			 Others.cMnsjErr=String.format("La fecha de ejecución de 'Respuesta', en conjunto con la Hora y Minutos, [%s] debe ser menor o igual a la fecha actual [%s]",  Others.gFechaEjecLarga, Others.gFechaHora);
//			return false;
//		}

		logger.info("sc4j_Constants_LIS_PARENTESCO: {}", Constants.LIS_PARENTESCO);
		logger.info("sc4j_rParentesco: {}", Others.stRptaMasiva.get("rParentesco"));

		if (!Constants.LIS_PARENTESCO.contains(
				Others.stRptaMasiva.get("rParentesco") != null ? Others.stRptaMasiva.get("rParentesco").toString()
						: "X")) { // Linea 654 -659
			logger.info("Entro a la validacion de parentesco ORMRespuestaMasiva sc4j");
			Others.printErrorinFile1.printf("%s\tLínea [%d], Código de Parentesco [%s] No Válido.\n", Others.gcLinea,
					Others.glNroLinea, (String) Others.stRptaMasiva.get("rParentesco"));
			Others.cMnsjErr = String.format("Código de Parentesco [%s] No Válido",
					(String) Others.stRptaMasiva.get("rParentesco"));
			return false;
		}

		if (((String) Others.stRptaMasiva.get("rOT_Ejecutada")).equalsIgnoreCase("NO")) {
			if (bfnHayValor((String) Others.stRptaMasiva.get("rMotivoNoEjecuc"))) {
				if (((String) Others.stRptaMasiva.get("rMotivoNoEjecuc")).equalsIgnoreCase("O")) {
					Others.stRptaMasiva.put("rMotivoNoEjecuc", "1");
				}
				if (((String) Others.stRptaMasiva.get("rMotivoNoEjecuc")).equalsIgnoreCase("F")) {
					Others.stRptaMasiva.put("rMotivoNoEjecuc", "2");
				}
				if (((String) Others.stRptaMasiva.get("rMotivoNoEjecuc")).equalsIgnoreCase("P")) {
					Others.stRptaMasiva.put("rMotivoNoEjecuc", "3");
				}

				if (!bfnValidarMotNoEjec((String) Others.stRptaMasiva.get("rMotivoNoEjecuc"))) // aquí se carga el
																								// rIDMotivoNoEjecuc de
																								// stRptaMasiva
				{
					Others.printErrorinFile1.printf(
							"%s\tLínea [%d], Código de Motivo de NO Ejecución [%s] No Válido.\n", Others.gcLinea,
							Others.glNroLinea, (String) Others.stRptaMasiva.get("rMotivoNoEjecuc"));
					Others.cMnsjErr = String.format("Código de Motivo de NO Ejecución [%s] No Válido",
							(String) Others.stRptaMasiva.get("rMotivoNoEjecuc"));
					return false;
				}
			} else {
				Others.printErrorinFile1.printf(
						"%s\tLínea [%d], el ingreso del Código de Motivo de NO Ejecución es obligatorio cuando la Ejecución de OT es 'NO'.\n",
						Others.gcLinea, Others.glNroLinea);
				Others.cMnsjErr = "El ingreso del Código de Motivo de NO Ejecución es obligatorio cuando la Ejecución de OT es 'NO'";
				return false;
			}
		}
		return true;
	}

	public Timestamp tASegundos(String gFechaEjecLarga) throws Exception {
		logger.info("sc4j_gFechaEjecLarga: {}", gFechaEjecLarga);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			Date parsedDate = sdf.parse(gFechaEjecLarga);
			logger.info("sc4j_parsedDate: {}", parsedDate);

			Timestamp fechaConvertida = new java.sql.Timestamp(parsedDate.getTime());
			logger.info("sc4j_fechaConvertida: {}", fechaConvertida);

			return fechaConvertida;
		} catch (Exception e) {
			logger.error("Ocurrió un error en tASegundos - Exeption: {}", e);
			throw e;
		}
	}

	private int ifnValidate(String cFecha) { /* Valida la fecha pasada como char * con formato "DD/MM/YYYY */
		if (cFecha.contains("-")) {
			cFecha = cFecha.replaceAll("-", "/");
		}
		int res = 1;
		String dateParts[] = cFecha.split("/");
		String dd = dateParts[0];
		String mm = dateParts[1];
		String aa = dateParts[2];
		if ((Integer.parseInt(mm) < 0) || (Integer.parseInt(mm) > 12)) {
			res = 0;
		} else {
			if (Integer.parseInt(dd) > ifnDaySinMonth(Integer.parseInt(mm), Integer.parseInt(aa))) {
				res = 0;
			}
		}
		return res;
	}

	private int ifnValidateFechaLectura(
			String cFecha) { /* Valida la fecha pasada como char * con formato "YYYY-MM-DD */
		int res = 1;
		String dateParts[] = cFecha.split("-");
		String dd = dateParts[2];
		String mm = dateParts[1];
		String aa = dateParts[0];
		if ((Integer.parseInt(mm) < 0) || (Integer.parseInt(mm) > 12)) {
			res = 0;
		} else {
			if (Integer.parseInt(dd) > ifnDaySinMonth(Integer.parseInt(mm), Integer.parseInt(aa))) {
				res = 0;
			}
		}
		return res;
	}

	private int ifnDaySinMonth(int mm, int aa) {/*
												 * devuelve la cantidad de dias para el mes pasado por parametro y año
												 * (verifica si es bisiesto)
												 */
		int res = 31;
		if ((mm == 4) || (mm == 6) || (mm == 9) || (mm == 11)) {
			res = 30;
		}
		if (mm == 2) {
			res = 28;
			if (ifnEsBisiesto(aa) == 1) {
				res++;
			}
		}
		return res;
	}

	private int ifnEsBisiesto(int aa) { /* Valida el año pasado por parametro como int aa */
		int res = 0;

		if (aa % 4 == 0) {
			res = 1;
		}
		if ((aa % 100 == 0) && (aa % 400 != 0)) {
			res = 0; /* ¡Salud Gregorio! */
		}
		return res;
	}

	private boolean bfnValidarMotNoEjec(String pCodMotNoEje) {
		try {
			Long idMotNoeje = i4jdao.getidMotNoeje(pCodMotNoEje);
			if (idMotNoeje != 0L) {
				Others.stRptaMasiva.put("rIDMotivoNoEjecuc", idMotNoeje);
			}
			return true;
		} catch (Exception e) {
			logger.error("Error al obtener el ID del Motivo de No Ejecucion. Revisar Log del Proceso. Exception: {}",
					e);
			return false;
		}
	}

	private boolean bfnHayValor(String pVar) {
		if (pVar.equalsIgnoreCase("") || pVar.length() < 1) {
			return false;
		}
		return true;
	}

	public boolean esNumero2(String cadena) { /* Valida números positivos y negativos, sean enteros o decimales */
		try {
			double numerod = Double.parseDouble(cadena);
			// long numeroi=Long.parseLong(cadena);

			return true;
		} catch (Exception e) {// No es entero o decimal, si en caso no se realiza el parse.
			return false;
		}
	}

	public boolean esNumero(String cadena) { /* Valida números positivos, sean enteros o decimales */
		logger.info("esNumero sc4j");
		logger.info("cadena sc4j: [{}]", cadena);
		cadena = cadena.trim();
		try {
			if (cadena.contains(" "))
				return false;
			if (cadena.contains("-"))
				return false;

			double numerod = Double.parseDouble(cadena);
			// long numeroi=Long.parseLong(cadena);
			if (numerod >= 0) { // Es positivo, cualquiera de las 2 conversiones, ya sea entero o decimal
				return true;
			}
			return false;
		} catch (Exception e) {// No es entero o decimal, si en caso no se realiza el parse.
			return false;
		}
	}

	public boolean cargarTareas(String pCadena, String sepTar) {
		Map<String, Object> map = new HashMap<>();
		Others.cMnsjErr = "";
		int x = 0, iTareas = 0, col = 0, iVacio = 0, iNoNumero = 0, iFlagFin = 0;
		String cCadena = pCadena;
		String[] cadena_completa = cCadena.split(sepTar);
		Others.stTarea = new ArrayList<>();
		for (x = 0; x < cadena_completa.length; x++) { // Iterar Grupos separados por "&"
			iTareas++;
			String[] cadena_interna = cadena_completa[x].split(Constants.SEP_DAT); // Iterar cadena de cada grupo por
																					// "\t"
			for (int i = 0; i < cadena_interna.length; i++) {
				logger.info("cadena_interna[{}]: [{}]", i, cadena_interna[i]);
				col++;
				cadena_interna[i] = cadena_interna[i].equals("null") ? "" : cadena_interna[i];
				switch (i) {

				case 0:
					if (!bfnHayValor(cadena_interna[i])) {
						iVacio = 1;
						break;
					}
					map.put("tNroTarea", cadena_interna[i]);
					break;

				case 1:
					if (!bfnHayValor(cadena_interna[i])) {
						iVacio = 1;
						break;
					}
					map.put("tIdentificador", cadena_interna[i]);
					break;

				case 2:
					if (!bfnHayValor(cadena_interna[i])) {
						iVacio = 1;
						break;
					}
					if (!esNumero(cadena_interna[i])) {
						iNoNumero = 1;
						break;
					}
					// map.put("tCantidad", Integer.parseInt(cadena_interna[i])); break; //
					// COMENTADO RSOSA 05072023
					map.put("tCantidad", Long.parseLong(cadena_interna[i]));
					break; // AGREGADO RSOSA 05072023

				case 3:
					if (!bfnHayValor(cadena_interna[i])) {
						iVacio = 1;
						break;
					}
					map.put("tEjecutado", cadena_interna[i]);
					iFlagFin = 1;
					break;

				default: {
					Others.printErrorinFile1.printf(
							"%s\tLínea [%d], la información para el grupo de datos 'Tareas' contiene más campos de lo esperado.\n",
							Others.gcLinea, Others.glNroLinea);
					Others.cMnsjErr = "La información para el grupo de datos 'Tareas' contiene más campos de lo esperado";
					return false;
				}
				}
				if (iVacio == 1) {
					Others.printErrorinFile1.printf("%s\tLínea [%d], dato obligatorio Vacío en el campo '%s'.\n",
							Others.gcLinea, Others.glNroLinea, Others.stTitPartidas[col]);
					Others.cMnsjErr = String.format("Dato obligatorio Vacío en el campo '%s'",
							Others.stTitPartidas[col]);
					return false;
				}
				if (iNoNumero == 1) {
					Others.printErrorinFile1.printf(
							"%s\tLínea [%d], el valor en el campo '%s' de Tareas debe ser un número válido [%s].\n",
							Others.gcLinea, Others.glNroLinea, Others.stTitPartidas[col], (String) cadena_interna[i]);
					Others.cMnsjErr = String.format(
							"El valor en el campo '%s' de Tareas debe ser un número válido [%s]",
							Others.stTitPartidas[col], (String) cadena_interna[i]);
					return false;
				}

				if (iFlagFin == 1) {
					Map<String, Object> mapOfCharge = new HashMap<String, Object>();
					for (Map.Entry<String, Object> entry : map.entrySet()) {
						String key = entry.getKey();
						String value = entry.getValue().toString();
						mapOfCharge.put(key, value);
					}
					Others.stTarea.add(x, mapOfCharge);
					break;
				}
			}

			if (col != 4) {
				Others.printErrorinFile1.printf(
						"%s\tLínea [%d], la información para el grupo de datos 'Tareas' se encuentra incompleta.\n",
						Others.gcLinea, Others.glNroLinea);
				Others.cMnsjErr = String
						.format("La información para el grupo de datos 'Tareas' se encuentra incompleta");
				return false;
			}
			col = 0;
			iFlagFin = 0;
			if (!((String) Others.stTarea.get(x).get("tEjecutado")).trim().equalsIgnoreCase("S")
					&& !((String) Others.stTarea.get(x).get("tEjecutado")).trim().equalsIgnoreCase("N")) {
				Others.printErrorinFile1.printf("%s\tLínea [%d], Valor Ejecutado [%s] No válido en 'Tareas'.\n",
						Others.gcLinea, Others.glNroLinea, (String) Others.stTarea.get(x).get("tEjecutado"));
				Others.cMnsjErr = String.format("Valor Ejecutado [%s] No válido en 'Tareas'",
						(String) Others.stTarea.get(x).get("tEjecutado"));
				return false;
			}
			logger.info("Others.stTarea.get({}).get(tCantidad): [{}]", x, Others.stTarea.get(x).get("tCantidad"));
			// if((Others.stTarea.get(x).get("tCantidad")!=null?Integer.parseInt(Others.stTarea.get(x).get("tCantidad").toString()):00)>999)
			// // COMENTADO RSOSA 05072023
			if ((Others.stTarea.get(x).get("tCantidad") != null
					? Long.parseLong(Others.stTarea.get(x).get("tCantidad").toString())
					: 00) > 999) // AGREGADO RSOSA 05072023
			{
				// Others.printErrorinFile1.printf("%s\tLínea [%d], Valor Cantidad [%d] No
				// válido en 'Tareas'.\n", Others.gcLinea, Others.glNroLinea,
				// Integer.parseInt(Others.stTarea.get(x).get("tCantidad").toString())); //
				// COMENTADO RSOSA 05072023
				// Others.cMnsjErr=String.format("Valor Cantidad [%d] No válido en 'Tareas'; el
				// valor No debe ser mayor a 999",
				// Integer.parseInt(Others.stTarea.get(x).get("tCantidad").toString())); //
				// COMENTADO RSOSA 05072023
				Others.printErrorinFile1.printf("%s\tLínea [%d], Valor Cantidad [%d] No válido en 'Tareas'.\n",
						Others.gcLinea, Others.glNroLinea,
						Long.parseLong(Others.stTarea.get(x).get("tCantidad").toString())); // AGREGADO RSOSA 05072023
				Others.cMnsjErr = String.format(
						"Valor Cantidad [%d] No válido en 'Tareas'; el valor No debe ser mayor a 999",
						Long.parseLong(Others.stTarea.get(x).get("tCantidad").toString())); // AGREGADO RSOSA 05072023
				return false;
			}
		}
		Others.iContTarea = iTareas;
		return true;
	}

	public boolean validarDatosEspecificos(Integer pIDEmpr, Integer pIdUser, Map<String, Object> pstRespMsv,
			List<Map<String, Object>> pstTareas, int piContTareas) {
		// Se almacena el error en la variable Others.cMnsjErr para controlar la orden a
		// rechazar
		Map<String, Object> result = new HashMap<>();
		int i = 0, iFlagCambioMed = 0, iFlagContraste = 0;

		try {
			result = bfnExisteOrden((String) pstRespMsv.get("rNroOrden")); // Cargar idOrden, idWorkflow, idBuzon,
																			// cFecVenc
			if (result == null) {
				Others.printErrorinFile1.printf(
						"No se pudo encontrar la Orden ORM Nro. [%s] o esta tiene un estado diferente a 'Creada'",
						(String) pstRespMsv.get("rNroOrden"));
				Others.cMnsjErr = String.format(
						"No se pudo encontrar la Orden ORM Nro. [%s] o esta tiene un estado diferente a 'Creada'",
						(String) pstRespMsv.get("rNroOrden")); // JAAG 15/06/2023
				return false;
			}
			if (!bfnExisteCuentaORM((Long) pstRespMsv.get("rNroCuenta"))) {
				Others.printErrorinFile1.printf("El Servicio de la cuenta [%d] no coincide con el servicio de la orden",
						(Long) pstRespMsv.get("rNroCuenta"));
				Others.cMnsjErr = String.format("El Servicio de la cuenta [%d] no coincide con el servicio de la orden",
						(Long) pstRespMsv.get("rNroCuenta")); // JAAG 15/06/2023
				return false;
			}
			;

			if (!bfnExisteContratistaORM((String) pstRespMsv.get("rCodContratista"))) {
				Others.printErrorinFile1.printf("El código de contratista [%s] no es válido",
						(String) pstRespMsv.get("rCodContratista"));
				Others.cMnsjErr = String.format("El código de contratista [%s] no es válido",
						(String) pstRespMsv.get("rCodContratista")); // JAAG 15/06/2023
				return false;
			}
			;

			if (!bfnExisteTipoDocum((String) pstRespMsv.get("rCodigoTipoDocum"))) {
				Others.printErrorinFile1.printf("Tipo de Documento [%s] No Válido",
						(String) pstRespMsv.get("rCodigoTipoDocum"));
				Others.cMnsjErr = String.format("Tipo de Documento [%s] No Válido",
						(String) pstRespMsv.get("rCodigoTipoDocum")); // JAAG 15/06/2023
				return false;
			}
			;

			// Tareas
			Others.gFlagTarNoEjecu = 0;
			for (i = 0; i < piContTareas; i++) {
				if (!bfnExisteTarea(pstTareas.get(i), i)) // Carga xcCambioMed y xcContraMed
				{
					Others.printErrorinFile1.printf("Código de Tarea [%s] No Válido (no existe o no está activo)",
							(String) pstTareas.get(i).get("tNroTarea"));
					Others.cMnsjErr = String.format("Código de Tarea [%s] No Válido (no existe o no está activo)",
							(String) pstTareas.get(i).get("tNroTarea")); // JAAG 15/06/2023
					return false;
				}

				if ((pstTareas.get(i).get("xcCambioMed") != null ? (String) pstTareas.get(i).get("xcCambioMed") : "")
						.equalsIgnoreCase("S") && iFlagCambioMed == 1) {
					Others.printErrorinFile1.printf("Sólo puede existir una (1) tarea de Cambio de Medidor. Tarea [%s]",
							(String) pstTareas.get(i).get("tNroTarea"));
					Others.cMnsjErr = String.format("Sólo puede existir una (1) tarea de Cambio de Medidor. Tarea [%s]",
							(String) pstTareas.get(i).get("tNroTarea")); // JAAG 15/06/2023
					return false;
				}

				if ((pstTareas.get(i).get("xcContraMed") != null ? (String) pstTareas.get(i).get("xcContraMed") : "")
						.equalsIgnoreCase("S") && iFlagContraste == 1) {
					Others.printErrorinFile1.printf(
							"Sólo puede existir una (1) tarea de Contraste de Medidor. Tarea [%s]",
							(String) pstTareas.get(i).get("tNroTarea"));
					Others.cMnsjErr = String.format(
							"Sólo puede existir una (1) tarea de Contraste de Medidor. Tarea [%s]",
							(String) pstTareas.get(i).get("tNroTarea")); // JAAG 15/06/2023
					return false;
				}
				Others.cIdentActivo = "";

				if (!bfnValidarIdentificador((String) pstTareas.get(i).get("tNroTarea"),
						(String) pstTareas.get(i).get("tIdentificador"))) // Carga Others.cIdentActivo
				{
					Others.printErrorinFile1.printf("Condición de la Tarea [%s] no es válida",
							(String) pstTareas.get(i).get("tIdentificador"));
					Others.cMnsjErr = String.format("Condición de la Tarea [%s] no es válida",
							(String) pstTareas.get(i).get("tIdentificador")); // JAAG 15/06/2023
					return false;
				}

				if (!Others.cIdentActivo.equalsIgnoreCase("S")) {
					Others.printErrorinFile1.printf(
							"No se encuentra activo [%s] el identificador [%s] para la tarea [%s]", Others.cIdentActivo,
							(String) pstTareas.get(i).get("tIdentificador"),
							(String) pstTareas.get(i).get("tNroTarea"));
					Others.cMnsjErr = String.format(
							"No se encuentra activo [%s] el identificador [%s] para la tarea [%s]", Others.cIdentActivo,
							(String) pstTareas.get(i).get("tIdentificador"),
							(String) pstTareas.get(i).get("tNroTarea")); // JAAG 15/06/2023
					return false;
				}

				if ((pstTareas.get(i).get("xcCambioMed") != null ? (String) pstTareas.get(i).get("xcCambioMed") : "")
						.equalsIgnoreCase("S")) {
					iFlagCambioMed = 1;
				}
				if ((pstTareas.get(i).get("xcContraMed") != null ? (String) pstTareas.get(i).get("xcContraMed") : "")
						.equalsIgnoreCase("S")) {
					iFlagContraste = 1;
				}
				if (((String) pstTareas.get(i).get("tEjecutado")).equalsIgnoreCase("N")) {
					Others.gFlagTarNoEjecu = 1;
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("Error en validarDatosEspecificos. Exception: {}", e);
			Others.cMnsjErr = String.format("Error en validarDatosEspecificos."); // JAAG 15/06/2023
			return false;
		}
	}

	private boolean bfnValidarIdentificador(String pNroTarea, String pIdentif) {
		String cTmpActivo = "";
		try {
			cTmpActivo = i4jdao.bfnValidarIdentificador(pNroTarea, pIdentif);
			if (cTmpActivo.equalsIgnoreCase("")) {
				return false;
			} else {
				Others.cIdentActivo = cTmpActivo;
			}
		} catch (Exception e) {
			logger.error(String.format(
					"Error al verificar existencia de la Tarea [%s], identificador [%s]. Revisar Log del Proceso.\\n",
					pNroTarea, pIdentif));
			return false;
		}
		return true;
	}

	private boolean bfnExisteTarea(Map<String, Object> pstTareas, int i) {
		Map<String, Object> result = new HashMap<>();
		String pNroTarea = (String) pstTareas.get("tNroTarea");
		try {
			result = i4jdao.bfnExisteTarea(pNroTarea);
			if (result == null || result.isEmpty()) {
				return false;
			} else {
				pstTareas.put("xdvalor", result.get("xdvalor"));
				pstTareas.put("xccambiomed", result.get("xccambiomed"));
				pstTareas.put("xccontramed", result.get("xccontramed"));
				pstTareas.put("xidtarea", result.get("xidtarea"));

				Others.stTarea.remove(i);
				Others.stTarea.add(i, pstTareas);
			}
		} catch (Exception e) {
			logger.error(String.format("Error al verificar existencia de la Tarea [%s]. Revisar Log del Proceso.\n",
					pNroTarea));
			return false;
		}
		return true;
	}

	private boolean bfnExisteTipoDocum(String pTipoDocum) {
		Others.gIdTipDocPersona = 0;
		Others.gTipDocCodInter = "";
		Map<String, Object> result = new HashMap<>();
		try {
			result = i4jdao.bfnExisteTipoDocum(pTipoDocum);
			if (result == null || result.isEmpty()) {
				return false;
			} else {
				Others.gIdTipDocPersona = Integer.parseInt(result.get("id_tip_doc_persona").toString());
				Others.gTipDocCodInter = result.get("cod_interno").toString();
			}
		} catch (Exception e) {
			logger.error(String.format("Error al verificar existencia Documento [%s]. Revisar Log del Proceso.\n",
					pTipoDocum));
			return false;
		}
		return true;
	}

	private boolean bfnExisteContratistaORM(String pCodContra) {
		try {
			int iTmpExiste = i4jdao.bfnExisteContratistaORM(pCodContra);
			if (iTmpExiste < 1) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error(String.format(
					"Error al verificar existencia del Contratista ORM [%s]. Revisar Log del Proceso.\n", pCodContra));
			return false;
		}
	}

	private boolean bfnExisteCuentaORM(Long pCuenta) {
		try {
			int iTmpExiste = i4jdao.bfnExisteCuentaORM(pCuenta);
			if (iTmpExiste < 1) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error(String.format(
					"Error al verificar existencia de la Cuenta ORM [%d]. Revisar Log del Proceso.\n", pCuenta));
			return false;
		}
	}

	private Map<String, Object> bfnExisteOrden(String pOrden) {
		Map<String, Object> result = new HashMap<>();
		try {
			result = i4jdao.bfnExisteOrden(pOrden);
		} catch (Exception e) {
			logger.error("Error al verificar existencia de la Orden - bfnExisteOrden(). Exception: {}", e);
			return null;
		}
		if (result != null) {
			Others.stRptaMasiva.put("lIdOrden", result.get("xIdOrden"));
			Others.stRptaMasiva.put("lIdWorkflow", result.get("xIdWorkflow"));
			Others.stRptaMasiva.put("lIdBuzon", result.get("xIdBuzon"));
			// Others.stRptaMasiva.put("pFecVenc", result.get("xFecVenc"));
		}
		return result;

	}

	public boolean bfnCargaGlobales(Map<String, Object> stRptaMasiva) {
		String cCodContra = "";
		String cNombreTec = "";
		long lNroCuenta = 0;

		cCodContra = (String) stRptaMasiva.get("rCodContratista");
		cNombreTec = (String) stRptaMasiva.get("rNombreTecnico");
		lNroCuenta = (Long) stRptaMasiva.get("rNroCuenta");

		Others.gIdContratista = 0;
		Others.gIdEjecutor = 0;
		Others.gIdServicio = 0;

		String messageError = "";
		try {

			/*
			 * Metodo anterior de controlar errores messageError=String.
			 * format("Error en bfnCargaGlobales_into_gIdContratista. Código contratista [%s]."
			 * , cCodContra); Others.gIdContratista=i4jdao.getCodigoContratista(cCodContra);
			 * 
			 * messageError=String.
			 * format("Error en bfnCargaGlobales_into_gIdContratista. Código ejecutor [%s]."
			 * , cNombreTec); Others.gIdEjecutor=i4jdao.getCodigoEjecutor(cNombreTec);
			 * 
			 * messageError=String.
			 * format("Error en bfnCargaGlobales_into_gIdContratista. Cuenta [%s].",
			 * lNroCuenta); Others.gIdServicio=i4jdao.getCuenta(lNroCuenta);
			 */

			// Se modifica para controlar el error JAAG INICIO
			if ((i4jdao.getCodigoContratista(cCodContra) != 0)) {
				Others.gIdContratista = i4jdao.getCodigoContratista(cCodContra);
			} else {
				Others.cMnsjErr = String
						.format("Error en bfnCargaGlobales_into_gIdContratista. Código contratista [%s].", cCodContra);
				return false;
			}

			if ((i4jdao.getCodigoEjecutor(cNombreTec) != 0)) {
				Others.gIdEjecutor = i4jdao.getCodigoEjecutor(cNombreTec);
			} else {
				Others.cMnsjErr = String.format("Error en bfnCargaGlobales_into_gIdContratista. Código ejecutor [%s].",
						cNombreTec);
				return false;
			}

			if ((i4jdao.getCuenta(lNroCuenta) != 0)) {
				Others.gIdServicio = i4jdao.getCuenta(lNroCuenta);
			} else {
				Others.cMnsjErr = String.format("Error en bfnCargaGlobales_into_gIdContratista. Cuenta [%s].",
						lNroCuenta);
				return false;
			}
			// FIN
			Others.gEjecuto = "";
			if (((String) stRptaMasiva.get("rOT_Ejecutada")).equalsIgnoreCase("SI")) {
				Others.gEjecuto = "S";
			} else {
				Others.gEjecuto = "N";
			}
			return true;
		} catch (Exception e) {
			logger.error(messageError + ", Exception:{}", e);
			Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
			return false;
		}
	}

	// JDFH INDEPE-5569 26052023 INI >
	public String validarCampos(String campo, String num) {
		String msgError = "";
		if (num != null && !num.isEmpty()) {
			if (!num.matches("\\d+")) {
				msgError = "campo " + campo + " cuenta con caracteres no numericos";
			}
		} else {
			msgError = "campo " + campo + " vacio";
		}
		return msgError;
	}

	public void setDatosRechazado(String msg) {
		Others.cMnsjErr = msg != null ? msg : "Error";
	}

	// JDFH INDEPE-5569 26052023 FIN<

//	@Transactional
	public boolean bfnActualizarDatosRespuesta(Integer pIDEmpr, Integer pIdUser, Map<String, Object> pstRespMsv)
			throws SQLException {
		String messageError = "";
		try {

			Map<String, Object> stRMbff = pstRespMsv;
			String cNroOrden = (String) stRMbff.get("rNroOrden");
			String cObservacion = (String) stRMbff.get("rObservacion");
			String cFechaEjec = (String) stRMbff.get("rFechaEjecucion");
			String cNombreTec = (String) stRMbff.get("rNombreTecnico");
			String cNombreCli = (String) stRMbff.get("rNombreCliente");
			String cNroDocum = (String) stRMbff.get("rNroDocumento");
			String cCodContra = (String) stRMbff.get("rCodContratista");

			String ccFechaVenc = "";
			String iMotiNoEjecuc = "";
			long id_audit_event = 0;
			long idMotiNoEjecuc = 0;
			long lid_motivo = 0;

			long lid_raiz_xml = 0;
			long lid_parentesco = 0;
			long lid_contratista = 0;
			long lid_servicio = 0;

			// JDFH INDEPE-5569 26052023 INI >

			// long
			// idOrden=stRMbff.get("lIdOrden")!=null?Long.parseLong(stRMbff.get("lIdOrden").toString()):null;
			// long
			// idWorkflow=stRMbff.get("lIdWorkflow")!=null?Long.parseLong(stRMbff.get("lIdWorkflow").toString()):null;
			// long
			// lNroNotif=stRMbff.get("rNroNotif")!=null?Long.parseLong(stRMbff.get("rNroNotif").toString()):null;
			// long idBuzon=
			// stRMbff.get("lIdBuzon")!=null?Long.parseLong(stRMbff.get("lIdBuzon").toString()):null;

			String valiIdOrden = validarCampos("idOrden",
					stRMbff.get("lIdOrden") != null ? stRMbff.get("lIdOrden").toString() : null);
			if (!valiIdOrden.isEmpty()) {
				setDatosRechazado(valiIdOrden);
				Others.printErrorinFile1.printf(valiIdOrden);
				Others.cMnsjErr = valiIdOrden; /* ANL_PROBLEMA */
				return false;
			}
			long idOrden = Long.parseLong(stRMbff.get("lIdOrden").toString());

			String validarIdWorkFlow = validarCampos("idWorkflow",
					stRMbff.get("lIdWorkflow") != null ? stRMbff.get("lIdWorkflow").toString() : null);
			if (!validarIdWorkFlow.isEmpty()) {
				setDatosRechazado(validarIdWorkFlow);
				Others.printErrorinFile1.printf(validarIdWorkFlow);
				Others.cMnsjErr = validarIdWorkFlow; /* ANL_PROBLEMA */
				return false;
			}
			long idWorkflow = Long.parseLong(stRMbff.get("lIdWorkflow").toString());

			String validarNroNotif = validarCampos("NroNotif",
					stRMbff.get("rNroNotif") != null ? stRMbff.get("rNroNotif").toString() : null);
			if (!validarNroNotif.isEmpty()) {
				setDatosRechazado(validarNroNotif);
				Others.printErrorinFile1.printf(validarNroNotif);
				Others.cMnsjErr = validarNroNotif; /* ANL_PROBLEMA */
				return false;
			}
			long lNroNotif = Long.parseLong(stRMbff.get("rNroNotif").toString());

			String validarIdBuzon = validarCampos("idBuzon",
					stRMbff.get("lIdBuzon") != null ? stRMbff.get("lIdBuzon").toString() : null);
			if (!validarIdBuzon.isEmpty()) {
				setDatosRechazado(validarIdBuzon);
				Others.printErrorinFile1.printf(validarIdBuzon);
				Others.cMnsjErr = validarIdBuzon; /* ANL_PROBLEMA */
				return false;
			}
			long idBuzon = Long.parseLong(stRMbff.get("lIdBuzon").toString());

			// JDFH INDEPE 5569 26052023 FIN <

			long cParent = 0;

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_Update_ord_orden. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.updateOrdOrdenSetLeidoAndFechaIngresoEstadoActual(idOrden);

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_into_ccFechaVenc. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			ccFechaVenc = i4jdao.getFechaVencimientoOrdOrden(idOrden);
			if (ccFechaVenc.equalsIgnoreCase("")) {
				setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
				return false;
			}
			if (ccFechaVenc.equalsIgnoreCase("00/00/0000")) { // No hay fecha de vencimiento (orden creada en campo o
																// probablemente orden antigua que no se le calculo
																// fecha de vencimiento)
				// Calcular Fecha de Vencimiento
				// Obtener Fecha de creacion y plazo de vencimiento interno // Cursor XCurVenc -
				// Cursor de Datos de Fecha de Vencimiento

				messageError = String.format(
						"Error en bfnActualizarDatosRespuesta_FETCH xCurVenc INTO. Orden [%s]. Revisar Log del Proceso",
						cNroOrden);
				ccFechaVenc = i4jdao.getCursorDatosFechaVencimiento(idOrden);
				if (ccFechaVenc.equalsIgnoreCase("")) {
					setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
					Others.printErrorinFile1.printf(messageError);
					Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
					return false;
				}

				messageError = String.format(
						"Error en bfnActualizarDatosRespuesta_UPDATE ORD_ORDEN SET FECHA_VENCIMIENTO. Orden [%s]. Revisar Log del Proceso",
						cNroOrden);
				i4jdao.updateOrdOrdenSetFechaVencimiento(ccFechaVenc, idOrden);
			}

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_Update_wkf_workflow. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.updateWFKWorkflowSetIdStateAndParentTypeAndIdEmpresa(pIDEmpr, idWorkflow);

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_Update_orm_orden. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.updateOrmOrdenSetIdContratistaAndIEjecutorAndAtendioSeguroAndIdParenNotificado(lNroNotif,
					Others.gIdContratista, Others.gIdEjecutor, idOrden);

			messageError = String.format("Error en ELECT SQAUDITEVENT.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			id_audit_event = i4jdao.selectNextValSQAUDITEVENT();
			if (id_audit_event == 0) {
				setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
				return false;
			}

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_fwk_auditevent. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertFWK_AUDITEVENT(id_audit_event, Others.gIdEmprGlob, idOrden, pIdUser);

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_ord_historico. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORDHISTORICO(id_audit_event, idBuzon);

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_ord_observacion. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORDOBSERVACION(pIDEmpr, cObservacion, Others.gFechaEjecLarga, pIdUser, idOrden);

			if ((stRMbff.get("rParentesco") != null ? (String) stRMbff.get("rParentesco") : "").equalsIgnoreCase("T")) {
				cParent = 1;
			}
			if ((stRMbff.get("rParentesco") != null ? (String) stRMbff.get("rParentesco") : "").equalsIgnoreCase("I")) {
				cParent = 2;
			}
			if ((stRMbff.get("rParentesco") != null ? (String) stRMbff.get("rParentesco") : "").equalsIgnoreCase("V")) {
				cParent = 3;
			}
			if ((stRMbff.get("rParentesco") != null ? (String) stRMbff.get("rParentesco") : "").equalsIgnoreCase("P")) {
				cParent = 4;
			}
			if ((stRMbff.get("rParentesco") != null ? (String) stRMbff.get("rParentesco") : "").equalsIgnoreCase("M")) {
				cParent = 5;
			}
			if ((stRMbff.get("rParentesco") != null ? (String) stRMbff.get("rParentesco") : "").equalsIgnoreCase("N")) {
				cParent = 6;
			}

			if (((String) stRMbff.get("rOT_Ejecutada")).equalsIgnoreCase("NO")) {
				idMotiNoEjecuc = (Long) stRMbff.get("rIDMotivoNoEjecuc");
				iMotiNoEjecuc = (String) stRMbff.get("rMotivoNoEjecuc");
			}

			messageError = String.format(
					"Error en SELECT SQRESPUESTAORDENMANTENIMIENTO.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			long gSeqOrmIdResp = i4jdao.selectNextValSQRESPUESTAORDENMANTENIMIENTO();
			if (id_audit_event == 0) {
				setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
				return false;
			}

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_orm_respuesta. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORMRESPUESTA(gSeqOrmIdResp, pIDEmpr, idOrden, pIdUser, Others.gEjecuto, Others.gFechaEjecLarga,
					ccFechaVenc, lNroNotif, Others.gIdContratista, cNombreTec, cNombreCli, cParent, Others.gIdServicio,
					Others.gIdTipDocPersona, cNroDocum, (String) stRMbff.get("rOT_Ejecutada"), idMotiNoEjecuc);

			messageError = String.format(
					"Error en SELECT SQINNERRAIZORDENMANTENIMIENTO.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			lid_raiz_xml = i4jdao.selectNextValSQINNERRAIZORDENMANTENIMIENTO();
			if (id_audit_event == 0) {
				setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
				return false;
			}

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_orm_in_raiz_xml. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORMINRAIZXML(lid_raiz_xml, pIDEmpr);

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_orm_in_tip_doc. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORMINTIPDOC(pIDEmpr, Others.gTipDocCodInter);

			messageError = String.format(
					"Error en SELECT SQINNERPARENTESCO.NEXTVAL. Orden [%s]. Revisar Log del Proceso", cNroOrden);
			lid_parentesco = i4jdao.selectNextValSQINNERPARENTESCO();
			if (lid_parentesco == 0) {
				setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
				return false;
			}

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_orm_in_parentesco. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORMINPARENTESCO(lid_parentesco, pIDEmpr, cParent);

			messageError = String.format(
					"Error en SELECT SQINNERCONTRATISTA.NEXTVAL. Orden [%s]. Revisar Log del Proceso", cNroOrden);
			lid_contratista = i4jdao.selectNextValSQINNERCONTRATISTA();
			if (lid_contratista == 0) {
				setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
				return false;
			}

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_orm_in_contratista. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORMINCONTRATISTA(lid_contratista, pIDEmpr, cCodContra);

			if (((String) stRMbff.get("rOT_Ejecutada")).equalsIgnoreCase("NO")) {
				messageError = String.format(
						"Error en SELECT SQINNERMOTIVONOEJECUCIONORDENM.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
						cNroOrden);
				lid_motivo = i4jdao.selectNextValSQINNERMOTIVONOEJECUCIONORDENM();
				if (lid_motivo == 0) {
					setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
					Others.printErrorinFile1.printf(messageError);
					Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
					return false;
				}

				messageError = String.format(
						"Error en bfnActualizarDatosRespuesta_insert_orm_in_motivo. Orden [%s]. Revisar Log del Proceso",
						cNroOrden);
				i4jdao.insertORMINMOTIVO(lid_motivo, pIDEmpr, iMotiNoEjecuc);
			}

			messageError = String.format(
					"Error en SELECT SQINNERRESPUESTAORDENMANTENIMI.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			Others.gSeqOrmInResp = i4jdao.selectNextValSQINNERRESPUESTAORDENMANTENIMI();
			if (Others.gSeqOrmInResp == 0) {
				setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
				return false;
			}

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_orm_in_resp. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORMINRESP(Others.gSeqOrmInResp, pIDEmpr, Others.gEjecuto, Others.gFechaEjecLarga, lNroNotif,
					lid_contratista, cNombreTec, lid_parentesco, cNombreCli, Others.gIdTipDocPersona, cNroDocum,
					(String) stRMbff.get("rOT_Ejecutada"), lid_motivo);

			messageError = String
					.format("Error en SQINNERSERVICIOASOCIADO.NEXTVAL. Orden [%s]. Revisar Log del Proceso", cNroOrden);
			lid_servicio = i4jdao.selectNextValSQINNERSERVICIOASOCIADO();
			if (lid_servicio == 0) {
				setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
				return false;
			}

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_orm_in_servicio. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORMINSERVICIO(lid_servicio, pIDEmpr, Others.gIdServicio);

			messageError = String.format(
					"Error en SQINNERORDENMANTENIMIENTO.NEXTVAL. Orden [%s]. Revisar Log del Proceso", cNroOrden);
			Others.gSeqOrmIdOrden = i4jdao.selectNextValSQINNERORDENMANTENIMIENTO();
			if (Others.gSeqOrmIdOrden == 0) {
				setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
				return false;
			}

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_orm_in_orden. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORMINORDEN(Others.gSeqOrmIdOrden, pIDEmpr, cNroOrden, Others.gSeqOrmInResp, lid_servicio,
					lid_raiz_xml);

			messageError = String.format(
					"Error en bfnActualizarDatosRespuesta_insert_orm_in_serv. Orden [%s]. Revisar Log del Proceso",
					cNroOrden);
			i4jdao.insertORMINSERV(pIDEmpr, cObservacion, Others.gSeqOrmIdOrden);
//            	conn.commit();

			return true;
		} catch (Exception e) {
			logger.error(messageError + ", Exception:{}", e);
//			conn.rollback();
			setDatosRechazado(messageError); // JDFH INDEPE-5569 26052023
			Others.printErrorinFile1.printf(messageError);
			Others.cMnsjErr = messageError; /* ANL_PROBLEMA */
			return false;
		}
	}

//	@Transactional
	public boolean bfnActualizarDatosDePartidas(Integer pIDEmpr, Integer pIdUser, Map<String, Object> pstRespMsv,
			Map<String, Object> pstTareas) throws SQLException {
		String messageError = "";
		try {
			long idOrden = pstRespMsv.get("lIdOrden") != null ? Long.parseLong(pstRespMsv.get("lIdOrden").toString())
					: 0;
			long idTarea = pstTareas.get("xidtarea") != null ? Long.parseLong(pstTareas.get("xidtarea").toString()) : 0;
			int iCantidad = pstTareas.get("tCantidad") != null ? Integer.parseInt(pstTareas.get("tCantidad").toString())
					: 0;
			double dValor = pstTareas.get("xdvalor") != null ? Double.parseDouble(pstTareas.get("xdvalor").toString())
					: 0;

			String cCostoSis = "";
			String cCondTrabajo = "";

			String cEjecutado = (String) pstTareas.get("tEjecutado");
			String cCambioMed = (String) pstTareas.get("xcCambioMed");
			String cContraMed = (String) pstTareas.get("xcContraMed");
			String cNroTarea = (String) pstTareas.get("tNroTarea");

			if (dValor > 0) {
				cCostoSis = "S";
			} else {
				cCostoSis = "N";
			}

			if (((String) pstTareas.get("tIdentificador")).equalsIgnoreCase("C")) {
				cCondTrabajo = "Cambio";
			}
			if (((String) pstTareas.get("tIdentificador")).equalsIgnoreCase("I")) {
				cCondTrabajo = "Preventivo";
			}
			if (((String) pstTareas.get("tIdentificador")).equalsIgnoreCase("P")) {
				cCondTrabajo = "InstalacionEjecucion";
			}
			if (((String) pstTareas.get("tIdentificador")).equalsIgnoreCase("R")) {
				cCondTrabajo = "Reposicion";
			}

			messageError = String.format(
					"Error en bfnActualizarDatosDePartidas_into_iContTarEje. Orden [%s]. Revisar Log del Proceso",
					(String) pstRespMsv.get("rNroOrden"));
			long iContTarEje = i4jdao.selectCOUNT_ORMTAREAEJEC(idTarea, idOrden);
			if (iContTarEje == 0) {

				messageError = String.format(
						"Error en bfnActualizarDatosDePartidas_insert_orm_tarea_ejec. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.insertORMTAREAEJEC(pIDEmpr, cEjecutado.trim(), dValor, iCantidad, cCambioMed, cContraMed,
						cCostoSis, cCondTrabajo, idTarea, idOrden);

			} else {

				messageError = String.format(
						"Error en bfnActualizarDatosDePartidas_insert_orm_tarea_ejec. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.updateORMTAREAEJEC(cEjecutado.trim(), dValor, cCostoSis, cCambioMed, cContraMed, cCondTrabajo,
						iCantidad, idTarea, idOrden);

			}

			if (cEjecutado.equalsIgnoreCase("S")) {

				messageError = String.format(
						"Error en SELECT SQINNERCONDICIONTRABAJOORDENMA.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				long lid_cond_trabajo = i4jdao.selectNextValSQINNERCONDICIONTRABAJOORDENMA();
				if (lid_cond_trabajo == 0) {
					Others.printErrorinFile1.printf(messageError);
					Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
					return false;
				}

				messageError = String.format(
						"Error en bfnActualizarDatosDePartidas_insert_orm_in_cond_trab. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.insertORMINCONDTRAB(lid_cond_trabajo, pIDEmpr, cCondTrabajo);

				messageError = String.format(
						"Error en SELECT SQINNERTAREAORDENMANTENIMIENTO.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				long lid_tarea = i4jdao.selectNextValSQINNERTAREAORDENMANTENIMIENTO();
				if (lid_tarea == 0) {
					Others.printErrorinFile1.printf(messageError);
					Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
					return false;
				}

				messageError = String.format(
						"Error en bfnActualizarDatosDePartidas_insert_orm_in_tarea. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.insertORMINTAREA(lid_tarea, pIDEmpr, cNroTarea);

				messageError = String.format(
						"Error en bfnActualizarDatosDePartidas_insert_orm_in_tarea_eje. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.insertORMINTAREAEJE(pIDEmpr, cEjecutado, iCantidad, lid_tarea, lid_cond_trabajo,
						Others.gSeqOrmIdOrden);

			}
//        	conn.commit();
			return true;
		} catch (Exception e) {
			logger.error(messageError + ", Exception:{}", e);
//			conn.rollback();
			Others.printErrorinFile1.printf(messageError);
			Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
			return false;
		}
	}

	public boolean cargarDatosDeMedidores(String cpLinea) {
		Others.stMedidor = new HashMap<>();
		String cCadena = cpLinea;
		int iFinLinea = 0, iVacio = 0, iNoNumero = 0;
		int col = 0;
		String[] cadena_split = cCadena.split("\t");
		for (int i = 0; i < cadena_split.length; i++) {
			cadena_split[i] = cadena_split[i].equals("null") ? "" : cadena_split[i];
			col++;
			switch (i) {
			case 0:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gCodContratista", cadena_split[i].trim());
				break;
			case 1:
				if (bfnHayValor(cadena_split[i])) {
					Others.stMedidor.put("gCodEjecutor", cadena_split[i].trim());
				}
				break;
			case 2:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				if (!esNumero(cadena_split[i])) {
					iNoNumero = 1;
					break;
				}
				Others.stMedidor.put("gNroCuenta", Long.parseLong(cadena_split[i].trim()));
				if (Long.parseLong(cadena_split[i]) <= 0)
					iVacio = 1;
				break;
			case 3:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gFechaLectura", cadena_split[i].trim());
				break;
			case 4:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gCodMarMedRet", cadena_split[i].trim());
				break;
			case 5:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gCodModMedRet", cadena_split[i].trim());
				break;
			case 6:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gNroMedRet", cadena_split[i].trim());
				break;
			case 7:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gLecturaMedRet", cadena_split[i].trim());
				break;
			case 8:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gCodMarMedInst", cadena_split[i].trim());
				break;
			case 9:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gCodModMedInst", cadena_split[i].trim());
				break;
			case 10:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gNroMedInst", cadena_split[i].trim());
				break;
			case 11:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("factorInstalar", cadena_split[i].trim());
				break; // R.I. Agregado 31/10/2023
			case 12:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gLecturaMedInst", cadena_split[i].trim());
				break;
			case 13:
				if (!bfnHayValor(cadena_split[i])) {
					iVacio = 1;
					break;
				}
				Others.stMedidor.put("gCodPropMedInst", cadena_split[i].trim());
				break;
			case 14:
				if (bfnHayValor(cadena_split[i])) {
					if (!esNumero(cadena_split[i])) {
						iNoNumero = 1;
						break;
					}
					Others.stMedidor.put("gNroSello1", Long.parseLong(cadena_split[i].trim()));
				}
				break;
			case 15:
				if (bfnHayValor(cadena_split[i])) {
					Others.stMedidor.put("gColSello1", cadena_split[i].trim());
				}
				break;
			case 16:
				if (bfnHayValor(cadena_split[i])) {
					Others.stMedidor.put("gUbiSello1", cadena_split[i].trim());
				}
				break;
			case 17:
				if (bfnHayValor(cadena_split[i])) {
					if (!esNumero(cadena_split[i])) {
						iNoNumero = 1;
						break;
					}
					Others.stMedidor.put("gNroSello2", Long.parseLong(cadena_split[i].trim()));
				}
				break;
			case 18:
				if (bfnHayValor(cadena_split[i])) {
					Others.stMedidor.put("gColSello2", cadena_split[i].trim());
				}
				break;
			case 19:
				if (bfnHayValor(cadena_split[i])) {
					Others.stMedidor.put("gUbiSello2", cadena_split[i].trim());
				}
				iFinLinea = 1;
				break;
			default:
				Others.printErrorinFile1.printf("%s\tLínea [%d], problema no definido. Reprocesar línea.\n", cpLinea,
						Others.glNroLinea);
				break;
			}
			if (iVacio == 1) {
				Others.printErrorinFile1.printf("%s\tLínea [%d], dato obligatorio vacío en el campo '%s'.\n", cpLinea,
						Others.glNroLinea, Others.stTitMedidor[col]);
				Others.cMnsjErr = String.format("Línea [%d], dato obligatorio vacío en el campo '%s'.\n",
						Others.glNroLinea, Others.stTitMedidor[col]);
				return false;
			}
			if (iNoNumero == 1) {
				Others.printErrorinFile1.printf(
						"%s\tLínea [%d], el valor en el campo '%s' de Medidores debe ser un número válido [%s].\n",
						cpLinea, Others.glNroLinea, Others.stTitMedidor[col], cadena_split[i]);
				Others.cMnsjErr = String.format("Línea [%d], dato obligatorio vacío en el campo '%s'.\n",
						Others.glNroLinea, Others.stTitMedidor[col]);
				return false;
			}
		}

		if (ifnValidate((String) Others.stMedidor.get("gFechaLectura")) == 0) {
			Others.printErrorinFile1.printf("%s\tLínea [%d], el formato de la fecha de lectura es incorrecto [%s].\n",
					cpLinea, Others.glNroLinea, (String) Others.stMedidor.get("gFechaLectura"));
			Others.cMnsjErr = String.format("Línea [%d], el formato de la fecha de lectura es incorrecto [%s].\n",
					Others.glNroLinea, (String) Others.stMedidor.get("gFechaLectura"));
			return false;
		}

		if (!cargarLecturaRet((String) Others.stMedidor.get("gLecturaMedRet"),
				(String) Others.stMedidor.get("gCodModMedRet"), (String) Others.stMedidor.get("gCodMarMedRet"),
				(String) Others.stMedidor.get("gNroMedRet"))) {
			Others.printErrorinFile1.printf(
					"%s\tLínea [%d], formato de lectura(s) de medidor a retirar incorrecto [%s]. %s.\n", cpLinea,
					Others.glNroLinea, (String) Others.stMedidor.get("gLecturaMedRet"), Others.cMnsj);
			Others.cMnsjErr = String.format(
					"Línea [%d], formato de lectura(s) de medidor a retirar incorrecto [%s]. %s.\n", Others.glNroLinea,
					(String) Others.stMedidor.get("gLecturaMedRet"), Others.cMnsj);
			return false;
		}

		if (!cargarLecturaInst((String) Others.stMedidor.get("gLecturaMedInst"),
				(String) Others.stMedidor.get("gCodModMedInst"), (String) Others.stMedidor.get("gCodMarMedInst"),
				(String) Others.stMedidor.get("gNroMedInst"))) {
			Others.printErrorinFile1.printf(
					"%s\tLínea [%d], formato de lectura(s) de medidor a instalar incorrecto [%s]. %s.\n", cpLinea,
					Others.glNroLinea, (String) Others.stMedidor.get("gLecturaMedInst"), Others.cMnsj);
			Others.cMnsjErr = String.format(
					"Línea [%d], formato de lectura(s) de medidor a instalar incorrecto [%s]. %s.\n", Others.glNroLinea,
					(String) Others.stMedidor.get("gLecturaMedInst"), Others.cMnsj);
			return false;
		}
		return true;

	}

	private boolean cargarLecturaInst(String pLect, String pModelo, String pMarca, String pMedidor) {
		Others.ArrLecturaInst = new ArrayList<>();
		int iGrupos = 0, iAst = 0, iMod = 0, iMar = 0, iMed = 0, iCantMedIndividuales = 0;

		String cLectura = pLect;
		String cModelo = pModelo;
		String cMarca = pMarca;
		String cMedidor = pMedidor;

		for (int i = 0; i < cLectura.length(); i++) {
			// Si el carácter en [i] es un espacio (' ')
			if (cLectura.charAt(i) == ' ') {
				Others.cMnsj = "No pueden existir espacios en blanco";
				return false;
			}
		}
		String[] cLectura_split = cLectura.split("\\|");
		iGrupos = cLectura_split.length; // .Separador de Grupos

		for (int i = 0; i < cLectura_split.length; i++) { // Contador de separador Código*Valor
			String[] splitCV = cLectura_split[i].split("\\*");
			if (splitCV.length == 2) {
				iAst++;
			}
		}

		String[] cModelo_split = cModelo.split("\\|"); // Contador de Modelos.
		iMod = cModelo_split.length;

		String[] cMarca_split = cMarca.split("\\|"); // Contador de Marcas.
		iMar = cMarca_split.length;

		String[] cMedidor_split = cMedidor.split("\\|"); // Contador de Medidores.
		iMed = cMedidor_split.length;

		if (iGrupos != iAst) {
			Others.cMnsj = "Para cada pareja 'Código*Valor' debe existir un separador '*'";
			return false;
		}
		if (iGrupos != iMed) {
			Others.cMnsj = "Hay diferencias entre el total de lecturas y el total de medidores";
			return false;
		}

		Map<String, Object> map = new HashMap<>();

		String medidorActual = "";

		String[] grupos = cLectura.split("\\|"); // .Separador de Grupos
		for (int i = 0; i < grupos.length; i++) {
			String[] splitCodigoValor = cLectura_split[i].split("\\*");
			String codigo = splitCodigoValor[0];
			if (!bfnHayValor(codigo)) {
				Others.cMnsj = "'Código' vacío";
				return false;
			}

			String valor = splitCodigoValor[1];
			if (!bfnHayValor(valor)) {
				Others.cMnsj = "''Valor' vacío";
				return false;
			}

			String cmodelo = cModelo_split[i];
			if (!bfnHayValor(cmodelo)) {
				Others.cMnsj = "'Modelo' vacío";
				return false;
			}

			String cmarca = cMarca_split[i];
			if (!bfnHayValor(cmarca)) {
				Others.cMnsj = "'Marca' vacío";
				return false;
			}

			String cmedidor = cMedidor_split[i];
			if (!bfnHayValor(cmedidor)) {
				Others.cMnsj = "'Medidor' vacío";
				return false;
			}

			map.put("cCodigo", codigo);
			map.put("dValor", valor);
			map.put("cMedModelo", cmodelo);
			map.put("cMedMarca", cmarca);
			map.put("cMedNumero", cmedidor);

			if (iMed == 1) {
				iCantMedIndividuales = 1;
			} else {
				if (i == 0) {
					medidorActual = cmedidor;
					iCantMedIndividuales++;
				} else {
					if (!medidorActual.equals(cmedidor)) {
						iCantMedIndividuales++;
					}
					medidorActual = cmedidor;
				}
			}
			map.put("iIndicaMedidor", iCantMedIndividuales);

			Map<String, Object> mapOfCharge = new HashMap<String, Object>();
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue().toString();
				mapOfCharge.put(key, value);
			}

			Others.ArrLecturaInst.add(i, mapOfCharge);
		}
		Others.iContInst = iGrupos;
		Others.iCantMedIndvInst = iCantMedIndividuales;
		return true;
	}

	private boolean cargarLecturaRet(String pLect, String pModelo, String pMarca, String pMedidor) {
		Others.ArrLecturaRet = new ArrayList<>();
		int iGrupos = 0, iAst = 0, iMod = 0, iMar = 0, iMed = 0, iCantMedIndividuales = 0;

		String cLectura = pLect;
		String cModelo = pModelo;
		String cMarca = pMarca;
		String cMedidor = pMedidor;

		for (int i = 0; i < cLectura.length(); i++) {
			// Si el carácter en [i] es un espacio (' ')
			if (cLectura.charAt(i) == ' ') {
				Others.cMnsj = "No pueden existir espacios en blanco";
				return false;
			}
		}
		String[] cLectura_split = cLectura.split("\\|");
		iGrupos = cLectura_split.length; // .Separador de Grupos

		for (int i = 0; i < cLectura_split.length; i++) { // Contador de separador Código*Valor
			String[] splitCV = cLectura_split[i].split("\\*");
			if (splitCV.length == 2) {
				iAst++;
			}
		}

		String[] cModelo_split = cModelo.split("\\|"); // Contador de Modelos.
		iMod = cModelo_split.length;

		String[] cMarca_split = cMarca.split("\\|"); // Contador de Marcas.
		iMar = cMarca_split.length;

		String[] cMedidor_split = cMedidor.split("\\|"); // Contador de Medidores.
		iMed = cMedidor_split.length;

		if (iGrupos != iAst) {
			Others.cMnsj = "Para cada pareja 'Código*Valor' debe existir un separador '*'";
			return false;
		}
		if (iGrupos != iMed) {
			Others.cMnsj = "Hay diferencias entre el total de lecturas y el total de medidores";
			return false;
		}

		Map<String, Object> map = new HashMap<>();

		String medidorActual = "";

		String[] grupos = cLectura.split("\\|"); // .Separador de Grupos
		for (int i = 0; i < grupos.length; i++) {
			String[] splitCodigoValor = grupos[i].split("\\*");
			String codigo = splitCodigoValor[0];
			if (!bfnHayValor(codigo)) {
				Others.cMnsj = "'Código' vacío";
				return false;
			}

			String valor = splitCodigoValor[1];
			if (!bfnHayValor(valor)) {
				Others.cMnsj = "''Valor' vacío";
				return false;
			}

			String cmodelo = cModelo_split[i];
			if (!bfnHayValor(cmodelo)) {
				Others.cMnsj = "'Modelo' vacío";
				return false;
			}

			String cmarca = cMarca_split[i];
			if (!bfnHayValor(cmarca)) {
				Others.cMnsj = "'Marca' vacío";
				return false;
			}

			String cmedidor = cMedidor_split[i];
			if (!bfnHayValor(cmedidor)) {
				Others.cMnsj = "'Medidor' vacío";
				return false;
			}

			map.put("cCodigo", codigo);
			map.put("dValor", valor);
			map.put("cMedModelo", cmodelo);
			map.put("cMedMarca", cmarca);
			map.put("cMedNumero", cmedidor);

			if (iMed == 1) {
				iCantMedIndividuales = 1;
			} else {
				if (i == 0) {
					medidorActual = cmedidor;
					iCantMedIndividuales++;
				} else {
					if (!medidorActual.equals(cmedidor)) {
						iCantMedIndividuales++;
					}
					medidorActual = cmedidor;
				}
			}

			map.put("iIndicaMedidor", iCantMedIndividuales);

			Map<String, Object> mapOfCharge = new HashMap<String, Object>();
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue().toString();
				mapOfCharge.put(key, value);
			}

			Others.ArrLecturaRet.add(i, mapOfCharge);
		}
		Others.iContReti = iGrupos;
		Others.iCantMedIndvRet = iCantMedIndividuales;
		logger.info("cargarLecturaRet fin Others.ArrLecturaRet: {}", Others.ArrLecturaRet);
		return true;
	}

//	@Transactional
	public boolean bfnActualizarDatosDeCambioMedidor(Integer pIDEmpr, Integer pIdUser, Map<String, Object> pstRespMsv,
			Map<String, Object> pstMedidor, List<Map<String, Object>> pArrLectRet, int piContReti,
			List<Map<String, Object>> pArrLectInst, int piContInst, int piCantMedRet, int piCantMedInst)
			throws SQLException {

		int i = 0, j = 0, k = 0;
		String chMedidorInst = "";
		String chModeloMedidorInst = "";
		String chMarcaMedidorInst = "";
		String chMedidorRet = "";
		String chModeloMedidorRet = "";
		String chMarcaMedidorRet = "";
		long idMedMarcaInst = 0;
		long idMedModeloInst = 0;
		long idMedMarcaRet = 0;
		long idMedModeloRet = 0;
		long idDynamicObjectInst = -2;
		long idEstComponenteInst = 0;
		long idComponenteInst = 0;
		long idDynamicObjectRet = -2;
		long idEstComponenteRet = 0;
		long idComponenteRet = 0;
		double vdValor = 0;
		String vcCodigo = "";
		long idOrden = pstRespMsv.get("lIdOrden") != null ? Long.parseLong(pstRespMsv.get("lIdOrden").toString())
				: null;
		String cCodPropMedInst = "";
		long lid_prop_comp;
		long lid_medidor_instalacion;

		String messageError = "";

		try {
			cCodPropMedInst = pstMedidor.get("gCodPropMedInst") != null ? pstMedidor.get("gCodPropMedInst").toString()
					: null;

			/* ANL_20210406 inicio - bucle para todos los medidores instalados */
			for (i = 1; i <= piCantMedInst; i++) {
				for (j = 0; j < piContInst; j++) {
					if ((pArrLectInst.get(j).get("iIndicaMedidor") != null
							? Integer.parseInt(pArrLectInst.get(j).get("iIndicaMedidor").toString())
							: -1) == i) {
						chMedidorInst = pArrLectInst.get(j).get("cMedNumero") != null
								? pArrLectInst.get(j).get("cMedNumero").toString()
								: "";
						chModeloMedidorInst = pArrLectInst.get(j).get("cMedModelo") != null
								? pArrLectInst.get(j).get("cMedModelo").toString()
								: "";
						chMarcaMedidorInst = pArrLectInst.get(j).get("cMedMarca") != null
								? pArrLectInst.get(j).get("cMedMarca").toString()
								: "";
						continue;
					}
				}
				logger.info("chMarcaMedidorInst sc4j: {}", chMarcaMedidorInst);

				idMedMarcaInst = bfnExisteMarcaMedidor2(chMarcaMedidorInst);
				logger.info("idMedMarcaInst sc4j: {}", idMedMarcaInst);

				if (idMedMarcaInst == 0) {
					Others.cMnsjErr = String.format(
							"No se encontró registrado/activo el siguiente código de marca de medidor a Instalar [%s].",
							chMarcaMedidorInst);
					return false;
				}

				logger.info("chModeloMedidorInst sc4j: {}", chModeloMedidorInst);
				logger.info("idMedMarcaInst sc4j: {}", idMedMarcaInst);

				idMedModeloInst = bfnExisteModeloMedidor2(chModeloMedidorInst, idMedMarcaInst);
				if (idMedModeloInst == 0) {
					Others.cMnsjErr = String.format(
							"No se pudo validar el medidor a Instalar, código de modelo [%s], ID marca [%d].",
							chModeloMedidorInst, idMedMarcaInst);
					return false;
				}

				logger.info("chMedidorInst sc4j: {}", chMedidorInst);
				logger.info("idMedModeloInst sc4j: {}", idMedModeloInst);
				logger.info("pIDEmpr sc4j: {}", pIDEmpr);

				if (!bfnExisteNumeroMedidor(chMedidorInst, idMedModeloInst, pIDEmpr, 0, Constants.OPC_I)) {
					Others.cMnsjErr = String.format(
							"No se encontró registrado/activo el siguiente número de medidor a Instalar [%s], ID modelo de medidor [%d].",
							chMedidorInst, idMedModeloInst);
					return false;
				}

				messageError = String.format(
						"Error en bfnActualizarDatosDeCambioMedidor_into_IdComponenteInst1. Medidor [%s].",
						chMedidorInst);
				Map<String, Object> map = i4jdao.selectDataInNroComponente(chMedidorInst, idMedModeloInst, pIDEmpr);

				idComponenteInst = (Long) map.get("IdComponenteInst");
				idDynamicObjectInst = (Long) map.get("idDynamicObjectInst");
				idEstComponenteInst = (Long) map.get("IdEstComponenteInst");

				messageError = String.format(
						"Error en bfnActualizarDatosDeCambioMedidor_insert_ord_medi_cambioInst. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.insertORDMEDICAMBIO(idComponenteInst, idOrden, pIDEmpr);

				messageError = String.format(
						"Error en SELECT SQINNERPROPIEDADCOMPONENTE.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				lid_prop_comp = i4jdao.selectNextValSQINNERPROPIEDADCOMPONENTE();
				if (lid_prop_comp == 0) {
					Others.printErrorinFile1.printf(messageError);
					Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
					return false;
				}

				messageError = String.format(
						"Error en bfnActualizarDatosDeCambioMedidor_insert_ord_in_prop_comp Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.insertORMINPROPCOMP(lid_prop_comp, pIDEmpr, cCodPropMedInst);

				messageError = String.format(
						"Error en SELECT SQINNERORMMEDIDORINSTALACION.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				lid_medidor_instalacion = i4jdao.selectNextValSQINNERORMMEDIDORINSTALACION();
				if (lid_medidor_instalacion == 0) {
					Others.printErrorinFile1.printf(messageError);
					Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
					return false;
				}

				messageError = String.format(
						"Error en bfnActualizarDatosDeCambioMedidor_insert_orm_in_med_inst. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.insertORMINMEDINST(lid_medidor_instalacion, pIDEmpr, chMedidorInst, chModeloMedidorInst,
						chMarcaMedidorInst, Others.gSeqOrmInResp, lid_prop_comp);

				for (k = 0; k < piContInst; k++) {
					if ((pArrLectInst.get(k).get("iIndicaMedidor") != null
							? Integer.parseInt(pArrLectInst.get(k).get("iIndicaMedidor").toString())
							: -1) == i) {
						vdValor = 0;
						vdValor = pArrLectInst.get(k).get("dValor") != null
								? Double.parseDouble(pArrLectInst.get(k).get("dValor").toString())
								: 0;
						vcCodigo = "";
						vcCodigo = pArrLectInst.get(k).get("cCodigo") != null
								? pArrLectInst.get(k).get("cCodigo").toString()
								: "";

						messageError = String.format(
								"Error en bfnActualizarDatosDeCambioMedidor_insert_orm_in_medida. Orden [%s]. Revisar Log del Proceso",
								(String) pstRespMsv.get("rNroOrden"));
						i4jdao.insertORMINMEDIDA(pIDEmpr, vcCodigo, vdValor, lid_medidor_instalacion);
					}
				}
			}
			/* ANL_20210406 fin instalado */

			/* ANL_20210406 inicio - bucle para todos los medidores retirados */
			for (i = 1; i <= piCantMedRet; i++) {
				for (j = 0; j < piContReti; j++) {
					if ((pArrLectRet.get(j).get("iIndicaMedidor") != null
							? Integer.parseInt(pArrLectRet.get(j).get("iIndicaMedidor").toString())
							: -1) == i) {
						chMedidorRet = pArrLectRet.get(j).get("cMedNumero") != null
								? pArrLectRet.get(j).get("cMedNumero").toString()
								: "";
						chModeloMedidorRet = pArrLectRet.get(j).get("cMedModelo") != null
								? pArrLectRet.get(j).get("cMedModelo").toString()
								: "";
						chMarcaMedidorRet = pArrLectRet.get(j).get("cMedMarca") != null
								? pArrLectRet.get(j).get("cMedMarca").toString()
								: "";
						continue;
					}
				}

				idMedMarcaRet = bfnExisteMarcaMedidor2(chMarcaMedidorRet);
				if (idMedMarcaInst == 0) {
					Others.cMnsjErr = String.format(
							"No se encontró registrado/activo el siguiente código de marca de medidor a Instalar [%s].",
							chMarcaMedidorRet);
					return false;
				}

				idMedModeloRet = bfnExisteModeloMedidor2(chModeloMedidorRet, idMedMarcaRet);
				if (idMedModeloRet == 0) {
					Others.cMnsjErr = String.format(
							"No se pudo validar el medidor a Instalar, código de modelo [%s], ID marca [%d].",
							chModeloMedidorRet, idMedMarcaRet);
					return false;
				}

				if (!bfnExisteNumeroMedidor(chMedidorRet, idMedModeloRet, pIDEmpr, Others.gIdServicio,
						Constants.OPC_R)) {
					Others.cMnsjErr = String.format("bfnExisteNumeroMedidor [%s], ID modelo de medidor [%d].",
							chMedidorRet, idMedModeloRet);
					return false;
				}

				messageError = String.format(
						"Error en bfnActualizarDatosDeCambioMedidor_into_IdComponenteRet. Medidor [%s].", chMedidorRet);
				Map<String, Object> map = i4jdao.selectDataInMedComponente(chMedidorRet, idMedModeloRet, pIDEmpr,
						Others.gIdServicio);

				idComponenteRet = map.get("IdComponenteRet") != null
						? Long.parseLong(map.get("IdComponenteRet").toString())
						: null;
				idDynamicObjectRet = map.get("idDynamicObjectRet") != null
						? Long.parseLong(map.get("idDynamicObjectRet").toString())
						: null;
				idEstComponenteRet = map.get("IdEstComponenteRet") != null
						? Long.parseLong(map.get("IdEstComponenteRet").toString())
						: null;

				messageError = String.format(
						"Error en bfnActualizarDatosDeCambioMedidor_insert_ord_medi_cambio_ret. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.insertORDMEDICAMBIO2(idComponenteRet, idOrden, pIDEmpr);

				messageError = String.format(
						"Error en SELECT SQINNERORMMEDIDORRETIRO.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				long gSeqOrmInMedRet = i4jdao.selectNextValSQINNERORMMEDIDORRETIRO();
				if (gSeqOrmInMedRet == 0) {
					Others.printErrorinFile1.printf(messageError);
					Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
					return false;
				}

				messageError = String.format(
						"Error en bfnActualizarDatosDeCambioMedidor_insert_orm_in_med_retiro. Orden [%s]. Revisar Log del Proceso",
						(String) pstRespMsv.get("rNroOrden"));
				i4jdao.insertORMINMEDRETIRO(gSeqOrmInMedRet, pIDEmpr, chMedidorRet, chModeloMedidorRet,
						chMarcaMedidorRet, Others.gSeqOrmInResp);

				for (k = 0; k < piContReti; k++) {
					if ((pArrLectRet.get(k).get("iIndicaMedidor") != null
							? Integer.parseInt(pArrLectRet.get(k).get("iIndicaMedidor").toString())
							: -1) == i) {
						vdValor = 0;
						vdValor = pArrLectRet.get(k).get("dValor") != null
								? Double.parseDouble(pArrLectRet.get(k).get("dValor").toString())
								: 0;
						vcCodigo = "";
						vcCodigo = pArrLectRet.get(k).get("cCodigo") != null
								? pArrLectRet.get(k).get("cCodigo").toString()
								: "";

						messageError = String.format(
								"Error en bfnActualizarDatosDeCambioMedidor_insert_orm_in_medida. Orden [%s]. Revisar Log del Proceso",
								(String) pstRespMsv.get("rNroOrden"));
						i4jdao.insertORMINMEDIDA2(pIDEmpr, vcCodigo, vdValor, gSeqOrmInMedRet);
					}
				}
			}
//	        conn.commit();
			return true;
		} catch (Exception e) {
			logger.error(messageError + ", Exception:{}", e);
//			conn.rollback();
			Others.printErrorinFile1.printf(messageError);
			Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
			return false;
		}
	}

	private boolean bfnExisteNumeroMedidor(String pNroMedidor, long pidMedModelo, Integer pIDEmpresa, long pIDServicio,
			Integer pOpc) {
		int iTmpExiste = i4jdao.bfnExisteNumeroMedidor(pNroMedidor, pidMedModelo, pIDEmpresa, pIDServicio, pOpc);

		if (iTmpExiste < 1) {
			return false;
		}
		return true;
	}

	private long bfnExisteModeloMedidor2(String pCodModeloMed, long pidMedMarca) {
		return i4jdao.bfnExisteModeloMedidor2(pidMedMarca, pCodModeloMed);
	}

	private long bfnExisteMarcaMedidor2(String chMarcaMedidorInst) {
		return i4jdao.bfnExisteMarcaMedidor2(chMarcaMedidorInst);
	}

	public boolean bfnValidarAIC(Long pIdOrden) {
		try {
			if (pIdOrden == null) {
				logger.error("Error al validar AIC para la Orden [%s]. Revisar Log del Proceso.\n", pIdOrden);
				Others.cMnsjErr = String.format("Error al validar AIC para la Orden [%s]. Revisar Log del Proceso.\n",
						pIdOrden); // JAAG 15/06/2023
				return false;
			}
			int count = i4jdao.bfnValidarAIC(pIdOrden);
			if (count == 0) {
				logger.error("Error al validar AIC para la Orden [%s]. Revisar Log del Proceso.\n", pIdOrden);
				Others.cMnsjErr = String.format("Error al validar AIC para la Orden [%s]. Revisar Log del Proceso.\n",
						pIdOrden); // JAAG 15/06/2023
				return false;
			}
		} catch (Exception e) {
			logger.error("Error al validar AIC para la Orden [%s]. Revisar Log del Proceso.\n", pIdOrden);
			Others.cMnsjErr = String.format("Error al validar AIC para la Orden [%s]. Revisar Log del Proceso.\n",
					pIdOrden); // JAAG 15/06/2023
			return false;
		}
		return true;
	}

	public boolean bfnAsignarUsuarioResponsable(Map<String, Object> stRptaMasiva) {
		String messageError = "";
		try {
			long idOrden = stRptaMasiva.get("lIdOrden") != null
					? Long.parseLong(stRptaMasiva.get("lIdOrden").toString())
					: null;
			long idUserRespEtapa = -1;
			long idResponsable = -1;

			// 1. Obtener el identificador SEG_USUARIO.ID_USUARIO responsable de la Etapa
			// actual de la Atención AIC
			idUserRespEtapa = i4jdao.getSEGUSUARIOID(idOrden);
			if (idUserRespEtapa == -1) {
				messageError = String.format(
						"Error en bfnAsignarUsuarioResponsable - SELECT ET.ID_USUARIO INTO :IdUserRespEtapa. Orden [%s]. Revisar Log del Proceso",
						idOrden);
				Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
				return false;
			}
			// -2. Obtener el identificador SEG_RESPONSABLE.ID_RESPONSABLE del usuario
			// obtenido en el paso 1. Para ello se puede utilizar el siguiente Qry de BD
			idResponsable = i4jdao.getSEGRESPONSABLEID(idUserRespEtapa);
			if (idResponsable == -1) {
				messageError = String.format(
						"Error en bfnAsignarUsuarioResponsable - SELECT ID_RESPONSABLE INTO :ID_RESPONSABLE. Orden [%s]. Revisar Log del Proceso",
						idOrden);
				Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
				return false;
			}
			// 3. Actualizar el identificador ID_RESPONSABLE de la tabla ORD_ORD_DERIV con
			// el ID_RESPONSABLE obtenido en el punto 2. Adicionalmente el último
			// responsable sería el actual antes del cambio
			messageError = String.format(
					"Error en bfnAsignarUsuarioResponsable - UPDATE ORD_ORD_DERIV SET ID_RESPONSABLE. Orden [%s]. Revisar Log del Proceso",
					idOrden);
			Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
			i4jdao.updateORDORDDERIV(idResponsable, idOrden);

		} catch (Exception e) {
			logger.error(messageError);
			Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
			return false;
		}

		return true;
	}

//	@Transactional
	public boolean bfnActualizacionFinalRespuesta(Integer pIDEmpr, Integer pIdUser, Map<String, Object> pstRespMsv)
			throws SQLException {
		String messageError = "";
		try {
			long idOrden = pstRespMsv.get("lIdOrden") != null ? Long.parseLong(pstRespMsv.get("lIdOrden").toString())
					: null;
			long idWorkflow = pstRespMsv.get("lIdWorkflow") != null
					? Long.parseLong(pstRespMsv.get("lIdWorkflow").toString())
					: null;
			long idBuzon = pstRespMsv.get("lIdBuzon") != null ? Long.parseLong(pstRespMsv.get("lIdBuzon").toString())
					: null;
			long lid_auditevent;

			messageError = String.format(
					"Error en bfnAsignarUsuarioResponsable - UPDATE ORD_ORD_DERIV SET ID_RESPONSABLE. Orden [%s]. Revisar Log del Proceso",
					(String) pstRespMsv.get("rNroOrden"));
			i4jdao.updateORDORDDENFINAL(idOrden);

			messageError = String.format(
					"Error en bfnActualizacionFinalRespuesta_update_wkf_workflow. Orden [%s]. Revisar Log del Proceso",
					(String) pstRespMsv.get("rNroOrden"));
			i4jdao.updateWKFWORKFLOWFINAL(idWorkflow);

			messageError = String.format("Error en ELECT SQAUDITEVENT.NEXTVAL. Orden [%s]. Revisar Log del Proceso",
					(String) pstRespMsv.get("rNroOrden"));
			lid_auditevent = i4jdao.selectNextValSQAUDITEVENT();
			if (lid_auditevent == 0) {
				Others.printErrorinFile1.printf(messageError);
				Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
				return false;
			}

			messageError = String.format(
					"Error en bfnActualizacionFinalRespuesta_update_wkf_workflow. Orden [%s]. Revisar Log del Proceso",
					(String) pstRespMsv.get("rNroOrden"));
			i4jdao.insertFWKAUDITEVENTFINAL(lid_auditevent, Others.gIdEmprGlob, idOrden, pIdUser);

			messageError = String.format(
					"Error en bfnActualizacionFinalRespuesta_update_wkf_workflow. Orden [%s]. Revisar Log del Proceso",
					(String) pstRespMsv.get("rNroOrden"));
			i4jdao.insertORDHISTORICOFINAL(lid_auditevent, idBuzon);

//			conn.commit();
		} catch (Exception e) {
			logger.error(messageError);
			Others.cMnsjErr = String.format(messageError); // JAAG 15/06/2023
//			conn.rollback();
			return false;
		}

		return true;
	}

	public boolean bfnCambioDeMedidor2(Integer pIDEmpr, Integer pIdUser, Map<String, Object> pstLin,
			List<Map<String, Object>> pArrLectRet, int piContReti, List<Map<String, Object>> pArrLectInst,
			int piContInst, int piCantMedRet, int piCantMedInst, Map<String, Object> pstRespMsv) throws SQLException {

		Map<String, Object> map = new HashMap<>();

		List<Map<String, Object>> ArrLecturaTemp = new ArrayList<>();

		Others.cSE_Estado_wkf = "";
		Others.lSE_IDEstado = 0;
		Others.idSE_IDServicioRet = 0;
		Others.cFechaEjecuVta = "";
		long idMedMarca = 0;
		long idMedModeloRet = 0;
		long idMedModeloInst = 0;
		long idSE_IDServicioRet = 0;
		String chMedidorRet = "";
		String chModeloMedidorRet = "";
		String chMarcaMedidorRet = "";
		String chMedidorInst = "";
		String chModeloMedidorInst = "";
		String chMarcaMedidorInst = "";
		String cFechaDMY = "";
		int i = 0, j = 0, k = 0, iTmpLect = 0;

		long idOrden = pstRespMsv.get("lIdOrden") != null ? Long.parseLong(pstRespMsv.get("lIdOrden").toString())
				: null;
		logger.info("bfnCambioDeMedidor2 idOrden: {}", idOrden);

		if (!bfnExisteCuenta(pIDEmpr,
				pstLin.get("gNroCuenta") != null ? Long.parseLong(pstLin.get("gNroCuenta").toString()) : null)) {
			Others.cMnsjErr = String.format("No se encontró registrada la cuenta [%d]",
					(Long) pstLin.get("gNroCuenta"));
			return false;
		}

		logger.info("sc4j_pstLin_gNroCuenta: {}", pstLin.get("gNroCuenta"));
		
		if (bfnCuentaEnFacturacion(
				pstLin.get("gNroCuenta") != null ? Long.parseLong(pstLin.get("gNroCuenta").toString()) : null)) {
			Others.cMnsjErr = String.format("La Cuenta [%d] se encuentra en proceso de facturación.",
					Long.parseLong(pstLin.get("gNroCuenta").toString()));
			return false;
		}

		logger.info("gNroCuenta: {}",
				pstLin.get("gNroCuenta") != null ? Long.parseLong(pstLin.get("gNroCuenta").toString()) : null);
		if (!bfnTieneServicioElectrico(
				pstLin.get("gNroCuenta") != null ? Long.parseLong(pstLin.get("gNroCuenta").toString()) : null)) {
			Others.cMnsjErr = String.format("Se encontró servicio eléctrico Retirado para la cuenta [%d]",
					(Long) pstLin.get("gNroCuenta"));
			return false;
		}

		logger.info("cSE_Estado_wkf: {}", Others.cSE_Estado_wkf);
		logger.info("lSE_IDEstado: {}", Others.lSE_IDEstado);
		logger.info("idSE_IDServicioRet: {}", Others.idSE_IDServicioRet);
		idSE_IDServicioRet = Others.idSE_IDServicioRet;

		if (Others.lSE_IDEstado == Constants.ESTADO_SE_MODIFICACION) {
			if (!bfnSE_VerificaVenta((Long) pstLin.get("gNroCuenta"))) {
				Others.cMnsjErr = String.format(
						"El servicio tiene una Venta por Modificación y se encuentra en un estado que aún no permite Cambio de Medidor para la cuenta [%d]",
						(Long) pstLin.get("gNroCuenta"));
				return false;
			}
		}

		if (!bfnExisteContratista(pIDEmpr,
				pstLin.get("gCodContratista") != null ? pstLin.get("gCodContratista").toString() : "")) {
			Others.cMnsjErr = String.format("No se encontró registrado/activo el contratista [%s]",
					(String) pstLin.get("gCodContratista"));
			return false;
		}

		if (!(pstLin.get("gCodEjecutor") != null ? pstLin.get("gCodEjecutor").toString() : "").equals("")) {
			if (!bfnExisteEjecutor(pIDEmpr, pstLin.get("gCodEjecutor").toString())) {
				Others.cMnsjErr = String.format("No se encontró registrado/activo el contratista [%s]",
						(String) pstLin.get("gCodEjecutor"));
				return false;
			}
		}

		if (Others.lSE_IDEstado == Constants.ESTADO_SE_MODIFICACION) {
			if (!bfnObtenerFechaEjecucionVta(
					pstLin.get("gNroCuenta") != null ? Long.parseLong(pstLin.get("gNroCuenta").toString()) : null)) {
				Others.cMnsjErr = String.format(
						"No se encontró la fecha de ejecución del servicio de ventas para la cuenta [%d]",
						(Long) pstLin.get("gNroCuenta"));
				return false;
			}

			if (!(pstLin.get("gFechaLectura") != null ? pstLin.get("gFechaLectura").toString() : "")
					.equalsIgnoreCase(Others.cFechaEjecuVta)) {
				Others.cMnsjErr = String.format(
						"El Servicio se encuentra en Modificación, la Fecha de Lectura [%s] debe ser igual a la Fecha de Ejecución del servicio de venta [%s]",
						(String) pstLin.get("gFechaLectura"), Others.cFechaEjecuVta);
				return false;
			}

		} else {
			cFechaDMY = carga_FechaHora(2);
			logger.info("gFechaLectura sc4j : {}", pstLin.get("gFechaLectura"));
			logger.info("cFechaDMY sc4j: {}", cFechaDMY);
			if (bfnFechaEsMenor(pstLin.get("gFechaLectura") != null ? pstLin.get("gFechaLectura").toString() : "",
					cFechaDMY) == false
					&& !(pstLin.get("gFechaLectura") != null ? pstLin.get("gFechaLectura").toString() : "")
							.equalsIgnoreCase(cFechaDMY)) {
				Others.cMnsjErr = String.format(
						"La Fecha de Lectura [%s] debe ser menor o igual a la Fecha actual [%s]",
						(String) pstLin.get("gFechaLectura"), cFechaDMY);
				return false;
			}
		}

		if (!bfnExisteCodigoPropiedadMedidor(
				pstLin.get("gCodPropMedInst") != null ? pstLin.get("gCodPropMedInst").toString() : "")) {
			Others.cMnsjErr = String.format(
					"No se encontró registrado/activo el siguiente código de propiedad de medidor a instalar [%s].",
					(String) pstLin.get("gCodPropMedInst"));
			return false;
		}

//		 if((Long)pstLin.get("gNroSello1") > 0)
//		    {
//			 if(!bfnExisteNroSello((Long)pstLin.get("gNroSello1"))){
//				 
//				 Others.cMnsjErr=String.format("No se encontró registrado/disponible el siguiente Número de Sello 1 [%d].",(Long)pstLin.get("gNroSello1"));
//				 	return false; 
//		        }
//			 
//			 if(!bfnExisteColorSello((String)pstLin.get("gColSello1"))){
//				 
//				 Others.cMnsjErr=String.format("No se encontró registrado/activo el siguiente Color de Sello 1 [%s].",(String)pstLin.get("gColSello1"));
//				 	return false; 
//		        }
//			 
//			 if(!bfnExisteUbicacionSello((String)pstLin.get("gUbiSello1"))){ 
//				 
//				 Others.cMnsjErr=String.format("No se encontró registrado/activa la siguiente Ubicación del Sello 1 [%s].",(String)pstLin.get("gUbiSello1"));
//				 	return false; 
//		        }
//		    }
//		 
//		 if((Long)pstLin.get("gNroSello2") > 0)
//		    {
//			 if(!bfnExisteNroSello((Long)pstLin.get("gNroSello2"))){
//				 
//				 Others.cMnsjErr=String.format("No se encontró registrado/disponible el siguiente Número de Sello 1 [%d].",(Long)pstLin.get("gNroSello2"));
//				 	return false; 
//		        }
//			 
//			 if(!bfnExisteColorSello((String)pstLin.get("gColSello2"))){
//				 
//				 Others.cMnsjErr=String.format("No se encontró registrado/activo el siguiente Color de Sello 1 [%s].",(String)pstLin.get("gColSello2"));
//				 	return false; 
//		        }
//			 
//			 if(!bfnExisteUbicacionSello((String)pstLin.get("gUbiSello2"))){ 
//				 
//				 Others.cMnsjErr=String.format("No se encontró registrado/activa la siguiente Ubicación del Sello 1 [%s].",(String)pstLin.get("gUbiSello2"));
//				 	return false; 
//		        }
//		    }
		logger.info("pArrLectRet: {}", pArrLectRet);
		logger.info("piCantMedRet: {}", piCantMedRet);
		logger.info("piContReti: {}", piContReti);
		/* ANL_20210406 inicio - bucle para todos los medidores retirados */
		for (i = 1; i <= piCantMedRet; i++) {
			iTmpLect = 0;
			for (j = 0; j < piContReti; j++) {
				logger.info("pArrLectRet.get(j).get(iIndicaMedidor): {}", pArrLectRet.get(j).get("iIndicaMedidor"));
				if ((pArrLectRet.get(j).get("iIndicaMedidor") != null
						? Integer.parseInt(pArrLectRet.get(j).get("iIndicaMedidor").toString())
						: -1) == i) {
					chMedidorRet = pArrLectRet.get(j).get("cMedNumero") != null
							? pArrLectRet.get(j).get("cMedNumero").toString()
							: null;
					chModeloMedidorRet = pArrLectRet.get(j).get("cMedModelo") != null
							? pArrLectRet.get(j).get("cMedModelo").toString()
							: null;
					chMarcaMedidorRet = pArrLectRet.get(j).get("cMedMarca") != null
							? pArrLectRet.get(j).get("cMedMarca").toString()
							: null;

					map.put("dValor",
							pArrLectRet.get(j).get("dValor") != null ? pArrLectRet.get(j).get("dValor").toString()
									: "");
					map.put("cCodigo",
							pArrLectRet.get(j).get("cCodigo") != null ? pArrLectRet.get(j).get("cCodigo").toString()
									: "");
					map.put("iIndica", -2);
					ArrLecturaTemp.add(iTmpLect, map);
					iTmpLect++;
				}
			}

			idMedMarca = bfnExisteMarcaMedidor2(chMarcaMedidorRet);
			if (idMedMarca == 0) {
				Others.cMnsjErr = String.format(
						"No se encontró registrado/activo el siguiente código de marca de medidor a retirar [%s].",
						chMarcaMedidorRet);
				return false;
			}

			idMedModeloRet = bfnExisteModeloMedidor2(chModeloMedidorRet, idMedMarca);
			if (idMedModeloRet == 0) {
				Others.cMnsjErr = String.format(
						"No se pudo validar el medidor a Retirar, código de modelo [%s], ID marca [%d].",
						chModeloMedidorRet, idMedMarca);
				return false;
			}

			if (!bfnExisteNumeroMedidor(chMedidorRet, idMedModeloRet, pIDEmpr, Others.gIdServicio, Constants.OPC_R)) {
				Others.cMnsjErr = String.format(
						"No se encontró registrado/activo el siguiente número de medidor a retirar [%s], ID modelo de medidor [%d].",
						chMedidorRet, idMedModeloRet);
				return false;
			}

			for (k = 0; k < iTmpLect; k++) {
				if (!bfnExisteCodigoLectura(pIDEmpr,
						ArrLecturaTemp.get(k).get("cCodigo") != null ? ArrLecturaTemp.get(k).get("cCodigo").toString()
								: null)) /* pArrLectRet */
				{
					Others.cMnsjErr = String.format(
							"No se encontró registrado el siguiente código de lectura del medidor a retirar [%s].",
							(String) ArrLecturaTemp.get(k).get("cCodigo"));
					return false;
				}
			}

			logger.info("chMedidorRet: {}", chMedidorRet);
			logger.info("idMedModeloRet: {}", idMedModeloRet);
			logger.info("idSE_IDServicioRet: {}", idSE_IDServicioRet);

			if (!bfnCorrespondenciaLecturasMedidor2(chMedidorRet, idMedModeloRet, pIDEmpr, idSE_IDServicioRet,
					Constants.OPC_R, ArrLecturaTemp, iTmpLect)) {
				Others.cMnsjErr = String.format(
						"Existen diferencias entre las lecturas del medidor y las lecturas del archivo de entrada para el siguiente número de medidor a retirar [%s], ID modelo de medidor [%d], ID servicio [%d].",
						chMedidorRet, idMedModeloRet, idSE_IDServicioRet);
				return false;
			}

			pstLin.put("gCodMarMedRet", chMarcaMedidorRet);
			pstLin.put("gCodModMedRet", chModeloMedidorRet);
			pstLin.put("gNroMedRet", chMedidorRet);
			Others.cMnsjErr = "";

			// logger.info("sc4j_Others_giFlagFacturacion: {}", Others.giFlagFacturacion);
			// cuando me pierda
			// if(Others.giFlagFacturacion!=1) /* ANL_20220920 */
			// {

			if (!bfnRetirarMedidor2(pIDEmpr, pIdUser, pstLin, ArrLecturaTemp, iTmpLect, idMedModeloRet,
					idSE_IDServicioRet, idOrden)) // ANL_20210406
			{
				Others.cMnsjErr = String.format("No se pudo retirar el siguiente medidor [%s]. %s.", chMedidorRet,
						Others.cMnsjErr);
				return false;
			}

			// } /* ANL_20220920 */
		}
		
		List<Map<String,Object>> lecturasMedidorEncontrado = ArrLecturaTemp; // R.I. Agregado 03/11/2023

		ArrLecturaTemp.clear();
		map.clear();
		i = 0;
		j = 0;
		k = 0;
		iTmpLect = 0;
		idMedMarca = 0;

		/* ANL_20210406 inicio - bucle para todos los medidores instalados */
		for (i = 1; i <= piCantMedInst; i++) {
			iTmpLect = 0;
			for (j = 0; j < piContInst; j++) {
				if ((pArrLectInst.get(j).get("iIndicaMedidor") != null
						? Integer.parseInt(pArrLectInst.get(j).get("iIndicaMedidor").toString())
						: -1) == i) {
					chMedidorInst = pArrLectInst.get(j).get("cMedNumero") != null
							? pArrLectInst.get(j).get("cMedNumero").toString()
							: null;
					chModeloMedidorInst = pArrLectInst.get(j).get("cMedModelo") != null
							? pArrLectInst.get(j).get("cMedModelo").toString()
							: null;
					chMarcaMedidorInst = pArrLectInst.get(j).get("cMedMarca") != null
							? pArrLectInst.get(j).get("cMedMarca").toString()
							: null;

					map.put("dValor",
							pArrLectInst.get(j).get("dValor") != null ? pArrLectInst.get(j).get("dValor").toString()
									: null);
					map.put("cCodigo",
							pArrLectInst.get(j).get("cCodigo") != null ? pArrLectInst.get(j).get("cCodigo").toString()
									: null);
					map.put("iIndica", -2);
					ArrLecturaTemp.add(iTmpLect, map);
					iTmpLect++;
				}
			}

			idMedMarca = bfnExisteMarcaMedidor2(chMarcaMedidorInst);
			if (idMedMarca == 0) {
				Others.cMnsjErr = String.format(
						"No se encontró registrado/activo el siguiente código de marca de medidor a instalar [%s].",
						chMarcaMedidorInst);
				return false;
			}

			idMedModeloInst = bfnExisteModeloMedidor2(chModeloMedidorInst, idMedMarca);
			if (idMedModeloInst == 0) {
				Others.cMnsjErr = String.format(
						"No se pudo validar el medidor a Instalar, código de modelo [%s], ID marca [%d]. [%s]",
						chModeloMedidorInst, idMedMarca);
				return false;
			}

			if (!bfnExisteNumeroMedidor(chMedidorInst, idMedModeloInst, pIDEmpr, 0, Constants.OPC_I)) {
				Others.cMnsjErr = String.format(
						"No se encontró disponible/activo el siguiente número de medidor a instalar [%s], ID modelo de medidor [%d].",
						chMedidorRet, idMedModeloInst);
				return false;
			}
			for (k = 0; k < iTmpLect; k++) {
				if (!bfnExisteCodigoLectura(pIDEmpr,
						ArrLecturaTemp.get(k).get("cCodigo") != null ? ArrLecturaTemp.get(k).get("cCodigo").toString()
								: null)) /* pArrLectRet */
				{
					Others.cMnsjErr = String.format(
							"No se encontró registrado el siguiente código de lectura del medidor a instalar [%s].",
							(String) ArrLecturaTemp.get(k).get("cCodigo"));
					return false;
				}
			}

			if (!bfnCorrespondenciaLecturasMedidor2(chMedidorInst, idMedModeloInst, pIDEmpr, 0, Constants.OPC_I,
					ArrLecturaTemp, iTmpLect)) {
				Others.cMnsjErr = String.format(
						"Existen diferencias entre las lecturas del medidor y las lecturas del archivo de entrada para el siguiente número de medidor a instalar [%s], ID modelo de medidor [%d].",
						chMedidorInst, idMedModeloInst);
				return false;
			}

			pstLin.put("gCodMarMedInst", chMarcaMedidorInst);
			pstLin.put("gCodModMedInst", chModeloMedidorInst);
			pstLin.put("gNroMedInst", chMedidorInst);
			Others.cMnsjErr = "";
			// logger.info("sc4j_Others_giFlagFacturacion: {}", Others.giFlagFacturacion);
			// if(Others.giFlagFacturacion!=1) /* ANL_20220920 */
			// {
			if (!bfnInstalarMedidor2(pIDEmpr, pIdUser, pstLin, ArrLecturaTemp, iTmpLect, idMedModeloInst,
					idOrden, lecturasMedidorEncontrado)) // R.I. Agregado 03/11/2023 /* ANL_20210406 */ // REQ 10 - JEGALARZA
			{
				// sprintf(pMnsjErr, "No se pudo instalar el siguiente medidor [%s]. %s.",
				// chMedidorInst, cMnsjErr);
				Others.cMnsjErr = String.format("No se pudo instalar el siguiente medidor [%s]. %s.", chMedidorInst,
						Others.cMnsjErr);
				return false;
			}
			// } /* ANL_20220920 */
		}
		return true;
	}

	// R.I. Se modifica por completo para obtener todos los datos del plano 31/10/2023
	public boolean bfnCambioDeFactor(Map<String, Object> stMedidor) { // R.I. Modificado String linRespuesta 31/10/2023
		try {
			String factorInstalado = i4jdao.selectMedidorEncontrado((String) stMedidor.get("gCodModMedInst"),
					(String) stMedidor.get("gCodMarMedInst"), (String) stMedidor.get("gNroMedInst"));

			if (factorInstalado != null && !factorInstalado.equals("") && !factorInstalado.equals(stMedidor.get("factorInstalar"))) {
				try {
					i4jdao.updateRegistroMedidores((String) stMedidor.get("gNroMedInst"),
							(String) stMedidor.get("gCodModMedInst"), (String) stMedidor.get("gCodMarMedInst"),
							(String) stMedidor.get("factorInstalar"));
				} catch (Exception e) {
					logger.error("Error: no se pudo actualizar el query ... : " + e);
					return false;
				}
			}
		} catch (Exception e) {
			logger.error("Error: no se pudo actualizar el factor ... : " + e);
			return false;
		}
		
		/*
		 * Others.stRptaMasivaMedidores=new HashMap<>(); String p_nro_legacy = "";
		 * String cCadena=linRespuesta; String[] cadena_split = cCadena.split("\t");
		 * Others.stRptaMasivaMedidores.put("rNroOrden", cadena_split[0]);
		 * 
		 * p_nro_legacy = (String)Others.stRptaMasivaMedidores.get("rNroOrden");
		 * 
		 * try { //ACTUALIZAR MEDIDORES List<MedidoresXML> listaMedidoresXML = new
		 * ArrayList<>();
		 * 
		 * List<Map<String,Object>> map_medidores =
		 * i4jdao.selectRegistroMedidores(p_nro_legacy);
		 * 
		 * if( !map_medidores.isEmpty() ) { for (Map<String, Object> medidores_cursor :
		 * map_medidores) { MedidoresXML medidoresxml = new MedidoresXML();
		 * 
		 * medidoresxml.setAccionMedidor(
		 * medidores_cursor.get("accionMedidor")!=null?medidores_cursor.get(
		 * "accionMedidor").toString():null);
		 * medidoresxml.setMedidor(medidores_cursor.get("numeroMedidor")!=null?
		 * medidores_cursor.get("numeroMedidor").toString():null); //numero_componente
		 * medidoresxml.setFactorMedidor(medidores_cursor.get("factorMedidor")!=null?
		 * medidores_cursor.get("factorMedidor").toString():null);
		 * medidoresxml.setModeloMedidor(medidores_cursor.get("modeloMedidor")!=null?
		 * medidores_cursor.get("modeloMedidor").toString():null);
		 * medidoresxml.setMarcaMedidor(medidores_cursor.get("marcaMedidor")!=null?
		 * medidores_cursor.get("marcaMedidor").toString():null);
		 * 
		 * listaMedidoresXML.add(medidoresxml); } }
		 * 
		 * int contador_list = listaMedidoresXML.size();
		 * 
		 * if( contador_list == 2 ) {
		 * 
		 * String v_accionMedidor_xml_encontrado =
		 * listaMedidoresXML.get(0).getAccionMedidor(); String v_medidor_xml_encontrado
		 * = ""; String v_medidor_xml_instalado = ""; String
		 * v_factorMedidor_xml_encontrado = ""; String v_factorMedidor_xml_instalado =
		 * ""; String v_modeloMedidor_xml_encontrado = ""; String
		 * v_modeloMedidor_xml_instalado = ""; String v_marcaMedidor_xml_encontrado =
		 * ""; String v_marcaMedidor_xml_instalado = "";
		 * 
		 * if( v_accionMedidor_xml_encontrado.equals("Encontrado") ) {
		 * 
		 * v_medidor_xml_encontrado = listaMedidoresXML.get(0).getMedidor();
		 * v_medidor_xml_instalado = listaMedidoresXML.get(1).getMedidor();
		 * v_factorMedidor_xml_encontrado = listaMedidoresXML.get(0).getFactorMedidor();
		 * v_factorMedidor_xml_instalado = listaMedidoresXML.get(1).getFactorMedidor();
		 * v_modeloMedidor_xml_encontrado = listaMedidoresXML.get(0).getModeloMedidor();
		 * v_modeloMedidor_xml_instalado = listaMedidoresXML.get(1).getModeloMedidor();
		 * v_marcaMedidor_xml_encontrado = listaMedidoresXML.get(0).getMarcaMedidor();
		 * v_marcaMedidor_xml_instalado = listaMedidoresXML.get(1).getMarcaMedidor();
		 * 
		 * }else {
		 * 
		 * v_medidor_xml_encontrado = listaMedidoresXML.get(1).getMedidor();
		 * v_medidor_xml_instalado = listaMedidoresXML.get(0).getMedidor();
		 * v_factorMedidor_xml_encontrado = listaMedidoresXML.get(1).getFactorMedidor();
		 * v_factorMedidor_xml_instalado = listaMedidoresXML.get(0).getFactorMedidor();
		 * v_modeloMedidor_xml_encontrado = listaMedidoresXML.get(1).getModeloMedidor();
		 * v_modeloMedidor_xml_instalado = listaMedidoresXML.get(0).getModeloMedidor();
		 * v_marcaMedidor_xml_encontrado = listaMedidoresXML.get(1).getMarcaMedidor();
		 * v_marcaMedidor_xml_instalado = listaMedidoresXML.get(0).getMarcaMedidor();
		 * 
		 * }
		 * 
		 * if( v_factorMedidor_xml_instalado != null ||
		 * !v_factorMedidor_xml_instalado.equals("") ) { try {
		 * 
		 * 
		 * if( v_factorMedidor_xml_encontrado == null ||
		 * v_factorMedidor_xml_encontrado.equals("")) { v_factorMedidor_xml_encontrado =
		 * i4jdao.selectMedidorEncontrado(v_modeloMedidor_xml_encontrado,
		 * v_marcaMedidor_xml_encontrado, v_medidor_xml_encontrado ); }
		 * 
		 * 
		 * String factorInstalado =
		 * i4jdao.selectMedidorEncontrado(v_modeloMedidor_xml_instalado,
		 * v_marcaMedidor_xml_instalado, v_medidor_xml_instalado );
		 * 
		 * //if( v_medidor_xml_encontrado.equals(v_medidor_xml_instalado) &&
		 * !v_factorMedidor_xml_encontrado.equals(v_factorMedidor_xml_instalado) ){
		 * if(!factorInstalado.equals(v_factorMedidor_xml_instalado)){ //UPDATE try {
		 * i4jdao.updateRegistroMedidores(v_medidor_xml_instalado,
		 * v_modeloMedidor_xml_instalado, v_marcaMedidor_xml_instalado,
		 * v_factorMedidor_xml_instalado); } catch (Exception e) {
		 * logger.error("Error: no se pudo actualizar el query ... : "+e); return false;
		 * } }
		 * 
		 * } catch (Exception e) { logger.error("Error: " + e); } }
		 * 
		 * } else { logger.info("Solo existe un item en el TAG Medidor -- SCOM"); }
		 * 
		 * 
		 * }catch (Exception ee){ logger.error("Error: " + ee); }
		 */

		return true;
	}

	private boolean bfnExisteUbicacionSello(String pUbiSello) {
		int iTmpExiste = 0;
		try {
			iTmpExiste = i4jdao.bfnExisteUbicacionSello(pUbiSello);
			if (iTmpExiste < 1) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error("Error en bfnExisteUbicacionSello. Exception:{}", e);
			return false;
		}
	}

	private boolean bfnExisteColorSello(String pColorSello) {
		int iTmpExiste = 0;
		try {
			iTmpExiste = i4jdao.bfnExisteColorSello(pColorSello);
			if (iTmpExiste < 1) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error("Error en bfnExisteColorSello. Exception:{}", e);
			return false;
		}
	}

	private boolean bfnExisteNroSello(Long pNroSello) {
		int iTmpExiste = 0;
		try {
			iTmpExiste = i4jdao.bfnExisteNroSello(pNroSello);
			if (iTmpExiste < 1) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error("Error en bfnExisteNroSello. Exception:{}", e);
			return false;
		}
	}

	private boolean bfnExisteCodigoPropiedadMedidor(String pCodPropMed) {
		int iTmpExiste = 0;
		try {
			iTmpExiste = i4jdao.bfnExisteCodigoPropiedadMedidor(pCodPropMed);
			if (iTmpExiste < 1) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error("Error en bfnExisteCodigoPropiedadMedidor. Exception:{}", e);
			return false;
		}
	}

	private boolean bfnFechaEsMenor(String cFecha1, String cFecha2) {

		logger.info("bfnFechaEsMenor revision sc4j");
		logger.info("bfnFechaEsMenor cFecha1 sc4j: {}", cFecha1);
		logger.info("bfnFechaEsMenor cFecha2 sc4j: {}", cFecha2);

		int dd1, mm1, yyyy1;
		int dd2, mm2, yyyy2;

		String[] fecha1_split = cFecha1.split("/");
		dd1 = Integer.parseInt(fecha1_split[0]);
		mm1 = Integer.parseInt(fecha1_split[1]);
		yyyy1 = Integer.parseInt(fecha1_split[2]);

		String[] cFecha2_split = cFecha2.split("/");
		dd2 = Integer.parseInt(cFecha2_split[0]);
		mm2 = Integer.parseInt(cFecha2_split[1]);
		yyyy2 = Integer.parseInt(cFecha2_split[2]);

		if (yyyy1 < yyyy2) {
			return true;
		} else if (yyyy1 > yyyy2)
			return false;
		else if (mm1 < mm2)
			return true;
		else if (mm1 > mm2)
			return false;
		else if (dd1 < dd2)
			return true;
		else
			return false;
	}

	private boolean bfnObtenerFechaEjecucionVta(Long pCuenta) {
		String fechaEjecucionVta = "";
		try {
			fechaEjecucionVta = i4jdao.bfnObtenerFechaEjecucionVta(pCuenta);
			if (fechaEjecucionVta.equals("")) {
				throw new Exception();
			} else {
				Others.cFechaEjecuVta = fechaEjecucionVta;
				return true;
			}
		} catch (Exception e) {
			logger.error(
					"Error en bfnObtenerFechaEjecucionVta. Error al obtener fecha de ejecución del servicio de ventas .Exception:{}",
					e);
			return false;
		}
	}

	private String carga_FechaHora(int opc) {
		String cadena = "";
		switch (opc) {
		case 1:
			cadena = i4jdao.carga_FechaHora1();
			break;
		case 2:
			cadena = i4jdao.carga_FechaHora2();
			break;
		default:
			cadena = "";
			break;
		}
		return cadena;
	}

	private boolean bfnExisteEjecutor(Integer pIDEmpr, String pCodEjecutor) {
		int iTmpExiste = 0;
		try {
			iTmpExiste = i4jdao.bfnExisteEjecutor(pCodEjecutor, pIDEmpr);
			if (iTmpExiste != 1) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error("Error en bfnExisteEjecutor. Exception:{}", e);
			return false;
		}
	}

	private boolean bfnExisteContratista(Integer pIDEmpresa, String pCodContra) {
		int iTmpExiste = 0;
		try {
			iTmpExiste = i4jdao.bfnExisteContratista(pCodContra, pIDEmpresa);
			if (iTmpExiste != 1) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error("Error en bfnExisteContratista. Exception:{}", e);
			return false;
		}
	}

	private boolean bfnSE_VerificaVenta(Long pCuenta) {
		int iCount;
		try {
			iCount = i4jdao.bfnSE_VerificaVenta(pCuenta);
			if (iCount == 0) {
				return true; /* no hay ventas por modificacion para la cuenta */
			} else {
				/*
				 * Verificar si servicio de venta por modificacion se encuentra en estado
				 * 'Ejecutado'
				 */
				iCount = i4jdao.verifyVentaPorModificaciónEjecutado(pCuenta);
				if (iCount == 0) {
					return false;
				} else {
					return true;
				}
			}
		} catch (Exception e) {
			logger.error("Error en bfnSE_VerificaVenta. Exception:{}", e);
			return false;
		}
	}

	private boolean bfnTieneServicioElectrico(Long pCuenta) {

		try {
			Map<String, Object> map = i4jdao.bfnTieneServicioElectrico(pCuenta);
			if (map == null) {
				throw new Exception();
			} else {
				Others.cSE_Estado_wkf = map.get("cEstadoWkf") != null ? map.get("cEstadoWkf").toString() : null;
				Others.lSE_IDEstado = map.get("lIDEstado") != null ? Integer.parseInt(map.get("lIDEstado").toString())
						: null;
				Others.idSE_IDServicioRet = map.get("idServicio") != null
						? Integer.parseInt(map.get("idServicio").toString())
						: null;
				return true;
			}
		} catch (Exception e) {
			logger.error(
					"Error al verificar servicio eléctrico de la Cuenta {}. Error en bfnTieneServicioElectrico. Exception:{}",
					pCuenta, e);
			return false;
		}
	}

	private boolean bfnCuentaEnFacturacion(Long pCuenta) {
		int iTmpExiste = 0;
		try {
			iTmpExiste = i4jdao.bfnCuentaEnFacturacion(pCuenta);
			if (iTmpExiste < 1) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error("Error en bfnCuentaEnFacturacion. Exception:{}", e);
			return false;
		}
	}

	private boolean bfnExisteCodigoLectura(Integer pIDEmpr, String pCodigo) {
		int iTmpExiste = 0;
		try {
			iTmpExiste = i4jdao.bfnExisteCodigoLectura(pCodigo, pIDEmpr);
			if (iTmpExiste < 1) {
				return false;
			}
		} catch (Exception e) {
			logger.error("Error en bfnExisteCodigoLectura. Exception:{}", e);
			return false;
		}
		return true;
	}

	private boolean bfnExisteCuenta(Integer pIDEmpr, Long pCuenta) {
		int iTmpExiste = 1;
		try {
			iTmpExiste = i4jdao.bfnExisteCuenta(pIDEmpr, pCuenta);
			if (iTmpExiste != 1) {
				return false;
			}
		} catch (Exception e) {
			logger.error("Error en bfnExisteCuenta. Exception:{}", e);
			return false;
		}
		return true;
	}

//	@Transactional
	private boolean bfnInstalarMedidor2(Integer pIDEmpr, Integer pIdUser, Map<String, Object> pstLin,
			List<Map<String, Object>> pArrLectInst, int piContInst, long pidMedModelo, long idOrden, List<Map<String, Object>> lecturasMedidorEncontrado) // R.I. Agregado 03/11/2023
			throws SQLException {
		int i;
		long idEmprGlob = 0;
		String cNroMedInst = (String) pstLin.get("gNroMedInst");
		long lNroCuenta = (Long) pstLin.get("gNroCuenta");
		String messageError = "";
		long idMotivoEstado = 0;
		long idUbiServicio = 0;
		long idDynamicObject = 0;
		long idPropiedad = 0;
		long idComponente = 0;
		long idObject = 0;
		long idHisComponente = 0;
		String cFechaLectura = "";
		long idHisComp = 0;
		long vIdTipMagnitud = 0;
		long vIdEstMagnitud = 0;
		double vdValor = 0;
		String vcCodigo = "";
		long vIdMedida = 0;
		long vIdTipCalculo = 0;
		double vIdFactor = 0;
		long vIdTipo_cons = 0;
		long vICantEnteros = 0;
		long vICantDecimales = 0;
		long xid_servicio = 0;
		long vIdSecMagnitud = 0;
		String cCodEjecutor = "";
		String cCodContratista = "";
		long idNroSello1 = 0;
		long idNroSello2 = 0;
		long idBolsaSello2 = 0;
		String cUbiSello1 = "";
		String cDesSello1 = "";
		String cDesSello2 = "";
		long idColor1 = 0;
		long idColor2 = 0;
		long idBolsaSello1 = 0;
		int iCount = -1;
		String cUbiSello2 = "";
		logger.info("bfnInstalarMedidor2 idOrden: {}", idOrden);

		try {
			// Empresa Global
			idEmprGlob = i4jdao.extractIdEmpresaGlobal();
			if (idEmprGlob == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_INTO_idEmprGlob. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}
			// Ubicación
			idUbiServicio = i4jdao.extractIdServicioInstalado(lNroCuenta);
			if (idUbiServicio == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_Into_idServicio. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			// Estado Instalado
			idMotivoEstado = i4jdao.extractIdMotivoEstadoInstalado();
			if (idMotivoEstado == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_INTO_IdMotivoEstado. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			// 1
			Others.cMnsjErr = String.format(
					"Error en bfnInstalarMedidor_select_into_idDynamicObject. Cuenta [%d]. Revisar Log del Proceso",
					lNroCuenta);
			Map<String, Object> map = i4jdao.selectDataInMedComponenteInstalar(cNroMedInst, pidMedModelo, pIDEmpr);

			idDynamicObject = Long.parseLong(map.get("idDynamicObject").toString());
			// idPropiedad=Long.parseLong(map.get("idPropiedad").toString());
			idComponente = Long.parseLong(map.get("idComponente").toString());
			//
			if (idDynamicObject == -1) /*
										 * Si MED_COMPONENTE.id_dynamicobject = NULL, inserta un registro en DYO_OBJECT
										 */
			{
				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_SQDYNAMICBUSINESSOBJECT. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				idObject = i4jdao.selectNextValSQDYNAMICBUSINESSOBJECT();
				if (idObject == 0) {
					Others.printErrorinFile1.printf(Others.cMnsjErr);
					return false;
				}

				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_INTO_DYO_OBJECT. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				i4jdao.insertDYOOBJECT(idObject, pIDEmpr);
			}

			// 2
			Others.cMnsjErr = String.format(
					"Error en bfnInstalarMedidor_Update_Med_componente. Cuenta [%d]. Revisar Log del Proceso",
					lNroCuenta);
			logger.info("idDynamicObject: {}", idDynamicObject);
			i4jdao.updateMEDCOMPONENTE_Instalar(idDynamicObject, lNroCuenta, (String) pstLin.get("gCodPropMedInst"),
					(String) pstLin.get("gFechaLectura"), idObject, (String) pstLin.get("gNroMedInst"), pidMedModelo);

			idHisComponente = i4jdao.selectMAXMedHisComponente_Inst(idComponente);
			if (idHisComponente == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_max_id_his_componente. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			cFechaLectura = (String) pstLin.get("gFechaLectura");

			Others.cMnsjErr = String.format(
					"Error en bfnInstalarMedidor_Update_med_his_componente. Cuenta [%d]. Revisar Log del Proceso",
					lNroCuenta);
			i4jdao.updateMEDHISPONENTE_INST(cFechaLectura, idHisComponente, idOrden);

			// 3
			Others.cMnsjErr = String.format(
					"Error en bfnInstalarMedidor_sqauditevent. Cuenta [%d]. Revisar Log del Proceso", lNroCuenta);
			// R.I. Correctivo incidencia INC000110799119 19/07/2023 INICIO
			// idHisComp=i4jdao.selectNextValSQAUDITEVENT();
			idHisComp = scomdao.selectNextValSQMEDHISCOMPONENTE();
			// R.I. Correctivo incidencia INC000110799119 19/07/2023 FIN
			if (idHisComp == 0) {
				Others.printErrorinFile1.printf(Others.cMnsjErr);
				return false;
			}

			// 4
			Others.cMnsjErr = String.format(
					"Error en bfnInstalarMedidor_Insert_Med_his_componente. Cuenta [%d]. Revisar Log del Proceso",
					lNroCuenta);
			i4jdao.insertMEDHISCOMPONENTE_INST(idHisComp, idComponente, (String) pstLin.get("gFechaLectura"),
					idUbiServicio, idOrden);

			// 5
			Others.cMnsjErr = String.format(
					"Error en bfnInstalarMedidor_Insert_Fwk_auditevent. Cuenta [%d]. Revisar Log del Proceso",
					lNroCuenta);
			i4jdao.insertFWKAUDITEVENT_INST(idHisComp, idComponente, pIdUser);

			// 6

			vIdTipMagnitud = i4jdao.extractIdMedTipoMagnitud_INST();
			if (vIdTipMagnitud == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_into_idTipMagnitud. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			vIdEstMagnitud = i4jdao.extractIdFacEstMagnitud_INST();
			if (vIdEstMagnitud == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_into_idEstMagnitud. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}
			
			// R.I. Agregado 03/11/2023 INICIO
			// Se valida que todas las medidas del instalado existen en el encontrado mediante el código
		    for (i = 0; i < pArrLectInst.size(); i++) {
		        vcCodigo = (String) pArrLectInst.get(i).get("cCodigo");
		        logger.info("Codigo del Instalado: {}", vcCodigo);
		        boolean codigoEncontrado = false;

		        for (Map<String, Object> lectura : lecturasMedidorEncontrado) {
		        	logger.info("Codigo del Encontrado: {}", lectura.get("cCodigo"));
		            if (lectura.containsKey("cCodigo") && lectura.get("cCodigo").equals(vcCodigo)) {
		                codigoEncontrado = true;
		                break;
		            }
		        }

		        if (!codigoEncontrado) {
		            return false;
		        }
		    }
		    
		    // Se obtienen todas las id_medida del instalado
		    List<Long> idMedidasInstalado = new ArrayList<>();

		    for (i = 0; i < piContInst; i++) {
		        vcCodigo = (String) pArrLectInst.get(i).get("cCodigo");

		        Map<String, Object> mapx = scomdao.selectDataInMoreTables(idComponente, vcCodigo);
		        vIdMedida = Long.parseLong(mapx.get("vIdMedida").toString());

		        idMedidasInstalado.add(vIdMedida);

		    }
		    
		    // Se eliminan todas las medidas diferentes a las que existen en idMedidasInstalado
		    scomdao.eliminarMedidaMedidor(idComponente, idMedidasInstalado);
		    // R.I. Agregado 03/11/2023 FIN

			for (i = 0; i < piContInst; i++) {
				// vdValor= (Double)pArrLectInst.get(i).get("dValor");
				vdValor = Double.parseDouble(pArrLectInst.get(i).get("dValor").toString());
				vcCodigo = (String) pArrLectInst.get(i).get("cCodigo");

				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_into_6. Cuenta [%d]. Código [%s] Valor [%.2f]. Revisar Log del Proceso",
						lNroCuenta, vcCodigo, vdValor);
				Map<String, Object> mapx = i4jdao.selectDataInMoreTables(idComponente, vcCodigo);

				vIdMedida = Long.parseLong(mapx.get("vIdMedida").toString());
				vIdFactor = Double.parseDouble(mapx.get("vIdFactor").toString());
				vIdTipCalculo = Long.parseLong(mapx.get("vIdTipCalculo").toString());
				vIdTipo_cons = Long.parseLong(mapx.get("vIdTipo_cons").toString());
				vICantEnteros = Long.parseLong(mapx.get("vICantEnteros").toString());
				vICantDecimales = Long.parseLong(mapx.get("vICantDecimales").toString());

				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_Into_vIdSecMagnitud. Cuenta [%d]. Código [%s] Valor [%.2f]. Revisar Log del Proceso",
						lNroCuenta, vcCodigo, vdValor);
				Map<String, Object> mapy = i4jdao.selectDataInNucCuentaServicio(lNroCuenta);

				vIdSecMagnitud = Long.parseLong(mapy.get("vIdSecMagnitud").toString());
				xid_servicio = Long.parseLong(mapy.get("xid_servicio").toString());

				logger.info("vIdSecMagnitud: {}", vIdSecMagnitud);
				logger.info("xid_servicio: {}", xid_servicio);
				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_Update_srv_electrico. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				i4jdao.updateSRV_Electrico(vIdSecMagnitud, xid_servicio);

				long secuenciaMedMagnitud = 0;
				try {
					secuenciaMedMagnitud = i4jdao.selectNextValSQMAGNITUDIMPL();
				} catch (Exception e) {
					Others.cMnsjErr = String.format(
							"Error en bfnRetirarMedidor_SQMAGNITUDIMPL. Cuenta [%d]. Revisar Log del Proceso",
							lNroCuenta);
					Others.printErrorinFile1.printf(Others.cMnsjErr);
					return false;
				}

				logger.info("secuenciaMedMagnitud: {}", secuenciaMedMagnitud);
				Others.cMnsjErr = String.format(
						"Error en bfnInstalarMedidor_Insert_Med_magnitud. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				i4jdao.insertMEDMAGNITUD(secuenciaMedMagnitud, pIDEmpr, idComponente, idUbiServicio, vIdMedida, vdValor,
						vIdFactor, vIdTipMagnitud, cFechaLectura, vIdTipCalculo, vIdEstMagnitud, vIdTipo_cons,
						vIdSecMagnitud, vICantEnteros, vICantDecimales);

				// 7
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_delete_from_med_lec_ultima. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				i4jdao.deleteMEDLECULTIMA(idComponente, vIdMedida);

				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_Insert_Med_lec_ultima. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				i4jdao.insertMEDLECULTIMA(secuenciaMedMagnitud, pIDEmpr, vIdMedida, idComponente);
			}

			logger.info("cCodContratista: {}", cCodContratista);
			logger.info("cCodEjecutor: {}", cCodEjecutor);
			cCodContratista = (String) pstLin.get("gCodContratista");
			cCodEjecutor = (String) pstLin.get("gCodEjecutor");

//				conn.commit();
			return true;
		} catch (Exception e) {
			logger.error(Others.cMnsjErr + ", Exception:{}", e);
//		  conn.rollback();
			return false;
		}
	}

//	@Transactional
	private boolean bfnRetirarMedidor2(Integer pIDEmpr, Integer pIdUser, Map<String, Object> pstLin,
			List<Map<String, Object>> pArrLectRet, int piContReti, long pidMedModelo, long pIDServicio, long idOrden)
			throws SQLException {

		logger.info("bfnRetirarMedidor2 sc4j");
		logger.info("pIDEmpr sc4j: {}", pIDEmpr);
		logger.info("pIDEmpr sc4j: {}", pIdUser);
		logger.info("pstLin sc4j: {}", pstLin);
		logger.info("pArrLectRet sc4j: {}", pArrLectRet);
		logger.info("piContReti sc4j: {}", piContReti);
		logger.info("pidMedModelo sc4j: {}", pidMedModelo);
		logger.info("pIDServicio sc4j: {}", pIDEmpr);

		int i;
		String cNroMedRet = (String) pstLin.get("gNroMedRet");
		long lNroCuenta = (Long) pstLin.get("gNroCuenta");
		// String messageError="";
		long idEmprGlob = 0;
		long idMotivoEstado = 0;
		long idDynamicObject = -2;
		long idComponente = 0;
		long idPropiedad = 0;
		long idObject = 0;
		long idHisComponente = 0;
		long idHisComp = 0;

		long vIdTipMagnitud = 0;
		long vIdEstMagnitud = 0;

		String cFechaLectura = "";
		double vdValor = 0;
		String vcCodigo = "";

		long vIdMedida = 0;
		long vIdTipCalculo = 0;
		double vIdFactor = 0;
		long vIdTipo_cons = 0;
		long vICantEnteros = 0;
		long vICantDecimales = 0;

		long xid_servicio = 0;
		long vIdSecMagnitud = 0;
		String cCodEjecutor = "";
		logger.info("bfnRetirarMedidor2 idOrden: {}", idOrden);

		try {
			// Empresa Global
			idEmprGlob = i4jdao.extractIdEmpresaGlobal();
			if (idEmprGlob == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_INTO_idEmprGlob. Cuenta [%d]. Revisar Log del Proceso", lNroCuenta);
				return false;
			}
			// Estado Retirado
			idMotivoEstado = i4jdao.extractIdMotivoEstado();
			if (idMotivoEstado == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_INTO_IdMotivoEstado. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			Others.cMnsjErr = String.format(
					"Error en bfnRetirarMedidor_select_into_idDynamicObject. Cuenta [%d]. Revisar Log del Proceso",
					lNroCuenta);
			Map<String, Object> map = i4jdao.selectDataInMedComponente2(cNroMedRet, pidMedModelo, pIDEmpr, pIDServicio);

			idDynamicObject = Long.parseLong(map.get("idDynamicObject").toString());
			idPropiedad = Long.parseLong(map.get("idPropiedad").toString());
			idComponente = Long.parseLong(map.get("idComponente").toString());

			if (idDynamicObject == -1) /*
										 * Si MED_COMPONENTE.id_dynamicobject = NULL, inserta un registro en DYO_OBJECT
										 */
			{
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_SQDYNAMICBUSINESSOBJECT. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				idObject = i4jdao.selectNextValSQDYNAMICBUSINESSOBJECT();
				if (idObject == 0) {
					Others.printErrorinFile1.printf(Others.cMnsjErr);
					return false;
				}

				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_INTO_DYO_OBJECT. Cuenta [%d]. Revisar Log del Proceso", lNroCuenta);
				i4jdao.insertDYOOBJECT(idObject, pIDEmpr);
			}

			long id_cliente = i4jdao.getIdCliente(lNroCuenta); // PARA EL Artificio adicional hacia 4J
			long id_contratista = i4jdao.getIdContratista((String) pstLin.get("gCodContratista")); // PARA EL Artificio
																									// adicional hacia
																									// 4J
			try {
				i4jdao.updateMEDCOMPONENTE(idDynamicObject, Constants.MED_PROPIEDAD_CLIENTE, id_cliente, id_contratista,
						idObject, (String) pstLin.get("gNroMedRet"), pidMedModelo, pIDEmpr, pIDServicio);
			} catch (Exception e) {
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_Update_Med_componente. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			logger.info("idComponente: {}", idComponente);
			idHisComponente = i4jdao.selectMAXMedHisComponete(idComponente);
			if (idHisComponente == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_max_id_his_componente. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			Others.cMnsjErr = String.format(
					"Error en bfnRetirarMedidor_Update_med_his_componente. Cuenta [%d]. Revisar Log del Proceso",
					lNroCuenta);
			i4jdao.updateMEDHISPONENTE(idHisComponente, idOrden);

			// 3
			Others.cMnsjErr = String.format(
					"Error en bfnRetirarMedidor_sqauditevent. Cuenta [%d]. Revisar Log del Proceso", lNroCuenta);
			// R.I. Correctivo incidencia INC000110799119 19/07/2023 INICIO
			// idHisComp=i4jdao.selectNextValSQAUDITEVENT();
			idHisComp = scomdao.selectNextValSQMEDHISCOMPONENTE();
			// R.I. Correctivo incidencia INC000110799119 19/07/2023 FIN
			if (idHisComp == 0) {
				Others.printErrorinFile1.printf(Others.cMnsjErr);
				return false;
			}

			// 4

			try {
				i4jdao.insertMEDHISCOMPONENTE(idHisComp, idComponente, idPropiedad, Constants.MED_PROPIEDAD_CLIENTE,
						id_cliente, id_contratista, idOrden);
			} catch (Exception e) {
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_Insert_Med_his_componente. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			// 5
			Others.cMnsjErr = String.format(
					"Error en bfnRetirarMedidor_Insert_Fwk_auditevent. Cuenta [%d]. Revisar Log del Proceso",
					lNroCuenta);
			i4jdao.insertFWKAUDITEVENT(idHisComp, idComponente, pIdUser);

			// 6
			vIdTipMagnitud = i4jdao.extractIdMedTipMagnitud();
			if (vIdTipMagnitud == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_into_idTipMagnitud. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			vIdEstMagnitud = i4jdao.extractIdFactEstMagnitud();
			if (vIdEstMagnitud == 0) {
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_into_idEstMagnitud. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				return false;
			}

			cFechaLectura = (String) pstLin.get("gFechaLectura");

			logger.info("piContReti sc4j: {}", piContReti);
			logger.info("pArrLectRet sc4j: {}", pArrLectRet);
			logger.info("Cantidad pArrLectRet sc4j: {}", pArrLectRet.size());

			for (i = 0; i < piContReti; i++) {
				logger.info("pArrLectRet.get({}) sc4j: {}", i, pArrLectRet.get(i));
				logger.info("dValor sc4j: {}", pArrLectRet.get(i).get("dValor"));
				logger.info("cCodigo sc4j: {}", pArrLectRet.get(i).get("cCodigo"));

				// vdValor= (Double)pArrLectRet.get(i).get("dValor"); //aca se cae
				vdValor = Double.parseDouble(pArrLectRet.get(i).get("dValor").toString());
				vcCodigo = (String) pArrLectRet.get(i).get("cCodigo");
				// suensuen
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_into_6. Cuenta [%d]. Código [%s] Valor [%.2f]. Revisar Log del Proceso",
						lNroCuenta, vcCodigo, vdValor);
				Map<String, Object> mapx = i4jdao.selectDataInMoreTables(idComponente, vcCodigo);

				vIdMedida = Long.parseLong(mapx.get("vIdMedida").toString());
				vIdFactor = Double.parseDouble(mapx.get("vIdFactor").toString());
				vIdTipCalculo = Long.parseLong(mapx.get("vIdTipCalculo").toString());
				vIdTipo_cons = Long.parseLong(mapx.get("vIdTipo_cons").toString());
				vICantEnteros = Long.parseLong(mapx.get("vICantEnteros").toString());
				vICantDecimales = Long.parseLong(mapx.get("vICantDecimales").toString());

				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_Into_vIdSecMagnitud. Cuenta [%d]. Código [%s] Valor [%.2f]. Revisar Log del Proceso",
						lNroCuenta, vcCodigo, vdValor);
				Map<String, Object> mapy = i4jdao.selectDataInNucCuentaServicio(lNroCuenta);

				vIdSecMagnitud = Long.parseLong(mapy.get("vIdSecMagnitud").toString());
				xid_servicio = Long.parseLong(mapy.get("xid_servicio").toString());

				logger.info("vIdSecMagnitud: {}", vIdSecMagnitud);
				logger.info("xid_servicio: {}", xid_servicio);

				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_Update_srv_electrico. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				i4jdao.updateSRV_Electrico(vIdSecMagnitud, xid_servicio);

				long secuenciaMedMagnitud = 0;
				try {
					secuenciaMedMagnitud = i4jdao.selectNextValSQMAGNITUDIMPL();
				} catch (Exception e) {
					Others.cMnsjErr = String.format(
							"Error en bfnRetirarMedidor_SQMAGNITUDIMPL. Cuenta [%d]. Revisar Log del Proceso",
							lNroCuenta);
					Others.printErrorinFile1.printf(Others.cMnsjErr);
					return false;
				}

				logger.info("secuenciaMedMagnitud: {}", secuenciaMedMagnitud);
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_Insert_Med_magnitud. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				i4jdao.insertMEDMAGNITUD(secuenciaMedMagnitud, pIDEmpr, idComponente, pIDServicio, vIdMedida, vdValor,
						vIdFactor, vIdTipMagnitud, cFechaLectura, vIdTipCalculo, vIdEstMagnitud, vIdTipo_cons,
						vIdSecMagnitud, vICantEnteros, vICantDecimales);

				// 7
				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_delete_from_med_lec_ultima. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				i4jdao.deleteMEDLECULTIMA(idComponente, vIdMedida);

				Others.cMnsjErr = String.format(
						"Error en bfnRetirarMedidor_Insert_Med_lec_ultima. Cuenta [%d]. Revisar Log del Proceso",
						lNroCuenta);
				i4jdao.insertMEDLECULTIMA(secuenciaMedMagnitud, pIDEmpr, vIdMedida, idComponente);
			}
			logger.info("pstLin.get(gCodEjecutor): {}", pstLin.get("gCodEjecutor"));
			cCodEjecutor = (String) pstLin.get("gCodEjecutor");

//			conn.commit();
			return true;
		} catch (Exception e) {
			logger.error(Others.cMnsjErr + ", Exception:{}", e);
//			conn.rollback();
			return false;
		}
	}

	private boolean bfnCorrespondenciaLecturasMedidor2(String pNroMedidor, long pidMedModelo, Integer pIDEmpresa,
			long pIDServicio, Integer pOpc, List<Map<String, Object>> pArrLect, int piCont) {
		logger.info("bfnCorrespondenciaLecturasMedidor2 sc4j inicio");
		logger.info("bfnCorrespondenciaLecturasMedidor2 pArrLect sc4j: {}", pArrLect);
		logger.info("bfnCorrespondenciaLecturasMedidor2 pArrLect.size sc4j: {}", pArrLect.size());
		int iCount = 0, i = 0, iContF = 0;
		long idTmpComponente = 0;

		logger.info("pNroMedidor: {}", pNroMedidor);
		logger.info("pidMedModelo: {}", pidMedModelo);
		logger.info("pIDEmpresa: {}", pIDEmpresa);
		logger.info("pIDServicio: {}", pIDServicio);

		try {
			if (pOpc == Constants.OPC_R) /* retirar */
			{
				idTmpComponente = i4jdao.getIdComponenteFromMedComponente(pNroMedidor, pidMedModelo, pIDEmpresa,
						pIDServicio);

			} else { /* instalar */

				idTmpComponente = i4jdao.getIdComponenteFromMedComponente(pNroMedidor, pidMedModelo, pIDEmpresa);
			}
			logger.info("idTmpComponente: {}", idTmpComponente);
			List<Map<String, Object>> bfnFetchLecturas2 = i4jdao.selectCursorqLect2(idTmpComponente);
			// cuando me pierda
			if (!bfnFetchLecturas2.isEmpty() || bfnFetchLecturas2 != null) {
				for (Map<String, Object> map : bfnFetchLecturas2) {
					for (i = 0; i < piCont; i++) {
						Map<String, Object> mapx = new HashMap<>();
						if (((String) pArrLect.get(i).get("cCodigo")).equalsIgnoreCase((String) map.get("cod_medida"))
								&& (Integer) pArrLect.get(i).get("iIndica") != -1) {
							iCount++;
							// mapx.put("iIndica", -1);
							// pArrLect.add(i, mapx); // verificado
							pArrLect.get(i).put("iIndica", -1);

						}
					}
					iContF++;
				}
			}
			logger.info("bfnCorrespondenciaLecturasMedidor2 pArrLect sc4j: {}", pArrLect);
			logger.info("bfnCorrespondenciaLecturasMedidor2 pArrLect.size sc4j: {}", pArrLect.size());
			logger.info("bfnCorrespondenciaLecturasMedidor2 sc4j fin");
			if (iContF == iCount && iContF == piCont) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			logger.error("Error: ERROR en bfnCorrespondenciaLecturasMedidor2, Exception:{}", e);
			return false;
		}
	}

}
