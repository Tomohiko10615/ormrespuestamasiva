package com.enel.scom.api.ormrespuestamasiva;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.enel.scom.api.ormrespuestamasiva.util.Others;
import com.enel.scom.api.ormrespuestamasiva.dao.I4jDAO;
import com.enel.scom.api.ormrespuestamasiva.service.GeneralServices;
import com.enel.scom.api.ormrespuestamasiva.service.S4jServices;
import com.enel.scom.api.ormrespuestamasiva.service.SCOMServices;
import com.enel.scom.api.ormrespuestamasiva.util.Constants;


@SpringBootApplication
public class OrmrespuestamasivaApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(OrmrespuestamasivaApplication.class, args);
	}
	
	@Autowired
	GeneralServices services;
	
	@Autowired
	S4jServices s4jservices;
	
	@Autowired
	SCOMServices sCOMservices;
	
	 @Autowired
	 @Qualifier("connORCL")
	 Connection connOrcl;
	 
	 @Autowired
	 @Qualifier("connPGS")
	 Connection connPg;

	// @Autowired
	//  I4jDAO i4jdao;
	
	
	private static final Logger logger = LoggerFactory.getLogger(OrmrespuestamasivaApplication.class);
	
	@Override
	public void run(String... args) throws Exception {
		
		// long idHisComponente=i4jdao.selectMAXMedHisComponete(32167411984306L);

		// if (true)
		// 	System.exit(1);

		// Object objecto = new Object();
		// logger.info("objecto: {}", objecto);
		// objecto = "18271.1";
		// logger.info("objecto: {}", objecto);
		// try {
			
		// 	double valor = Double.parseDouble(objecto.toString());
		// 	// %0.2f
		// 	// %,.2f

		// 	// String var1 = String.format("%0.2f", valor);
		// 	String var2 = String.format("%.2f", valor);
		// 	// String var3 = String.format("%l", valor);
		// 	logger.info(var2);
		// 	int x = 1;
		// } catch (Exception e) {
		// 	logger.error(e.getMessage());
		// 	System.exit(1);
		// }
		// System.exit(0);





		// String cFechaDMY =  s4jservices.carga_FechaHora(2);
		// String gFechaLectura = "2023-02-07";
		// logger.info("gFechaLectura sc4j : {}", gFechaLectura);
		// logger.info("cFechaDMY sc4j: {}", cFechaDMY);
		// logger.info("s4jservices.bfnFechaEsMenor(gFechaLectura, cFechaDMY): {}",  s4jservices.bfnFechaEsMenor(gFechaLectura, cFechaDMY));
		// logger.info("(gFechaLectura).equalsIgnoreCase(cFechaDMY): {}", (gFechaLectura).equalsIgnoreCase(cFechaDMY));
		
		// if(s4jservices.bfnFechaEsMenor(gFechaLectura, cFechaDMY)==false && !(gFechaLectura).equalsIgnoreCase(cFechaDMY))
		// { 
		// 	Others.cMnsjErr=String.format("La Fecha de Lectura [%s] debe ser menor o igual a la Fecha actual [%s]", gFechaLectura, cFechaDMY);
		// 	logger.error(Others.cMnsjErr);
		// 	System.exit(0);
		// }
		
		// logger.info("esNumero true: {}", s4jservices.esNumero("00"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("-1.2"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("-1"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("-1,3"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("-1212"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("1"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("-1"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("-1"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("-100"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("-00"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("00"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("0"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("1.2"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("1.2"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("1"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("1,3"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("1212"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("1"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("1"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("1"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("100"));
		// logger.info("esNumero true: {}", s4jservices.esNumero("00"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("0s0"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("0,0"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("0asdasdas,0"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("0s0"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("0,0"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("0asdasdas,0"));
		// logger.info("esNumero false: {}", s4jservices.esNumero("263362 "));

		// logger.info(" -----------");
		// logger.info(" -----------");
		// logger.info(" -----------");
		// logger.info(" -----------");
		// logger.info(" -----------");
		// logger.info(" -----------");
		// logger.info(" -----------");

		// logger.info("esNumero2 false: {}", s4jservices.esNumero2("-1,2"));
		// logger.info("esNumero2 false: {}", s4jservices.esNumero2("-1,2"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("-1.2"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("-1.2"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("-1"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("-1"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("-1212"));
		// logger.info("esNumero2 false: {}", s4jservices.esNumero2("1,2"));
		// logger.info("esNumero2 false: {}", s4jservices.esNumero2("1,2"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("1.2"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("1.2"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("1"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("1"));
		// logger.info("esNumero2 true: {}", s4jservices.esNumero2("1212"));


	
		// System.exit(1);

		
		// java -jar /binarios/ejecutables/SCOM/eorder/conexiones/ORMRespuestaMasiva.jar EDEL EOR_CNX_RECEP_ORM6005.txt 6005 pe02850937 EORDER
		String proceso = "ORMRespuestaMasiva";

		// borrar/comentar cuando se haga el pase a los servidores linux produccion
		
		//Pruebas
	
		/*
		String odt = "77";
		 args = new String[5];
		 args[0] = "EDEL";
		 //args[1] = String.format("EOR_CNX_RECEP_ORM%s.txt", odt);
		 args[1] = String.format("EOR_CNX_RECEP_ORM_8163642301.txt");
		 args[2] = odt;
		 args[3] = "pe02850937";
		 args[4] = "EORDER"; 
		 */
		
        logger.info(proceso);
		logger.info("Proceso de Respuesta Masiva de ORM - Version 2021-04-06");
		 
		if(args.length!=4 && args.length!=5) {
			logger.info("<Empresa> <Archivo de Entrada> <Nro. de Orden ODT><ID Usuario><opcional origen>");
			System.exit(1);
		}
		

		
		Others.gc_Empresa=args[0];
		Others.gcArchivo=args[1];
		Others.gcODT=args[2];
		Others.gcUser=args[3];
		Others.gcOrigen="";
		
		if(args.length==5) {
			Others.gcOrigen=args[4];
		}
		
		Others.lRegProcesados=0;
		Others.lRegActualizados=0;
		Others.lRegRechazados=0;
		
		Others.gFechaHora=services.carga_FechaHora(1);
		logger.info(String.format("Inicio del Proceso: %s", Others.gFechaHora));
		
		
		if(!services.bfnObtenerEmpresa(Others.gc_Empresa))
		{	
			logger.info("Empresa no válida {}", Others.gc_Empresa);
			System.exit(1);
		}
		
		if(!services.bfnObtenerIdUsuario(Others.gcUser))
		{	
			logger.info("Usuario no válido {}", Others.gcUser);
			System.exit(1);
		}
		
		if(!services.inicializarVariables()){
			logger.info("Error al inicializar variables");
			System.exit(1);
		}

		if(!services.bfnObtenerPath("ORM_REPORTES", Others.gIDEmpresa)){ // Carga cPathSalida
			logger.info("No se encontró la ruta de salida MED_REPORTES");
			System.exit(1);
		}
		logger.info("Others.cPathSalida: {}", Others.cPathSalida);

		
		if(!services.bfnObtenerPath("ORM_REPORTES_IN", Others.gIDEmpresa)){ // Carga cPathEntrada
			logger.info("No se encontró la ruta de salida MED_REPORTES");
			System.exit(1);
		}
		logger.info("Others.cPathEntrada: {}", Others.cPathEntrada);
		

//		Others.cPathSalida="C:\\temp"; //JAAG LOG PRUEBAS 
//		Others.cPathEntrada="C:\\temp";
//		
		// borrar/comentar cuando se haga el pase a los servidores linux produccion
		//Others.cPathSalida = Constants.RUTA_LINUX_DES_LOCAL.concat(Others.cPathSalida);
		// Others.cPathEntrada = Constants.RUTA_LINUX_DES_LOCAL.concat(Others.cPathEntrada);

		logger.info("Others.cPathSalida: {}", Others.cPathSalida);
		logger.info("Others.cPathEntrada: {}", Others.cPathEntrada);


		//Activar post pruebas JAAG
		Others.cArchEntrada =String.format("%s/%s", Others.cPathEntrada,Others.gcArchivo);
		Others.cArchErrores =String.format("%s/ORM_RESPUESTA_MASIVA_ERRORES_%s.txt", Others.cPathSalida,Others.gcODT);
		Others.cArchResumen =String.format("%s/ORM_RESPUESTA_MASIVA_RESUMEN_%s.txt", Others.cPathSalida,Others.gcODT);
		
		//PRUEBAS JAAG
//		Others.cArchEntrada =String.format("%s\\%s", Others.cPathEntrada,Others.gcArchivo);
//		Others.cArchErrores =String.format("%s\\ORM_RESPUESTA_MASIVA_ERRORES_%s.txt", Others.cPathSalida,Others.gcODT);
//		Others.cArchResumen =String.format("%s\\ORM_RESPUESTA_MASIVA_RESUMEN_%s.txt", Others.cPathSalida,Others.gcODT);
//		
		if(!abrirArchivos())
		{
			System.exit(1);
		}
		
		logger.info(String.format("Archivo de Entrada: %s\n", Others.cArchEntrada));
		logger.info(String.format("Archivo de Errores: %s\n", Others.cArchErrores));
		logger.info(String.format("Archivo de Resumen: %s\n", Others.cArchResumen));

		Others.printResumeninFile2.printf("%s","Proceso Respuesta Masiva de Ordenes de Mantenimiento\n");
		Others.printResumeninFile2.printf("Fecha y hora inicio: %s\n", Others.gFechaHora);
		
		logger.info("Realizando proceso de respuesta masiva de órdenes de mantenimiento...");
		Others.glNroLinea=0;
		
		logger.info("Others.gFechaHora: {}", Others.gFechaHora);
		Others.dActual=s4jservices.tASegundos(Others.gFechaHora);
		
		
		 Others.gcLinea="";
		  while((Others.gcLinea =  Others.fArchivo.readLine()) != null) {
			  if(Others.gcLinea.length()<=Constants.LARGO_ORM) {
				  logger.info("Others.gcLinea: {}", Others.gcLinea);
				  Others.vCodOperacion="000";
				  
				  Others.glNroLinea++;
				  Others.lRegProcesados++;
				  
				  Others.gcLinea=limpiarFinDeLinea(Others.gcLinea);
				  limpiarVariables();
				  
				  Others.cMnsjErr="";
				  Others.cMnsjErrObs="";
				  
				  /************************************************************************************************************/
				  //RECORRIDO DE ORDENES DEL CURSOR DE 4J 
				  /************************************************************************************************************/
				  if(Others.gcLinea.contains("flag_SN4J")) {
					  logger.info("flag_SN4J");
					
					  Others.gcLinea=Others.gcLinea.replaceFirst("flag_SN4J\t",""); //Quitamos el flag que identifica de que cursor proviene.
					  
					  if(!services.bfnCargaGrupos(Others.gcLinea))
						{
						  
						  Others.lRegRechazados++;
						  obtenerNroOrden_ORM(Others.linRespuesta);//Obtener nro de orden ORM
						  //s4jservices.guardarTransferenciaOrden(Others.cNroOrdTmp, Others.cMnsjErr);
						  sCOMservices.guardarTransferenciaOrden(Others.cNroOrdTmp, Others.cMnsjErr); //AGREGADO JAAG INDEPE-5569 09/06/2023
						  vLimpiarLineaEntrada();
						  continue;
						}
					  
					  if(!s4jservices.cargarDatosDeRespuesta(Others.linRespuesta))
						{	
						  Others.lRegRechazados++;
						  obtenerNroOrden_ORM(Others.linRespuesta);//Obtener nro de orden ORM
						  
						  //s4jservices.guardarTransferenciaOrden(Others.cNroOrdTmp, Others.cMnsjErr);
						  sCOMservices.guardarTransferenciaOrden(Others.cNroOrdTmp, Others.cMnsjErr); //AGREGADO JAAG INDEPE-5569 09/06/2023
						  vLimpiarLineaEntrada();
						  continue;
						}
					  
					  if(!s4jservices.cargarTareas(Others.linPartidas, Constants.SEP_TAR))
						{
						  Others.lRegRechazados++;
						  // s4jservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
						  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
						  vLimpiarLineaEntrada();
						  continue;
						}
					  
					  //Se modifico el metodo para que se controle los mensajes de error y se Rechace la orden JAAG 15/06/2023
					  if(!s4jservices.validarDatosEspecificos(Others.gIDEmpresa, Others.gIdUser,Others.stRptaMasiva,Others.stTarea,Others.iContTarea))
						{
						  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
						  Others.lRegRechazados++;
						  
						 // s4jservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
						  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
						  vLimpiarLineaEntrada();
						  continue;
						} 
						
					  //Se ha modificado para que controle los errores JAAG 15/06/2023
					  if(!s4jservices.bfnCargaGlobales(Others.stRptaMasiva))
						{
						  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
						  Others.lRegRechazados++;
						  // s4jservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
						  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
						  vLimpiarLineaEntrada();
						  continue;
						}
					  
					  if(!s4jservices.bfnActualizarDatosRespuesta(Others.gIDEmpresa, Others.gIdUser,Others.stRptaMasiva))
						{
						  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
						  Others.lRegRechazados++;
						  connOrcl.rollback();
						  connPg.rollback(); // ANL
						  //s4jservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); //JDFH INDEPE-5569 26052023
						  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); //JDFH INDEPE-5569 26052023
						  vLimpiarLineaEntrada();
						  continue;
						}
					  for(int i=0; i<Others.iContTarea; i++)
						{
						  if(!s4jservices.bfnActualizarDatosDePartidas(Others.gIDEmpresa, Others.gIdUser, Others.stRptaMasiva, Others.stTarea.get(i)))
							{
							  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
							  Others.lRegRechazados++;
							  vLimpiarLineaEntrada();
							  connOrcl.rollback();
							  connPg.rollback(); // ANL
							  Others.iFlagSaleTar=1;
							  break;
							}
						  
						  /*if(Others.gcOrigen.equalsIgnoreCase("")) {
							  if(((String)Others.stTarea.get(i).get("xcContraMed")).equalsIgnoreCase("S")) {
								  if(!s4jservices.cargarDatosDeContraste(Others.linContraste))
									{
									  Others.lRegRechazados++;
									  vLimpiarLineaEntrada();
									  Others.iFlagSaleTar=1;
									  break;
									}
								  if(!s4jservices.validarDatosContraste(Others.gIDEmpresa,Others.gIdUser,Others.stRptaMasiva))
									{
									  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
									  Others.lRegRechazados++;
									  vLimpiarLineaEntrada();
									  Others.iFlagSaleTar=1;
									  Others.vCodOperacion="0003";
									  break;
									}
								  if(!s4jservices.bfnActualizarDatosDeContraste(Others.gIDEmpresa,Others.gIdUser,Others.stRptaMasiva,Others.stTarea.get(i)))
									{
									  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
									  Others.lRegRechazados++;
									  vLimpiarLineaEntrada();
									  Others.iFlagSaleTar=1;
									  Others.vCodOperacion="0003";
									  break;
									}
							  }
						  }*/
						  
						  logger.info("xccambiomed: {}", Others.stTarea.get(i).get("xccambiomed"));
						  if((Others.stTarea.get(i).get("xccambiomed")!=null?(String)Others.stTarea.get(i).get("xccambiomed"):"").equalsIgnoreCase("S")) {
							  
							  Others.linMediFull=String.format("%s\t%s\t%d\t%s", (String)Others.stRptaMasiva.get("rCodContratista"),(String)Others.stRptaMasiva.get("rNombreTecnico"),(Long)Others.stRptaMasiva.get("rNroCuenta"), Others.linMedidor);
							  
							  if(!s4jservices.cargarDatosDeMedidores(Others.linMediFull))
								{
								  Others.lRegRechazados++;
								  vLimpiarLineaEntrada();
								  Others.iFlagSaleTar=1;
								  Others.printErrorinFile1.printf("%s", "Error al cargar datos de medidores");
								  break;
								}
							  if(!s4jservices.bfnActualizarDatosDeCambioMedidor(Others.gIDEmpresa, Others.gIdUser, Others.stRptaMasiva, Others.stMedidor, Others.ArrLecturaRet, Others.iContReti, Others.ArrLecturaInst, Others.iContInst,Others.iCantMedIndvRet ,Others.iCantMedIndvInst))
								{
								  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
								  Others.lRegRechazados++;
								  connOrcl.rollback();
								  connPg.rollback(); // ANL
								  vLimpiarLineaEntrada();
								  Others.iFlagSaleTar=1;
								  break;
								}
							
							  if(!s4jservices.bfnCambioDeMedidor2(Others.gIDEmpresa, Others.gIdUser,Others.stMedidor,Others.ArrLecturaRet,Others.iContReti,Others.ArrLecturaInst,Others.iContInst,Others.iCantMedIndvRet,Others.iCantMedIndvInst, Others.stRptaMasiva))
								{
								  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
								  Others.lRegRechazados++;
								  connOrcl.rollback();
								  connPg.rollback(); // ANL
								  vLimpiarLineaEntrada();
								  Others.iFlagSaleTar=1;
								  break;
								}
							  
							// INICIO - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 
							  if(!s4jservices.bfnCambioDeFactor(Others.stMedidor)){ // R.I. Modificado Others.linRespuesta 31/10/2023
								  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
								  break;
							  }
							//  FIN - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 
							  
								// if(Others.giFlagFacturacion==1) break;  /* ANL_20220920 - Si la cuenta se encuentra en proceso de facturacion, deja de validar las tareas */				
						  }						  
						}
					  
					  
					  if(Others.iFlagSaleTar==1)
						{
						  // s4jservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
						  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
						  continue;
						}

						// /* ANL_20220920 */
						// if(Others.giFlagFacturacion==1)
						// {
						// 	Others.cMnsjErr="";
						// 	connOrcl.rollback();
						// 	s4jservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr);
						// 	continue;
						// }
						// /* ANL_20220920 */
					  
					  //se modifica metodo para controlar el mensaje de error y se rechace la orden JAAG 15/06/2023
					  if(s4jservices.bfnValidarAIC(Others.stRptaMasiva.get("lIdOrden")!=null?Long.parseLong(Others.stRptaMasiva.get("lIdOrden").toString()):null))  /* Si esta asociada a atención AIC */
						{
						  if(!s4jservices.bfnAsignarUsuarioResponsable(Others.stRptaMasiva)) {
							  logger.error(String.format( "%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr));
							  Others.lRegRechazados++;
							  connOrcl.rollback();
							  connPg.rollback(); // ANL
							  vLimpiarLineaEntrada();
							  Others.printErrorinFile1.printf("%s", "Error al Asignar usuario responsable de la etapa atencion relacionada");
							  // s4jservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
							  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
							  continue;
						  }
						  
						}else
						{	
							if(Others.gFlagTarNoEjecu!=1) /* Si todas las tareas se responden como Ejecutadas */
							{
								if(!s4jservices.bfnActualizacionFinalRespuesta(Others.gIDEmpresa, Others.gIdUser, Others.stRptaMasiva))
								{
									 logger.error(String.format( "%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr));
									  Others.lRegRechazados++;
									  connOrcl.rollback();
									  connPg.rollback(); // ANL
									  vLimpiarLineaEntrada();
									  Others.printErrorinFile1.printf("%s", "Error al actualizar Final de Respuesta");
									  // s4jservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
									  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
									  continue;
								}
							}
							
						}				  
					  
					   Others.cMnsjErr=""; // AGREGADO JAAG 15/06/2023 Ordenes se rechazan 
					  //s4jservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
					  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr); // AGREGADO JAAG INDEPE-5569 09/06/2023
//					  connOrcl.commit();  /* ANL_PROBLEMA */
					
					  Others.lRegActualizados++;
					  vLimpiarLineaEntrada();
				  }else {
				  /************************************************************************************************************/
				  //RECORRIDO DE ORDENES DEL CURSOR DE SCOM 
				  /************************************************************************************************************/
					  if(Others.gcLinea.contains("flag_SCOM")) {
						  logger.info("flag_SCOM");
						  Others.gcLinea=Others.gcLinea.replaceFirst("flag_SCOM\t",""); //Quitamos el flag que identifica de que cursor proviene.
						  
						  if(!services.bfnCargaGrupos(Others.gcLinea))
							{
							  Others.lRegRechazados++;
							  obtenerNroOrden_ORM(Others.linRespuesta);//Obtener nro de orden ORM
							  sCOMservices.guardarTransferenciaOrden(Others.cNroOrdTmp, Others.cMnsjErr);
							  vLimpiarLineaEntrada();
							  continue;
							}//OBTENER VALORES ARCHIVO PLANO
						  if(!sCOMservices.cargarDatosDeRespuesta(Others.linRespuesta))
							{	
							  Others.lRegRechazados++;
							  obtenerNroOrden_ORM(Others.linRespuesta);//Obtener nro de orden ORM
							  sCOMservices.guardarTransferenciaOrden(Others.cNroOrdTmp, Others.cMnsjErr);
							  vLimpiarLineaEntrada();
							  continue;
							}
						  if(!sCOMservices.cargarTareas(Others.linPartidas, Constants.SEP_TAR))
							{
							  Others.lRegRechazados++;
							  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr);
							  vLimpiarLineaEntrada();
							  continue;
							}
						  
						  if(!sCOMservices.validarDatosEspecificos(Others.gIDEmpresa, Others.gIdUser,Others.stRptaMasiva,Others.stTarea,Others.iContTarea))
							{
							  
							  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
							  Others.lRegRechazados++;
							  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr);
							  vLimpiarLineaEntrada();
							  continue;
							}
						  
						  if(!sCOMservices.bfnCargaGlobales(Others.stRptaMasiva))
							{
							  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
							  Others.lRegRechazados++;
							  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr);
							  vLimpiarLineaEntrada();
							  continue;
							}
						  
						  if(!sCOMservices.bfnActualizarDatosRespuesta(Others.gIDEmpresa, Others.gIdUser,Others.stRptaMasiva))
							{
							  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
							  Others.lRegRechazados++;
							  connPg.rollback();  /* ANL_PROBLEMA - se descomenta */
							  connOrcl.rollback(); // ANL
							  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr);
							  vLimpiarLineaEntrada();
							  continue;
							}
						  
						  for(int i=0; i<Others.iContTarea; i++)
							{
							  if(!sCOMservices.bfnActualizarDatosDePartidas(Others.gIDEmpresa, Others.gIdUser, Others.stRptaMasiva, Others.stTarea.get(i)))
								{
								  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
								  Others.lRegRechazados++;
								  connPg.rollback(); // ANL
								  connOrcl.rollback(); // ANL
								  vLimpiarLineaEntrada();
								  Others.iFlagSaleTar=1;
								  break;
								}
							  
							  /*if(Others.gcOrigen.equalsIgnoreCase("")) {
								  if(((String)Others.stTarea.get(i).get("xcContraMed")).equalsIgnoreCase("S")) {
									  if(!s4jservices.cargarDatosDeContraste(Others.linContraste))
										{
										  Others.lRegRechazados++;
										  vLimpiarLineaEntrada();
										  Others.iFlagSaleTar=1;
										  break;
										}
									  if(!s4jservices.validarDatosContraste(Others.gIDEmpresa,Others.gIdUser,Others.stRptaMasiva))
										{
										  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
										  Others.lRegRechazados++;
										  vLimpiarLineaEntrada();
										  Others.iFlagSaleTar=1;
										  Others.vCodOperacion="0003";
										  break;
										}
									  if(!s4jservices.bfnActualizarDatosDeContraste(Others.gIDEmpresa,Others.gIdUser,Others.stRptaMasiva,Others.stTarea.get(i)))
										{
										  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
										  Others.lRegRechazados++;
										  vLimpiarLineaEntrada();
										  Others.iFlagSaleTar=1;
										  Others.vCodOperacion="0003";
										  break;
										}
								  }
							  }*/
							  
							  
							  logger.info("xccambiomed: {}", Others.stTarea.get(i).get("xccambiomed"));
							  if((Others.stTarea.get(i).get("xccambiomed")!=null?(String)Others.stTarea.get(i).get("xccambiomed"):"").equalsIgnoreCase("S")) {
								  Others.linMediFull=String.format("%s\t%s\t%d\t%s", (String)Others.stRptaMasiva.get("rCodContratista"),(String)Others.stRptaMasiva.get("rNombreTecnico"),(Long)Others.stRptaMasiva.get("rNroCuenta"), Others.linMedidor);
								  
								  if(!sCOMservices.cargarDatosDeMedidores(Others.linMediFull))
									{
									  Others.lRegRechazados++;
									  vLimpiarLineaEntrada();
									  Others.iFlagSaleTar=1;
									  Others.printErrorinFile1.printf("%s", "Error al cargar datos de medidores");
									  break;
									}
								  if(!sCOMservices.bfnActualizarDatosDeCambioMedidor(Others.gIDEmpresa, Others.gIdUser, Others.stRptaMasiva, Others.stMedidor, Others.ArrLecturaRet, Others.iContReti, Others.ArrLecturaInst, Others.iContInst,Others.iCantMedIndvRet ,Others.iCantMedIndvInst))
									{
									  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
									  Others.lRegRechazados++;
									  connPg.rollback(); // ANL
									  connOrcl.rollback(); // ANL
									  vLimpiarLineaEntrada();
									  Others.iFlagSaleTar=1;
									  break;
									}
								
									
								  if(!sCOMservices.bfnCambioDeMedidor2(Others.gIDEmpresa, Others.gIdUser,Others.stMedidor,Others.ArrLecturaRet,Others.iContReti,Others.ArrLecturaInst,Others.iContInst,Others.iCantMedIndvRet,Others.iCantMedIndvInst, Others.stRptaMasiva))
									{
									  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
									  Others.lRegRechazados++;
									  connPg.rollback(); // ANL
									  connOrcl.rollback(); // ANL
									  vLimpiarLineaEntrada();
									  Others.iFlagSaleTar=1;
									  break;
									}
								// INICIO - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 
								  if(!sCOMservices.bfnCambioDeFactor(Others.stMedidor)){ // R.I. Modificado Others.linRespuesta 31/10/2023
									  Others.printErrorinFile1.printf("%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr);
									  break;
								  }
								// FIN - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 
							  }							  
							}
						  
						  if(Others.iFlagSaleTar==1)
							{
							  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr);
//							  sCOMservices.devolverEstadoWorkflow(Others.stRptaMasiva); // AGREGADO RSOSA INDEPE-5569 30/06/2023  /* ANL */
							  continue;
							}
						  
						  /*if(sCOMservices.bfnValidarAIC((Long)Others.stRptaMasiva.get("lIdOrden")))   //Si esta asociada a atención AIC 
							{
							  if(!sCOMservices.bfnAsignarUsuarioResponsable(Others.stRptaMasiva)) {
								  logger.error(String.format( "%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr));
								  Others.lRegRechazados++;
								  vLimpiarLineaEntrada();
								  Others.printErrorinFile1.printf("%s", "Error al Asignar usuario responsable de la etapa atencion relacionada");
								  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr);
								  continue;
							  }
							  
							}else
							{*/	
								if(Others.gFlagTarNoEjecu!=1) /* Si todas las tareas se responden como Ejecutadas */
								{
									if(!sCOMservices.bfnActualizacionFinalRespuesta(Others.gIDEmpresa, Others.gIdUser, Others.stRptaMasiva))
									{
										 logger.error(String.format( "%s\tLínea [%d]. %s\n", Others.gcLinea, Others.glNroLinea, Others.cMnsjErr));
										  Others.lRegRechazados++;
										  connPg.rollback();  /* ANL_PROBLEMA - se descomenta */
										  connOrcl.rollback(); // ANL
										  vLimpiarLineaEntrada();
										  Others.printErrorinFile1.printf("%s", "Error al actualizar Final de Respuesta");
										  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr);
										  continue;
									}
								}
								
							/*}*/
					  
						  Others.cMnsjErr="";
						  sCOMservices.guardarTransferenciaOrden((String)Others.stRptaMasiva.get("rNroOrden"), Others.cMnsjErr);
						 // connPg.commit();
						  Others.lRegActualizados++;
						  vLimpiarLineaEntrada();
					  } 
				  }
			  }
		  }
		  
		  Others.gFechaHora=services.carga_FechaHora(1);
		  Others.printResumeninFile2.printf("Fecha y hora fin   : %s\n\n", Others.gFechaHora);
		  Others.printResumeninFile2.printf("Archivo de Entrada: %s\n", Others.cArchEntrada);
		  Others.printResumeninFile2.printf("Archivo de Errores: %s\n\n", Others.cArchErrores);
		  Others.printResumeninFile2.printf("Archivo de Errores: %s\n\n", Others.cArchErrores);
		  Others.printResumeninFile2.printf("Cantidad Registros Actualizados: %d\n", Others.lRegActualizados);
		  Others.printResumeninFile2.printf("Cantidad Registros Rechazados  : %d\n", Others.lRegRechazados);
		  
		  logger.info(String.format("Cantidad Registros Procesados  : %d\n", Others.lRegProcesados));
		  logger.info(String.format("Cantidad Registros Actualizados: %d\n", Others.lRegActualizados));
		  logger.info(String.format("Cantidad Registros Rechazados  : %d\n", Others.lRegRechazados));
		  logger.info(String.format("Salida del Proceso: %s\n", Others.gFechaHora));
		  
		  if(Others.fArchivo != null) {
			     Others.fArchivo.close();
			   }
			  
		  Others.fileErrorWriter.close();
		  Others.fileResumenWriter.close();
		  logger.info("Cerrando Programa");
		  System.exit(0);
	}



	private void vLimpiarLineaEntrada() {
		Others.gcLinea="";
	}

	private void obtenerNroOrden_ORM(String linRespuesta) {
		Others.cNroOrdTmp="";
		String cCadena=linRespuesta;
		String[] cadena_split = cCadena.split("\t");
		for (int i=0; i<cadena_split.length; i++) {
			if(i == 0) {
				Others.cNroOrdTmp=cadena_split[i];
			}
		}
	}
	private void limpiarVariables() {
		
		//Líneas
		Others.linRespuesta="";
		Others.linPartidas="";
		Others.linMedidor="";
		Others.linMediFull="";
		Others.linContraste="";
		
		Others.iContReti=0;
		Others.iContInst=0;
		Others.iContTarea=0;
		Others.dEjecuc=null;
		Others.iFlagSaleTar=0;
		Others.iCantMedIndvRet=0;
		Others.iCantMedIndvInst=0;

		// Others.gcMensjFacturacion = "";  /* ANL_20220920 */
		// Others.giFlagFacturacion=0;            /* ANL_20220920 */
	}

	private String limpiarFinDeLinea(String pCadena) {
		String cTmp;
		String[] ptr;

		cTmp=pCadena;
		ptr = cTmp.split("\r\n\r");
		return ptr[0];
	}

	private boolean abrirArchivos() {

		 String nameFileCurrentExecution="";
			try {
				  nameFileCurrentExecution=Others.cArchEntrada;
				  Others.fArchivo = new BufferedReader(new FileReader(nameFileCurrentExecution));

				  nameFileCurrentExecution=Others.cArchErrores;
				  //Abro stream, crea el fichero si no existe
				  Others.fileErrorWriter=new FileWriter(nameFileCurrentExecution);
				  Others.printErrorinFile1 = new PrintWriter(Others.fileErrorWriter);			  
				  
				  nameFileCurrentExecution=Others.cArchResumen;
				  //Abro stream, crea el fichero si no existe
				  Others.fileResumenWriter=new FileWriter(nameFileCurrentExecution);
				  Others.printResumeninFile2 = new PrintWriter(Others.fileResumenWriter);
				  
				  return true;
		        } catch (FileNotFoundException ex) {
		        	logger.error("Error: Fichero {} no encontrado, error:{} ",nameFileCurrentExecution,ex);
		            return false;
		        }catch (IOException e) {
		        	logger.error(String.format("No se pudo generar el archivo en método abrirArchivos [%s]: %s",
		        			nameFileCurrentExecution, e.getMessage()));
		        	return false;
		        }
	
	}

}
