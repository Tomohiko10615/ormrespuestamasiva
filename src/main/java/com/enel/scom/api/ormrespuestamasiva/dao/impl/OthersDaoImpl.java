package com.enel.scom.api.ormrespuestamasiva.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enel.scom.api.ormrespuestamasiva.dao.IOthersDAO;
import com.enel.scom.api.ormrespuestamasiva.mapper.OthersMapper;

@Repository
public class OthersDaoImpl implements IOthersDAO{
	
	@Autowired
	@Qualifier("postgreTemplate")
	private JdbcTemplate jdbcPostgreTemplate;

	private static final Logger logger = LoggerFactory.getLogger(OthersDaoImpl.class);

	
	@Override
	public Integer bfnVerificaEmpresa(String gc_Empresa) {
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_VERIFICA_EMPRESA;
			logger.debug("Executing query OthersMapper.SQL_SELECT_VERIFICA_EMPRESA");
        	return jdbcPostgreTemplate.queryForObject(sqlString, Integer.class,gc_Empresa);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  bfnVerificaEmpresa: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  bfnVerificaEmpresa: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public Integer bfnVerificaEmpresaGlobal() {
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_VERIFICA_EMPRESA_GLOBAL;
			logger.debug("Executing query OthersMapper.SQL_SELECT_VERIFICA_EMPRESA_GLOBAL");
        	return jdbcPostgreTemplate.queryForObject(sqlString, Integer.class);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  bfnVerificaEmpresaGlobal: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  bfnVerificaEmpresaGlobal: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public Integer bfnObtenerIdUsuario(String gcUser) {
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_OBTENER_ID_USUARIO;
			logger.debug("Executing query OthersMapper.SQL_SELECT_OBTENER_ID_USUARIO");
        	return jdbcPostgreTemplate.queryForObject(sqlString, Integer.class,gcUser);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  bfnObtenerIdUsuario: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  bfnObtenerIdUsuario: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public Integer getCODESTPRECE() {
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_COD_EST_PRECE;
			logger.debug("Executing query OthersMapper.SQL_SELECT_COD_EST_PRECE");
        	return jdbcPostgreTemplate.queryForObject(sqlString, Integer.class);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  getCODESTPRECE: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  getCODESTPRECE: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public Integer getCODESTRECEP() {
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_COD_EST_RECEP;
			logger.debug("Executing query OthersMapper.SQL_SELECT_COD_EST_RECEP");
        	return jdbcPostgreTemplate.queryForObject(sqlString, Integer.class);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  getCODESTRECEP: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  getCODESTRECEP: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public Integer getCODESTRECER() {
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_COD_EST_RECER;
			logger.debug("Executing query OthersMapper.SQL_SELECT_COD_EST_RECER");
        	return jdbcPostgreTemplate.queryForObject(sqlString, Integer.class);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  getCODESTRECER: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  getCODESTRECER: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public Map<String, String> bfnObtenerErrores(String pkey) {
		Map<String, String> map = new HashMap<>();
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_OBTENER_ERRORES;
			logger.debug("Executing query OthersMapper.SQL_SELECT_OBTENER_ERRORES");
			List<Map<String, Object>> results = jdbcPostgreTemplate.queryForList(sqlString,pkey);
			for (Map<String, Object> map2 : results) {
				  map.put("VALOR_ALF", (String) map2.get("VALOR_ALF"));
				  map.put("DESCRIPCION", (String) map2.get("DESCRIPCION"));
			}
			return map;
        }  catch (EmptyResultDataAccessException ee) {
        	 logger.error("Error al obtener Estado de Transferencia {}: {}",pkey,ee.getMessage());
            throw ee;
        } catch (DataAccessException e) {
            logger.error("Error al obtener Estado de Transferencia {}: {}",pkey,e.getMessage());
            throw e;
        }
	}

	@Override
	public String bfnObtenerPath(String pKey, Integer gIDEmpresa) {
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_OBTENER_PATH;
			logger.debug("Executing query OthersMapper.SQL_SELECT_OBTENER_PATH");
        	return jdbcPostgreTemplate.queryForObject(sqlString, String.class,pKey);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  bfnObtenerPath: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  bfnObtenerPath: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public String carga_FechaHora1() {
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_CARGA_FECHA_1;
			logger.debug("Executing query OthersMapper.SQL_SELECT_CARGA_FECHA_1");
        	return jdbcPostgreTemplate.queryForObject(sqlString, String.class);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  carga_FechaHora1: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  carga_FechaHora1: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public String carga_FechaHora2() {
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_CARGA_FECHA_2;
			logger.debug("Executing query OthersMapper.SQL_SELECT_CARGA_FECHA_2");
        	return jdbcPostgreTemplate.queryForObject(sqlString, String.class);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  carga_FechaHora2: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  carga_FechaHora2: {}",e.getMessage());
            throw e;
        }
	}
}
