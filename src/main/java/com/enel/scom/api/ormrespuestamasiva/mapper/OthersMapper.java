package com.enel.scom.api.ormrespuestamasiva.mapper;

public class OthersMapper {

	public static final String SQL_SELECT_VERIFICA_EMPRESA = "SELECT id_empresa " 
			+ " FROM SCHSCOM.nuc_empresa "
		    + " WHERE cod_partition = ?";
	
	public static final String SQL_SELECT_VERIFICA_EMPRESA_GLOBAL = "SELECT id_empresa " 
			+ " FROM SCHSCOM.nuc_empresa "
		    + " WHERE cod_partition = 'GLOB'";

	public static final String SQL_SELECT_OBTENER_ID_USUARIO = "SELECT id"
			+ " FROM SCHSCOM.USUARIO"
			+ " WHERE username = ?";

	public static final String SQL_SELECT_COD_EST_PRECE = "SELECT VALOR_NUM "
			+ "    FROM SCHSCOM.COM_PARAMETROS PAR"
			+ "    WHERE SISTEMA='EORDER'"
			+ "    AND ENTIDAD = 'ESTADO_TRANSFERENCIA'"
			+ "    AND CODIGO = 'PRECE' ;";

	public static final String SQL_SELECT_COD_EST_RECEP = "SELECT VALOR_NUM "
			+ "    FROM SCHSCOM.COM_PARAMETROS PAR"
			+ "    WHERE SISTEMA='EORDER'"
			+ "    AND ENTIDAD = 'ESTADO_TRANSFERENCIA'"
			+ "    AND CODIGO = 'RECEP'";

	public static final String SQL_SELECT_COD_EST_RECER = "SELECT VALOR_NUM "
			+ "    FROM SCHSCOM.COM_PARAMETROS PAR"
			+ "    WHERE SISTEMA='EORDER'"
			+ "    AND ENTIDAD = 'ESTADO_TRANSFERENCIA'"
			+ "    AND CODIGO = 'RECER'";

	public static final String SQL_SELECT_OBTENER_ERRORES = "SELECT VALOR_ALF, DESCRIPCION " +
            "FROM SCHSCOM.COM_PARAMETROS " +
            "WHERE SISTEMA = 'EORDER' " +
            "AND ENTIDAD = 'ESTADO_ERRSYN' " +
            "AND CODIGO  = ? ";

	public static final String SQL_SELECT_OBTENER_PATH = " SELECT path "
			+ "     FROM SCHSCOM.COM_PATH "
			+ "     WHERE key = ? ";
//			+ "     AND id_empresa = ? ";

	public static final String SQL_SELECT_CARGA_FECHA_1 = "SELECT TO_CHAR(NOW(),'DD/MM/YYYY HH24:MI:SS')";

	public static final String SQL_SELECT_CARGA_FECHA_2 = "SELECT TO_CHAR(NOW(),'DD/MM/YYYY')";

}
