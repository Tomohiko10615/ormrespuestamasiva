package com.enel.scom.api.ormrespuestamasiva.config;


import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class ScomDataSourceJDBCConfiguration {
	
	public static final BeanDataSource beanConn = DataSourceConfig.getConnectionDatos("POST");

	/*@Bean
	@Primary
	@ConfigurationProperties("postgres.datasource")
	public DataSourceProperties  postgresDataSourceProperties() {
		 return new DataSourceProperties();
	}	
		@Bean(name = "postgresJdbcTemplateAlternativo")
		@DependsOn("postgresDatasource")
		public JdbcTemplate jdbcTemplate(@Qualifier("postgresDatasource") DataSource dsPost) {
			return new JdbcTemplate(dsPost);
		}
	*/
	// postgresDatasource
	@Bean(name = "postgreDataSource") 
	@Primary
	//@ConfigurationProperties("postgres.datasource")
	public DataSource postgreDataSource() {
		
		HikariConfig dataSourceBuilder = new HikariConfig();
        dataSourceBuilder.setDriverClassName(beanConn.getDbDriver());
        dataSourceBuilder.setJdbcUrl(beanConn.getDbURL());
        dataSourceBuilder.setUsername(beanConn.getDbUser());
        dataSourceBuilder.setPassword(beanConn.getDbPass());        
        dataSourceBuilder.setMaximumPoolSize(beanConn.getDbMax());
        dataSourceBuilder.setMinimumIdle(beanConn.getDbMin());
        
        HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
        return dataSource;
		 //return postgresDataSourceProperties().initializeDataSourceBuilder().build();
	}
		
	@Autowired
	DataSourceConfig dataSourcePost;

	@Bean(name = "postgreTemplate")
	@DependsOn("postgreDataSource")
	public JdbcTemplate jdbcTemplate(@Qualifier("connPGS")Connection conn) throws SQLException {
		conn.setAutoCommit(false); /* ANL_PROBLEMA - Se descomenta */
		return new JdbcTemplate(new SingleConnectionDataSource(conn, true));
	}

	@Bean(name = "connPGS")
	Connection registrarConnectionPostgre(@Qualifier("postgreDataSource")DataSource postgreDataSource) throws SQLException {
		return postgreDataSource.getConnection();
	}

			
			
	/*@Bean
	@ConfigurationProperties("post.datasource")
	public DataSourceProperties  postgresDataSourceProperties2() {
		 return new DataSourceProperties();
	}

	@Bean(name = "postgresDatasource2") 
	@ConfigurationProperties("post.datasource")
	public DataSource postgresDataSource2() {
		 return postgresDataSourceProperties2().initializeDataSourceBuilder().build();
	}
	
	@Bean(name = "namedParameterJdbcTemplate")
	@DependsOn("postgresDatasource2")
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Qualifier("postgresDatasource2") DataSource dsOracle) {
		return new NamedParameterJdbcTemplate(dsOracle);
	}*/
	
	// @Bean(name = "postgresDatasource2") 
	// @Primary
	// public DataSource postgresDataSource2() {
		
	// 	HikariConfig dataSourceBuilder = new HikariConfig();
    //     dataSourceBuilder.setDriverClassName(beanConn.getDbDriver());
    //     dataSourceBuilder.setJdbcUrl(beanConn.getDbURL());
    //     dataSourceBuilder.setUsername(beanConn.getDbUser());
    //     dataSourceBuilder.setPassword(beanConn.getDbPass());        
    //     dataSourceBuilder.setMaximumPoolSize(beanConn.getDbMax());
    //     dataSourceBuilder.setMinimumIdle(beanConn.getDbMin());
        
    //     HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
    //     return dataSource;
	// }

	// @Bean(name = "namedParameterJdbcTemplate")
	// @DependsOn("postgresDatasource2")
	// public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
	// 	return new NamedParameterJdbcTemplate(new SingleConnectionDataSource(beanConn.getConexion(), true));
	// }
}
