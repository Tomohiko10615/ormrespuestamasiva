package com.enel.scom.api.ormrespuestamasiva.util;

import lombok.Data;

//INICIO - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 

@Data
public class MedidoresXML {
    private String accionMedidor;
    private String medidor;
    private String factorMedidor;   
    private String modeloMedidor;
    private String marcaMedidor;   
    
}


//FIN - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 