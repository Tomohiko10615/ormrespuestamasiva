package com.enel.scom.api.ormrespuestamasiva.util;

public class Constants {
	 /* DEFINES PUBLICOS ***********************************************************/
		public static final String PROCESO    =    "ORMRespuestaMasiva";

		public static final Integer LARGO_ORM  =     6005 ;    //Largo línea de entrada para respuesta masiva de ORM
		public static final Integer LARGO_GRUPO  =   1500 ;     /* ANL_20210406 se aumento 200 */
		public static final Integer MAX_TAREAS  =    10   ;

		public static final String LIS_PARENTESCO = "T I V P N M";  // T=Titular I=Inquilino V=Vecino P=Pariente N=Ninguno M=Migracion
		public static final String LIS_NO_EJECUC =  "O F P";    // O=Oposición F=Falta Material P=Proceso Facturación

		public static final Integer DISCO_ATRACADO=  1;
		public static final Integer DISCO_CONFORME = 2;

		public static final String SEP_GRP   =      "#"  ;      // Separador de grupos de datos
		public static final String SEP_TAR     =    "&"  ;      // Separador de tareas
		public static final String SEP_DAT    =     "\t" ;     // Separador de datos
		public static final String SEP_LEC    =     "|"  ;     // Separador de lecturas
		public static final String SEP_CML     =    "*"  ;     // Separador de campos de lectura

		public static final Integer OPC_R = 1; 					// Opción Retirar
		public static final Integer OPC_I = 2; 					// Opción Instalar

		public static final Integer ESTADO_SE_MODIFICACION = 5;	// SRV_ELECT_ESTADO.COD_INTERNO = 'Modificacion'
		public static final Integer MED_PROPIEDAD_CLIENTE = 25; // MED_PROPIEDAD.COD_PROPIEDAD = 'CLIE'

		public static final String RUTA_LINUX_DES_LOCAL = "D:/cfdiaze/indra/linux";

}
