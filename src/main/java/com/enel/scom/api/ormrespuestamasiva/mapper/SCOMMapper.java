package com.enel.scom.api.ormrespuestamasiva.mapper;

public class SCOMMapper {
	public static final String SQL_SELECT_DATOS_DE_REGISTRO = "SELECT ID_ORD_TRANSFER, " //(SCHSCOM)
			+ " NRO_RECEPCIONES, COD_TIPO_ORDEN_EORDER "
			+ " FROM SCHSCOM.EOR_ORD_TRANSFER "
			+ " WHERE "
			+ " COD_TIPO_ORDEN_EORDER IN ('NCX.03','NCX.05') "
			+ " AND NRO_ORDEN_LEGACY = ? ";

	public static final String SQL_ACTUALIZAR_EOR_ORD_TRANSFER = "UPDATE SCHSCOM.EOR_ORD_TRANSFER " //(SCHSCOM)
			+ " SET fec_operacion = now() "
			+ " , COD_OPERACION = ? "
			+ " , OBSERVACIONES = ? "
			+ " , COD_ESTADO_ORDEN = ("
			+ " CASE WHEN ? = ? THEN ? "
			+ " ELSE ? "
			+ " END ) "
			+ " WHERE ID_ORD_TRANSFER = ? ";

	public static final String SQL_ACTUALIZAR_EOR_ORD_TRANSFER_DET = "UPDATE SCHSCOM.EOR_ORD_TRANSFER_DET" //(SCHSCOM)
			+ " SET cod_error = ? "
			+ " WHERE ID_ORD_TRANSFER = ? "
			+ " AND accion = 'RECEPCION'"
			+ " AND NRO_EVENTO = ? ";

	public static final String SQL_GET_ID_MOT_NO_EJE = " SELECT id_motivo_no_ej "  //(CONVERTIDO A SCOM)
			+ " FROM ORM_MOTIVO_NO_EJ "
			+ " WHERE code = ? ";

	public static final String SQL_BFN_EXISTE_ORDEN = " SELECT ord.id as xIdOrden,  "  //(CONVERTIDO A SCOM)
			+ " word.id  as xIdWorkflow,"
			+ " ord.id_buzon  as xIdBuzon,"
			+ " to_char(ord.fecha_vencimiento,'DD/MM/YYYY HH24:MI:SS') as xFecVenc "
			+ " FROM SCHSCOM.ord_orden ord, SCHSCOM.orm_orden orm, SCHSCOM.wkf_workflow word "
			+ " WHERE ord.id  = orm.id_orden "
			+ " AND ord.nro_orden  = ? "
			+ " AND ord.id_workflow  =  word.id "
			+ " AND word.id_state  IN ('Creada', 'Emitida','SCreada','SEmitida')";

	public static final String SQL_BFN_EXISTE_CUENTA_ORM = "SELECT count(1) FROM SCHSCOM.ord_orden ord, SCHSCOM.orm_orden orm, SCHSCOM.wkf_workflow ww" //(CONVERTIDO A SCOM)
			+ " WHERE ord.id  = orm.id_orden"
			+ " AND ord.id_workflow  = ww.id"
			+ " AND ww.id_state  IN ('Creada', 'Emitida','SCreada','SEmitida')"
			+ " AND ord.id_servicio  = "
			+ " (select id_servicio "
			+ " from SCHSCOM.cliente "
			+ " where  nro_cuenta = ? )";

	public static final String SQL_BFN_EXISTE_CONTRATISTA_ORM = " SELECT count(1) " //(CONVERTIDO A SCOM)
			+ "	FROM SCHSCOM.com_contratista c,"
			+ "	SCHSCOM.com_cont_mod m,"
			+ "	SCHSCOM.seg_modulo s"
			+ "	WHERE c.id  = m.id_contratista "
			+ "	AND m.id_modulo  = s.id_modulo"
			+ "	AND c.cod_contratista  = ?"
			+ "	AND s.cod_modulo = 'ORM'";

	public static final String SQL_BFN_EXISTE_TIPO_DOCUM = "SELECT id_tip_doc_persona, cod_interno "  //(LO MANTUVE EN 4J PORQUE NO EXISTEN ESOS DATOS EN CLIENTE Y SE NECESITA PARA EL FLUJO (ES UN CATALOGO))
			+ "  FROM nuc_tip_doc_persona "
			+ "  WHERE cod_tip_doc_persona = ? ";

	public static final String SQL_BFN_EXISTE_TAREA = "SELECT valor  as xdValor, trim(req_cambio_medidor) as xcCambioMed, " //(CONVERTIDO A SCOM)
			+ " trim(req_contraste_medidor) as xcContraMed, id as xIdTarea 		"
			+ " FROM SCHSCOM.orm_tarea "
			+ " WHERE code  = ? "
			+ " AND estado  = 'S' ";

	public static final String SQL_BFN_VALIDAR_IDENTIFICADOR = "SELECT " 	//(CONVERTIDO A SCOM)
			+ "  case when ? = 'C' then COND_CAMBIO "
			+ "  when ? = 'P' then COND_PREVENTIVO "
			+ "  when ? = 'I' then COND_INSTALACION "
			+ "  when ? = 'R' then COND_REPOSICION "
			+ "  end activo "
			+ "  FROM SCHSCOM.ORM_TAREA "
			+ "  WHERE code = ? ";

	public static final String SQL_GET_CODIGO_CONTRATISTA = " SELECT id "  //(CONVERTIDO A SCOM)
			+ "  FROM SCHSCOM.com_contratista "
			+ "  WHERE cod_contratista = ? ";

	public static final String SQL_GET_CODIGO_EJECUTOR = "SELECT id_ejecutor "  //(CONVERTIDO A SCOM)
			+ "  FROM SCHSCOM.com_ejecutor"
			+ "  WHERE cod_ejecutor = ?";

	public static final String SQL_GET_CUENTA = " SELECT id_servicio FROM SCHSCOM.cliente where nro_cuenta = ? "; //(CONVERTIDO A SCOM)

	public static final String SQL_ACTUALIZAR_UPDATE_ORD_ORDEN = " UPDATE SCHSCOM.ord_orden " //(CONVERTIDO A SCOM)
			+ "  SET LEIDO = 'S' "
			+ " ,FECHA_INGRESO_ESTADO_ACTUAL = now() "
			+ "  WHERE id = ? ";

	public static final String SQL_GET_FECHA_VENCIMIENTO = " SELECT coalesce(TO_CHAR(fecha_vencimiento,'DD/MM/YYYY HH24:MI:SS'),'00/00/0000')" //(CONVERTIDO A SCOM)
			+ "  FROM SCHSCOM.ord_orden WHERE id = ? ";

	
	public static final String SQL_SELECT_DATA_CURSOR_XCUR_VENC = " SELECT TO_CHAR(ORD.FECHA_CREACION,'DD/MM/YYYY HH24:MI:SS') FECHA_CREACION,CNF.PLAZO_INTERNO" //(CONVERTIDO A SCOM)
			+ "     FROM SCHSCOM.ORD_ORDEN ORD, SCHSCOM.ORM_ORDEN ORM, SCHSCOM.ORD_CONFIGURACION CNF, SCHSCOM.ORM_CONFIGURACION CNFM  "
			+ "     WHERE ORD.ID = ORM.ID_ORDEN  "
			+ "     AND CNF.ID_CONFIGURACION = CNFM.ID_CONFIGURACION   "
			+ "     AND CNFM.ID_MOTIVO = ORD.ID_MOTIVO  "
			+ "     AND CNFM.ID_TEMA = ORM.ID_TEMA  "
			+ "     AND CNFM.ID_TRABAJO = ORM.ID_TRABAJO "
			+ "     AND ORD.ID = ? "
			+ "     AND ORD.ID_TIPO_ORDEN = 2 "; 
	
	public static final String SQL_GET_CURSOR_FECHA_VENCIMIENTO = "SELECT " //(CONVERTIDO A SCOM)
			+ "coalesce(TO_CHAR(SCHSCOM.fn_calcular_fecha_vencimiento(?::TIMESTAMP, ? * ?, 1, 15),'DD/MM/YYYY HH24:MI:SS'),'1900-01-01 00:00:00')";

	public static final String SQL_ACTUALIZAR_UPDATE_ORD_ORDEN_FECHA_VENCIMIENTO = "UPDATE SCHSCOM.ORD_ORDEN " //(CONVERTIDO A SCOM)
			+ " SET FECHA_VENCIMIENTO = TO_DATE( ? ,'DD/MM/YYYY HH24:MI:SS') "
			+ " WHERE ID = ?";

	public static final String SQL_ACTUALIZAR_UPDATE_WORK_FLOW_SET_ID_STATE_PARENTTYPE_IDEMPRESA = "UPDATE SCHSCOM.WKF_WORKFLOW " //(CONVERTIDO A SCOM)
			+ "    SET ID_STATE = 'Respondida' "
			+ "    ,ID_OLD_STATE = 'Creada' "
			+ "    ,PARENT_TYPE = 'com.synapsis.synergia.orm.domain.OrdenMantenimiento' "
			+ "    WHERE ID = ? ";
	// INICIO RSOSA INDEPE-5569 30/06/2023
	public static final String SQL_ACTUALIZAR_UPDATE_WORK_FLOW_SET_ID_STATE_OLD = "UPDATE SCHSCOM.WKF_WORKFLOW " 
			+ "    SET ID_STATE = ID_OLD_STATE "
			+ "    WHERE ID = ? ";
	// FIN RSOSA INDEPE-5569 30/06/2023

	public static final String SQL_ACTUALIZAR_UPDATE_ORM_ORDEN_SET_IDCONTRATISTA_IDEJECUTOR_ATENDIOSEGURO_IDPARENTNOTIFICADO = "UPDATE SCHSCOM.ORM_ORDEN"  //(CONVERTIDO A SCOM)
			+ "    SET NRO_NOTIFICACION = ? "
			+ "    ,ID_CONTRATISTA = ? "
			+ "    ,ID_EJECUTOR = ? "
			+ "    ,ATENDIDO_SEGURO = 'N' "
			+ "    ,ID_PAREN_NOTIFICADO = 1 "
			+ "    WHERE id_orden = ? ";

	public static final String SQL_SELECT_NEXTVAL_SQAUDITEVENT = "SELECT SQAUDITEVENT.NEXTVAL FROM DUAL";  //(SC4J)
	public static final String SQL_SELECT_NEXTVAL_SQMAGNITUDIMPL = "SELECT SQMAGNITUDIMPL.NEXTVAL FROM DUAL";  //(SC4J)
	
	public static final String SQL_INSERT_FWK_AUDITEVENT = "INSERT INTO SCHSCOM.FWK_AUDITEVENT" //(CONVERTIDO A SCOM)
			+ "    (   id,"
			+ "        usecase,"
			+ "        fecha_ejecucion,"
			+ "        objectref,"
			+ "        id_fk,"
			+ "        id_user,"
			+ "        specific_auditevent"
			+ "    )"
			+ "    VALUES"
			+ "    (   "
			+ "	       ?,"
			+ "        '',"
			+ "        now(),"
			+ "        'com.synapsis.synergia.orm.domain.OrdenMantenimiento',"
			+ "        ?,"
			+ "        ?,"
			+ "        'ORD'"
			+ "    )";

	public static final String SQL_INSERT_ORD_HISTORICO = " INSERT INTO SCHSCOM.ORD_HISTORICO" //(CONVERTIDO A SCOM)
			+ "    (   ID_AUDITEVENT,"
			+ "        estado_inicial,"
			+ "        estado_final,"
			+ "        actividad,"
			+ "        id_buzon"
			+ "    )"
			+ "    VALUES"
			+ "    (   "
			+ "		   ?,"
			+ "        'Orden Creada',"
			+ "        'Orden Respondida',"
			+ "        'Responde Orden',"
			+ "        ? "
			+ "    )";

	public static final String SQL_INSERT_ORD_OBSERVACION = " INSERT INTO SCHSCOM.ORD_OBSERVACION" //(CONVERTIDO A SCOM)
			+ "    (   id_observacion,"
			+ "        texto,"
			+ "        fecha_observacion,"
			+ "        id_usuario,"
			+ "        id_orden,"
			+ "        state_name,"
			+ "        discriminator"
			+ "    )"
			+ "    VALUES"
			+ "    (   nextval('SCHSCOM.\"sqobservacionorden\"'),"
			+ "        ?,"
			+ "        to_date( ? , 'DD/MM/YYYY HH24:MI:SS'), "
			+ "        ?,"
			+ "        ?,"
			+ "        'Respondida',"
			+ "        'ObeservacionOrden'"
			+ "    )";

	public static final String SQL_SELECT_NEXTVAL_SQRESPUESTAORDENMANTENIMIENTO = "SELECT nextval('SCHSCOM.\"sqrespuestaordenmantenimiento\"')"; //(CONVERTIDO A SCOM)

	public static final String SQL_INSERT_ORM_RESPUESTA = " INSERT INTO SCHSCOM.ORM_RESPUESTA" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN 
			+ "    (    id_respuesta    ,id_empresa         ,id_orden,id_usuario,ejecuto"
			+ "        ,fecha_ejecucion ,fecha_creacion     ,fecha_vencimiento"
			+ "        ,tipo_creacion   ,nro_notificacion"
			+ "        ,id_contratista  ,ejecutor_nombre    ,atendido_seguro   ,nombre_notificado ,id_paren_notificado"
			+ "        ,id_servicio     ,id_tip_doc_persona ,nro_docto_ident   ,id_motivo_no_ej)"
			+ "    VALUES"
			+ "    (    ?  , ?           , ?          ,?          ,?"
			+ "        ,to_date( ? , 'DD/MM/YYYY HH24:MI:SS'),"
			+ "        now()           ,to_date( ? , 'DD/MM/YYYY HH24:MI:SS')"
			+ "        ,'Masiva'        , ? "
			//+ "        ,?  , ?        ,'N'               , ?       , CASE WHEN ? = 0 THEN NULL ELSE ? end" // COMENTADO RSOSA INDEPE-5569 30/06/2023
			//+ "        ,?    ,?  ,?        , CASE WHEN ? = 'NO' THEN ? ELSE NULL END " // COMENTADO RSOSA INDEPE-5569 30/06/2023
			+ "        ,?  , ?        ,'N'               , ?       , cast(CASE WHEN ? = 0 THEN NULL ELSE ? end as bigint)" // AGREGADO RSOSA INDEPE-5569 30/06/2023
			+ "        ,?    ,?  ,?        , cast(CASE WHEN ? = 'NO' THEN ? ELSE NULL END as bigint)" // AGREGADO RSOSA INDEPE-5569 30/06/2023
			+ "        );";

	public static final String SQL_SELECT_NEXTVAL_SQINNERRAIZORDENMANTENIMIENT = "SELECT nextval('SCHSCOM.\"sqinnerraizordenmantenimiento\"')"; //(CONVERTIDO A SCOM)

	public static final String SQL_INSERT_ORD_IN_RAIZ_XML = " INSERT into SCHSCOM.ORM_IN_RAIZ_XML" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN 
			+ "    (   id_raiz_xml,"
			+ "        id_empresa,"
			+ "        nro_proceso,"
			+ "        fecha_procesamiento"
			+ "    )"
			+ "    VALUES"
			+ "    ("
			+ "     	?,"
			+ "         ?,"
			+ "        nextval('SCHSCOM.\"sqinnerraizordenmantenimiento\"'),"
			+ "        now()"
			+ "    )";

	public static final String SQL_INSERT_ORD_IN_TIPO_DOC = "INSERT into SCHSCOM.ORM_IN_TIP_DOC" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN 
			+ "    (   id_tipo_documento_persona,"
			+ "        id_empresa,"
			+ "        code,"
			+ "        estado"
			+ "    )"
			+ "    VALUES"
			+ "    (   nextval('SCHSCOM.\"sqinnertipodocumentopersona\"'),"
			+ "        ?,"
			+ "        ?,"
			+ "        ''"
			+ "    )";

	public static final String SQL_SELECT_NEXTVAL_SQINNERPARENTESCO = "SELECT nextval('SCHSCOM.\"sqinnerparentesco\"')"; //(CONVERTIDO A SCOM) 

	public static final String SQL_INSERT_ORD_IN_PARENTESCO = "INSERT into SCHSCOM.ORM_IN_PARENTESCO" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN 
			+ "    (   id_parentesco,"
			+ "        id_empresa,"
			+ "        code,"
			+ "        estado"
			+ "    )"
			+ "    VALUES"
			+ "    ( "
			+ "      	?,"
			+ "         ?,"
			+ "         ?,"
			+ "        ''"
			+ "    )";

	public static final String SQL_SELECT_NEXTVAL_SQINNERCONTRATISTA = "SELECT nextval('SCHSCOM.\"sqinnercontratista\"')"; //(CONVERTIDO A SCOM)

	public static final String SQL_INSERT_ORM_IN_CONTRATISTA = "INSERT into SCHSCOM.ORM_IN_CONTRATISTA" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN 
			+ "    (   id_contratista,"
			+ "        id_empresa,"
			+ "        code,"
			+ "        estado"
			+ "    )"
			+ "    VALUES"
			+ "    ( "
			+ "        ?,"
			+ "        ?,"
			+ "        ?,"
			+ "        ''"
			+ "    )";

	public static final String SQL_SELECT_NEXTVAL_SQINNERMOTIVONOEJECUCIONORDENM = "SELECT nextval('SCHSCOM.\"sqinnermotivonoejecucionordenm\"')"; //(CONVERTIDO A SCOM)

	public static final String SQL_INSERT_ORM_IN_MOTIVO = "  INSERT into SCHSCOM.ORM_IN_MOTIVO" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN 
			+ "        (   id_motivo,"
			+ "            id_empresa,"
			+ "            code,"
			+ "            estado"
			+ "        )"
			+ "        VALUES"
			+ "        ( "
			+ "          ?,"
			+ "          ?,"
			+ "          ?,"
			+ "         'OK'"
			+ "        )";

	public static final String SQL_SELECT_NEXTVAL_SQINNERRESPUESTAORDENMANTENIMI = "SELECT nextval('SCHSCOM.\"sqinnerrespuestaordenmantenimi\"')"; //(CONVERTIDO A SCOM)

	public static final String SQL_INSERT_ORM_IN_RESP = "INSERT into SCHSCOM.ORM_IN_RESP" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN 
			+ "    (   id_respuesta            , id_empresa        , ejecuto,"
			+ "        fecha_ejecucion         , nro_notificacion  , atendido_por_seguro,"
			+ "        id_contratista          , ejecutor_nombre   ,"
			+ "        id_parentesco_notificado, nombre_notificado ,"
			+ "        id_tipo_dto_notificado  , nro_dto_notificado, estado,"
			+ "        id_motivo_no_ejec"
			+ "    )"
			+ "    VALUES"
			+ "    (   ?,             ?,           ?,"
			+ "        to_date(?, 'DD/MM/YYYY HH24:MI:SS'), ?,         'N',"
			+ "        ?, ?,"
			+ "        ?,  ?,"
			+ "        ?,          ?,         '',"
			//+ "        CASE WHEN ? = 'NO' THEN ? ELSE NULL END" // COMENTADO RSOSA INDEPE-5569 30/06/2023
			+ "        cast(CASE WHEN ? = 'NO' THEN ? ELSE NULL END as bigint)" // AGREGADO RSOSA INDEPE-5569 30/06/2023
			+ "    )";

	public static final String SQL_SELECT_NEXTVAL_SQINNERSERVICIOASOCIADO = "SELECT nextval('SCHSCOM.\"sqinnerservicioasociado\"')"; //(CONVERTIDO A SCOM)

	public static final String SQL_INSERT_ORM_IN_SERVICIO = "INSERT into SCHSCOM.ORM_IN_SERVICIO" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN 
			+ "    (   id_servicio,"
			+ "        id_empresa,"
			+ "        nro_servicio,"
			+ "        estado"
			+ "    )"
			+ "    VALUES"
			+ "    (   "
			+ "      ?,"
			+ "      ?,"
			+ "      ?,"
			+ "        ''"
			+ "    )";

	public static final String SQL_SELECT_NEXTVAL_SQINNERORDENMANTENIMIENTO = "SELECT nextval('SCHSCOM.\"sqinnerordenmantenimiento\"')"; //(CONVERTIDO A SCOM)

	public static final String SQL_INSERT_ORM_IN_ORDEN = "INSERT into SCHSCOM.ORM_IN_ORDEN" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN 
			+ "    (   id_orden,"
			+ "        id_empresa,"
			+ "        nro_orden,"
			+ "        tipo,"
			+ "        estado,"
			+ "        id_respuesta,"
			+ "        id_servicio,"
			+ "        id_raiz_xml"
			+ "    )"
			+ "    VALUES"
			+ "    (   ?,"
			+ "        ?,"
			+ "        ?,"
			+ "        '',"
			+ "        'Ejecutado',"
			+ "        ?,"
			+ "        ?,"
			+ "        ?"
			+ "    )";

	public static final String SQL_INSERT_ORM_IN_SERV = "INSERT into SCHSCOM.ORM_IN_OBSERV" //(CONVERTIDO A SCOM)  --> NO ESTA CREADA AUN
			+ "    (   id_observacion,"
			+ "        id_empresa,"
			+ "        description,"
			+ "        id_orden"
			+ "    )"
			+ "    VALUES"
			+ "    (   nextval('SCHSCOM.\"sqinnerobservacionorden\"'),"
			+ "        ?,"
			+ "        ?,"
			+ "        ?"
			+ "    )";

	public static final String SQL_SELECT_COUNT_ORMTAREAEJEC = "SELECT count(1)" //(CONVERTIDO A SCOM)
			+ "      FROM SCHSCOM.orm_tarea_ejec"
			+ "     WHERE id_tarea = ?"
			+ "       AND id_orden = ?";

	public static final String SQL_INSERT_ORM_TAREA_EJEC = "INSERT into SCHSCOM.ORM_TAREA_EJEC" //(CONVERTIDO A SCOM)
			+ "        (   id,"
			+ "            fecha_creacion,    ejecutada,          costo,    cantidad_ejec, lleva_cambio_medidor, lleva_contraste_medidor,"
			+ "            costo_por_sistema, condicion_trabajo, tipo_mantenimiento, id_tarea, id_orden,      facturar"
			+ "        )"
			+ "        VALUES"
			+ "        (   nextval('SCHSCOM.\"sqtareaejecutadaordenmantenimi\"'),"
			+ "            now(),           ?,        ?,  ?,    ?,          ?,"
			+ "            ?,        ?,     'CORRECTIVO',       ?, ?,      'N'"
			+ "        )";

	public static final String SQL_UPDATE_ORM_TAREA_EJEC = " UPDATE SCHSCOM.ORM_TAREA_EJEC" //(CONVERTIDO A SCOM)
			+ "        SET ejecutada               = ? "
			+ "           ,costo                   = ? "
			+ "           ,costo_por_sistema       = ? "
			+ "           ,facturar                = 'N' "
			+ "           ,lleva_cambio_medidor    = ? "
			+ "           ,lleva_contraste_medidor = ? "
			+ "           ,condicion_trabajo       = ? "
			+ "           ,tipo_mantenimiento      = 'CORRECTIVO' "
			+ "           ,cantidad_ejec           = ? "
			+ "        WHERE id_tarea = ? "
			+ "          AND id_orden = ? ";

	public static final String SQL_SELECT_NEXTVAL_SQINNERCONDICIONTRABAJOORDENMA = "SELECT nextval('SCHSCOM.\"sqinnercondiciontrabajoordenma\"')"; //(CONVERTDO A SCOM)

	public static final String SQL_INSERT_ORM_IN_COND_TRAB = "INSERT into SCHSCOM.ORM_IN_COND_TRAB" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN
			+ "        (   id_cond_trabajo,"
			+ "            id_empresa,"
			+ "            description"
			+ "        )"
			+ "        VALUES"
			+ "        (   "
			+ "            ?,"
			+ "            ?,"
			+ "            ?"
			+ "        )";

	public static final String SQL_SELECT_NEXTVAL_SQINNERTAREAORDENMANTENIMIENTO = "SELECT nextval('SCHSCOM.\"sqinnertareaordenmantenimiento\"')"; //(CONVERTIDO A SCOM)

	public static final String SQL_INSERT_ORM_IN_TAREA = "INSERT into SCHSCOM.ORM_IN_TAREA" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN
			+ "        (   id_tarea,"
			+ "            id_empresa,"
			+ "            code,"
			+ "            estado"
			+ "        )"
			+ "        VALUES"
			+ "        ( "
			+ "          ?,"
			+ "          ?,"
			+ "          ?,"
			+ "          ''"
			+ "        )";

	public static final String SQL_INSERT_ORM_IN_TAREA_EJE = "INSERT into SCHSCOM.ORM_IN_TAREA_EJE" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN
			+ "        (   id_tarea_ejec,"
			+ "            id_empresa,"
			+ "            tarea_ejecutada,"
			+ "            cantidad_ejecutada,"
			+ "            estado,"
			+ "            id_tarea_orden_mantenimiento,"
			+ "            id_condicion_trabajo,"
			+ "            id_orden,"
			+ "            facturar_tarea"
			+ "        )"
			+ "        VALUES"
			+ "        (   nextval('SCHSCOM.\"sqinnertareasejecutadas\"'),"
			+ "            ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            '',"
			+ "            ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            'N'"
			+ "        )";

//	public static final String SQL_SELECT_VALIDAR_AIC = "SELECT count(*) "  //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "     FROM aic_asoc_ordenes aic "
//			+ "     WHERE aic.id_orden = ? ";
//
//	public static final String SQL_SELECT_GET_USUARIO_ID = "SELECT ET.ID_USUARIO " //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "     FROM AIC_ETAPA_ATENCION ET, AIC_ASOC_ORDENES ASOC "
//			+ "     WHERE ET.ID_ETAPA_ATENCION = ASOC.ID_ETAPA_ATENCION "
//			+ "     AND ASOC.ID_ORDEN = ? ";

	public static final String SQL_SELECT_GET_RESPONSABLE_ID = " SELECT ID " //(CONVERTIDO A SCOM)
			+ "    FROM SCHSCOM.ORD_RESPONSABLE "
			+ "    WHERE TIPO_RESPONSABLE = 'Usuario' AND PERSISTOR_REF= 'com.cubika.framework.security.User' "
			+ "	   AND ID_OBJECT =? ";

	public static final String UPDATE_ORD_ORDER_DERIV = "UPDATE SCHSCOM.ORD_ORD_DERIV "	//(CONVERTIDO A SCOM)
			+ "SET ID_ULT_RESPONSABLE =ID_RESPONSABLE, ID_RESPONSABLE = ? WHERE ID_ORDEN = ? ";

	public static final String UPDATE_ORD_ORDEN_FINAL = " UPDATE SCHSCOM.ORD_ORDEN " //(CONVERTIDO A SCOM)
			+ "    SET fecha_finalizacion = NOW(), "
			+ "    leido = 'S', "
			+ "    fecha_ingreso_estado_actual = NOW() "
			+ "    WHERE id = ? ";

	public static final String UPDATE_WKFWORKFLOW_FINAL = " UPDATE SCHSCOM.WKF_WORKFLOW" //(CONVERTIDO A SCOM)
			+ "    SET id_state     = 'Finalizada', "
			+ "    id_old_state = 'Respondida' "
			+ "    WHERE id = ? ";

	public static final String INSERT_FFWKAUDITEVENT_FINAL = "INSERT INTO SCHSCOM.FWK_AUDITEVENT" //(CONVERTIDO A SCOM)
			+ "    (   id,"
			+ "        usecase,"
			+ "        fecha_ejecucion,"
			+ "        objectref,"
			+ "        id_fk,"
			+ "        id_user,"
			+ "        specific_auditevent"
			+ "    )"
			+ "    VALUES"
			+ "    ("
			+ "         ?,"
			+ "        '',"
			+ "        now(),"
			+ "        'com.synapsis.synergia.orm.domain.OrdenMantenimiento',"
			+ "        ?,"
			+ "        ?,"
			+ "        'ORD'"
			+ "    )";

	public static final String INSERT_ORDHISTORICO_FINAL = "INSERT INTO SCHSCOM.ORD_HISTORICO "   //(CONVERTIDO A SCOM)
			+ "    (   id_auditevent,"
			+ "        estado_inicial,"
			+ "        estado_final,"
			+ "        actividad,"
			+ "        id_buzon"
			+ "    )"
			+ "    VALUES"
			+ "    ("
			+ "      ?,"
			+ "   'Orden Respondida',"
			+ "   'Orden Finalizada',"
			+ "   'Finaliza la Orden',"
			+ "       ?"
			+ "    )";

	public static final String SQL_SELECT_BFN_EXISTE_MARCA_MEDIDOR_2 = "SELECT id "   //(SCHSCOM)
			+ "     FROM SCHSCOM.med_marca "
			+ "     WHERE cod_marca = ? "
			+ "     AND id_tip_componente = 1 "
			+ "     AND activo = 'S' ";

	public static final String SQL_SELECT_BFN_EXISTE_MODELO_MEDIDOR_2 = "SELECT id" //(SCHSCOM)
			+ "     FROM SCHSCOM.med_modelo "
			+ "     WHERE id_marca = ? "
			+ "     AND cod_modelo = ? "
			+ "     AND activo = 'S' ";

	public static final String SQL_SELECT_DATA_IN_NRO_COMPONENTE = "SELECT id AS IdComponenteInst , coalesce(id_dynamicobject,-1) AS idDynamicObjectInst, " //(SCHSCOM)
			+ "		id_est_componente AS IdEstComponenteInst "
			+ "     FROM SCHSCOM.med_componente "
			+ "     WHERE nro_componente = ? "
			+ "     AND id_modelo      = ? ";

	public static final String INSERT_ORD_MEDI_CAMBIO = "INSERT into SCHSCOM.ORD_MEDI_CAMBIO" //(CONVERTIDO A SCOM)
			+ "        ("
			+ "            id_medi_cambio,"
			+ "            id_componente,"
			+ "            id_orden,"
			+ "            id_empresa,"
			+ "            accion"
			+ "        )"
			+ "        VALUES"
			+ "        ("
			+ "            nextval('SCHSCOM.\"sqcambiomedidororden\"'),"
			+ "            ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            'Instalacion'"
			+ "        )";

	public static final String SQL_SELECT_NEXTVAL_SQINNERPROPIEDADCOMPONENTE = "SELECT nextval('SCHSCOM.\"sqinnerpropiedadcomponente\"')"; //(CONVERTIDO A SCOM)

	public static final String INSERT_ORD_IN_PROP_COMP = "INSERT INTO SCHSCOM.ORM_IN_PROP_COMP" //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN
			+ "        (   id_prop_comp,"
			+ "            id_empresa,"
			+ "            code,"
			+ "            estado"
			+ "        )"
			+ "        VALUES"
			+ "        (   "
			+ "          ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            ''"
			+ "        )";

	public static final String SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORINSTALACION = "SELECT nextval('SCHSCOM.\"sqinnerormmedidorinstalacion\"')";  //(CONVERTIDO A SCOM)

	public static final String INSERT_ORM_IN_MED_INST = "INSERT INTO SCHSCOM.ORM_IN_MED_INST"  //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN
			+ "        (   id_medidor_instalacion,"
			+ "            id_empresa,"
			+ "            nro_componente,"
			+ "            code_modelo,"
			+ "            code_marca,"
			+ "            estado,"
			+ "            id_respuesta,"
			+ "            id_propiedad"
			+ "        )"
			+ "        VALUES"
			+ "        (   "
			+ "            ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            'OK',"
			+ "            ?,"
			+ "            ?"
			+ "        )";

	public static final String INSERT_ORM_IN_MEDIA = " INSERT INTO SCHSCOM.ORM_IN_MEDIDA"	//(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN
			+ "                (   id_medida,"
			+ "                    id_empresa,"
			+ "                    cod_tipo_medida,"
			+ "                    estado,"
			+ "                    valor,"
			+ "                    id_medidor_instalacion"
			+ "                )"
			+ "                VALUES"
			+ "                (   nextval('SCHSCOM.\"sqinnerormmedida\"'),"
			+ "                    ?,"
			+ "                    ?,"
			+ "                    'OK',"
			+ "                    ?,"
			+ "                    ?"
			+ "                )";

	public static final String SQL_SELECT_EXISTE_NUMERO_MEDIDOR = "SELECT count(*)"	//(SCHSCOM)
			+ " FROM SCHSCOM.med_componente c, SCHSCOM.med_est_componente e"
			+ " WHERE c.nro_componente = ? "
			+ " AND c.id_modelo = ? "
			+ " AND c.id_est_componente = e.id "
			+ " AND c.id_ubicacion = ?"
			+ " AND e.cod_interno = 'Instalado'";

	public static final String SQL_SELECT_EXISTE_NUMERO_MEDIDOR2 = "SELECT count(*)" //(SCHSCOM)
			+ " FROM SCHSCOM.med_componente c, SCHSCOM.med_est_componente e"
			+ " WHERE c.nro_componente = ? "
			+ " AND c.id_modelo = ? "
			+ " AND c.id_est_componente = e.id "
			+ " AND e.cod_interno = 'Disponible' "
			;

	public static final String SQL_SELECT_DATA_IN_MED_COMPONENTE = "SELECT id AS IdComponenteRet, coalesce(id_dynamicobject,-1) AS idDynamicObjectRet, " //(SCHSCOM)
			+ "	id_est_componente AS IdEstComponenteRet"
			+ " FROM SCHSCOM.med_componente"
			+ " WHERE nro_componente = ? "
			+ " AND id_modelo      = ? "
			+ " AND id_ubicacion   = ? ";
	
	public static final String INSERT_ORM_IN_MEDIA_CAMBIO_2 = "INSERT into SCHSCOM.ORD_MEDI_CAMBIO" //(CONVERTIDO A SCOM)
			+ "        ("
			+ "            id_medi_cambio,"
			+ "            id_componente,"
			+ "            id_orden,"
			+ "            id_empresa,"
			+ "            accion"
			+ "        )"
			+ "        VALUES"
			+ "        ("
			+ "            nextval('SCHSCOM.\"sqcambiomedidororden\"'),"
			+ "            ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            'Retiro'"
			+ "        )";
	
	public static final String SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORRETIRO = "SELECT nextval('SCHSCOM.\"sqinnerormmedidorretiro\"')";  //(CONVERTIDO A SCOM)

	public static final String INSERT_ORM_IN_MED_RETIRO = "INSERT INTO SCHSCOM.ORM_IN_MED_RETIRO"  //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN
			+ "        (   id_medidor_retiro,"
			+ "            id_empresa,"
			+ "            nro_componente,"
			+ "            code_modelo,"
			+ "            code_marca,"
			+ "            estado,"
			+ "            id_respuesta"
			+ "        )"
			+ "        VALUES"
			+ "        (   ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            'OK',"
			+ "            ?"
			+ "        )";

	public static final String INSERT_ORM_IN_MEDIDA_2 = "INSERT INTO SCHSCOM.ORM_IN_MEDIDA"   //(CONVERTIDO A SCOM) --> NO ESTA CREADA AUN
			+ "                (   id_medida,"
			+ "                    id_empresa,"
			+ "                    cod_tipo_medida,"
			+ "                    estado,"
			+ "                    valor,"
			+ "                    id_medidor_retiro"
			+ "                )"
			+ "                VALUES"
			+ "                (   nextval('SCHSCOM.\"sqinnerormmedida\"'),"
			+ "                    ?,"
			+ "                    ?,"
			+ "                    'OK',"
			+ "                    ?,"
			+ "                    ?"
			+ "                )";

	public static final String SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE = "SELECT id "  //(SCHSCOM)
			+ "          FROM SCHSCOM.med_componente"
			+ "         WHERE nro_componente = ? "
			+ "           AND id_modelo      = ? "
			+ "           AND id_ubicacion   = ? ";

	public static final String SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE2 = "SELECT id " //(SCHSCOM)
			+ "          FROM SCHSCOM.med_componente"
			+ "         WHERE nro_componente = ? "
			+ "           AND id_modelo      = ? ";

	public static final String SQL_SELECT_CURSOR_QLECT2 = "SELECT me.cod_medida " 	//(SCHSCOM)
			+ " FROM SCHSCOM.med_medida me, SCHSCOM.med_medida_medidor med "
			+ " WHERE med.id_componente = ? "
			+ " AND me.id = med.id_medida";

	public static final String SQL_SELECT_EXTRACT_ID_EMPRESA_GLOBAL = "SELECT ne.id_empresa "	//(CONVERTIDO A SCOM)
			+ " FROM SCHSCOM.nuc_empresa ne  WHERE ne.cod_partition = 'GLOB' ";

//	public static final String SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO = "SELECT id_motivo_estado " //OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J
//			+ " FROM sel_motivo_estado  WHERE cod_interno = 'Retirado' AND activo = 'S'";

	public static final String SQL_SELECT_NEXTVAL_SQDYNAMICBUSINESSOBJECT = "SELECT NEXTVAL('sqdynamicbusinessobject')"; // R.I. Correctivo 09/11/2023 //(SC4j) SELECT SQDYNAMICBUSINESSOBJECT.NEXTVAL FROM DUAL

	public static final String SQL_SELECT_DATA_IN_MED_COMPONENTE_2 = "SELECT coalesce(id_dynamicobject,-1) AS idDynamicObject, id_propiedad AS idPropiedad, " //(SCHSCOM)
			+ " id AS idComponente"
			+ " FROM SCHSCOM.med_componente "
			+ " WHERE nro_componente = ? "
			+ " AND id_modelo = ? "
			+ " AND id_ubicacion = ? ";

	public static final String INSERT_DYO_OBJECT = "INSERT INTO SCHSCOM.DYO_OBJECT(ID) "  //(SCHSCOM)
			+ " VALUES (?)";

	public static final String UPDATE_MED_COMPONENTE1 = "UPDATE SCHSCOM.med_componente"			//(SCHSCOM) - ARTIFICIO
			+ " SET id_est_componente = (select id from med_est_componente where cod_interno = 'Retirado')," 
			+ " id_ubicacion = (case when(id_propiedad = ?)"
			+ " then ? "
			+ " else ? "
			+ " end), "
			+ " type_ubicacion = (case when (id_propiedad = ?)"
			+ " then 'com.synapsis.synergia.nucleo.domain.interfaces.Cliente'"
			+ " else 'com.synapsis.synergia.common.domain.Contratista' "
			+ " end)"
			+ " WHERE nro_componente = ?"
			+ " AND id_modelo = ?"
			+ " AND id_ubicacion = ?";
	
	public static final String UPDATE_MED_COMPONENTE2 = "UPDATE SCHSCOM.med_componente"			//(SCHSCOM)- ARTIFICIO
			+ " SET id_est_componente = (select id from med_est_componente where cod_interno = 'Retirado'),"
			+ " id_ubicacion = (case when(id_propiedad = ?)"
			+ " then ? "
			+ " else ? "
			+ " end), "
			+ " type_ubicacion = (case when (id_propiedad = ?)"
			+ " then 'com.synapsis.synergia.nucleo.domain.interfaces.Cliente'"
			+ " else 'com.synapsis.synergia.common.domain.Contratista' "
			+ " end) "
			+ " ,id_dynamicobject = ?"
			+ " WHERE nro_componente = ?"
			+ " AND id_modelo = ?"
			+ " AND id_ubicacion = ?";

	public static final String SQL_SELECT_MAX_MED_HIS_COMPONENTE = "SELECT max(id_his_componente)" //(SCHSCOM)
			+ " FROM SCHSCOM.med_his_componente"
			+ " WHERE id_componente = ?"
			+ " AND id_est_componente = 18";

	public static final String UPDATE_MED_HIS_COMPONENT = "UPDATE SCHSCOM.med_his_componente " //(SCHSCOM)
			+ " SET fec_hasta = now(), "
			+ " id_orden = ?"
			+ " WHERE id_his_componente = ? ";

	public static final String INSERT_MED_HIS_COMPONENTE1 = "INSERT INTO SCHSCOM.MED_HIS_COMPONENTE ( "  //(SCHSCOM) - ARTIFICIO
			+"   id_his_componente, "
            +"   id_componente, "
            +"   id_est_componente, "
            +"   fec_desde, "
            +"   id_ubicacion, "
            +"   type_ubicacion, "
            +"   id_orden) "
            +"	 SELECT       "
            + "  ?,"
            + "  ?,"
            + "  (select id from SCHSCOM.med_est_componente where cod_interno = 'Retirado'),"
            + "  now(),"
            + "  ?,"
            + "  'com.synapsis.synergia.common.domain.Contratista',"
            + "  ?"
			;

	public static final String INSERT_MED_HIS_COMPONENTE2 = "INSERT INTO SCHSCOM.MED_HIS_COMPONENTE ( "  //(SCHSCOM)- ARTIFICIO
			+"   id_his_componente, "
            +"   id_componente, "
            +"   id_est_componente, "
            +"   fec_desde, "
            +"   id_ubicacion, "
            +"   type_ubicacion, "
            +"   id_orden) "
            +"	 SELECT       "
            + "  ?,"
            + "  ?,"
            + "  (select id from SCHSCOM.med_est_componente where cod_interno = 'Retirado'),"
            + "  now(),"
            + "  ?,"
            + "  'com.synapsis.synergia.nucleo.domain.interfaces.Cliente',"
            + "  ?"
			;

	public static final String INSERT_FWK_AUDITEVENT = "INSERT INTO SCHSCOM.FWK_AUDITEVENT("	//CONVERTIDO A SCOM
			+ "        id,"
			+ "        usecase,"
			+ "        objectref,"
			+ "        id_fk,"
			+ "        fecha_ejecucion,"
			+ "        specific_auditevent,"
			+ "        id_user)"
			+ "    VALUES("
			+ "        ?,"
			+ "        'Medidor.UPDATE',"
			+ "        'com.synapsis.synergia.med.domain.componente.Medidor',"
			+ "        ?,"
			+ "        now(),"
			+ "        'COMPONENTE',"
			+ "        ?)";

	public static final String SQL_SELECT_EXTRACT_ID_MED_TIP_MAGNITUD = "SELECT id_tip_magnitud " //PARA AMBOS HACER LA CONSULTA AL 4J
			+ "      FROM med_tip_magnitud WHERE cod_interno = 'Retiro'";

	public static final String SQL_SELECT_EXTRACT_ID_FACT_ES_MAGNITUD = " SELECT id_est_magnitud " //PARA AMBOS HACER LA CONSULTA AL 4J
			+ "      FROM fac_est_magnitud WHERE codigo = '01'";

	public static final String SQL_SELECT_DATA_IN_MORE_TABLES = "SELECT med.id_medida as vIdMedida, fac.val_factor as vIdFactor, med.id_tip_calculo as vIdTipCalculo" //(SCHSCOM)
			+ "			, tc.id_tipo_cons as vIdTipo_cons, "
			+ "          med.cant_enteros as vICantEnteros, med.cant_decimales as vICantDecimales "
			+ "          FROM med_medida me, med_medida_medidor med, med_factor fac, med_ent_dec ed, fac_tipo_consumo tc"
			+ "          WHERE med.id_componente = ? "
			+ "           AND me.id      = med.id_medida"
			+ "           AND med.id_factor     = fac.id"
			+ "           AND med.id_ent_dec    = ed.id"
			+ "           AND med.id_medida     = tc.id_medida"
			+ "           AND me.cod_medida     = ? ";

	public static final String SQL_SELECT_DATA_IN_NUC_CUENTA_SERVICIO = "SELECT nvl(se.sec_magnitud,0) + 1 as vIdSecMagnitud, se.id_servicio as xid_servicio " //(DUDA sec_magnitud -- no existe en CLIENTE) 4J SACARLO
			+ "          FROM nuc_cuenta nc, nuc_servicio ns, srv_electrico se"		
			+ "         WHERE nc.id_cuenta = ns.id_cuenta"
			+ "           AND ns.id_servicio = se.id_servicio"
			+ "           AND ns.tipo = 'ELECTRICO'"
			+ "           AND nc.nro_cuenta = ? ";

	public static final String UPDATE_SRV_ElECTRICO = "UPDATE srv_electrico"  		//(SIEMPRE 4J)
			+ "           SET sec_magnitud = ? "
			+ "         WHERE id_servicio  = ? ";

	public static final String INSERT_MED_MAGNITUD = "INSERT INTO MED_MAGNITUD("		//(SIEMPRE 4J)
			+ "            id_magnitud,  id_empresa,     id_componente,   id_servicio,     id_medida,"
			+ "            valor,        factor,         id_tip_magnitud, fec_lec_terreno, observaciones,"
			+ "            leido_desde,  fecha_lect_ini, id_tip_calculo,  id_est_magnitud, cod_observacion,"
			+ "            id_tipo_cons, sec_magnitud,   tipo,            enteros,         decimales )"
			+ "        VALUES ("
			+ "            ?, ?,        ?,   ?,    ?,"
			+ "            ?,               ?,      ?, to_date( ? , 'DD/MM/YYYY'), '',"
			+ "            '',                     NULL,            ?,  ?, '',"
			+ "            ?,          ?, 'LECTURA',       ?,  ? "
			+ "        )";

	public static final String DELETE_MED_LEC_ULTIMA = "DELETE FROM SCHSCOM.MED_LEC_ULTIMA WHERE id_componente = ? AND id_medida = ? "; //(SCHSCOM) --> NO ESTA CREADA AUN

	public static final String INSERT_MED_LEC_ULTIMA = " INSERT INTO SCHSCOM.MED_LEC_ULTIMA" //(SCHSCOM) --> NO ESTA CREADA AUN
			+ "        (   id_lectura,"
			+ "            id_empresa,"
			+ "            id_medida,"
			+ "            id_componente )"
			+ "        VALUES"
			+ "        (   ?,"
			+ "            ?,"
			+ "            ?,"
			+ "            ?"
			+ "        )";

//	public static final String UPDATE_SEL_SELLO = "UPDATE sel_sello set" 		//(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "            id_motivo_estado = ?,"
//			+ "            fec_modificacion = SYSDATE,"
//			+ "            fec_retiro       = SYSDATE,"
//			+ "            id_origen_ret    = (select ID_ORIGEN from SEL_ORIGEN where COD_INTERNO='OrdenCorte'),"
//			+ "            id_ejec_ret      = (select ID_EJECUTOR from COM_EJECUTOR where COD_EJECUTOR = ? )"
//			+ "        	   WHERE id_componente = ? ";

	public static final String SQL_SELECT_EXTRACT_ID_SERVICIO_INSTALADO = " select id_servicio from SCHSCOM.cliente where nro_cuenta  = ? "; //CONVERTIDO A SCOM

//	public static final String SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO_INSTALADO = "SELECT id_motivo_estado " //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "    FROM sel_motivo_estado  WHERE cod_interno = 'Instalado' AND activo = 'S'";

	public static final String SQL_SELECT_DATA_IN_MED_COMPONENTE_INSTALAR = "SELECT coalesce(id_dynamicobject,-1) AS idDynamicObject, id_propiedad AS idPropiedad, " //(SCHSCOM)
			+ "		id AS idComponente "
			+ "     FROM SCHSCOM.med_componente "
			+ "     WHERE nro_componente = ? "
			+ "     AND id_modelo = ? ";

	public static final String UPDATE_MED_COMPONENTE_INSTALAR1 = " UPDATE SCHSCOM.med_componente "   //(SCHSCOM)
			+ "	SET id_est_componente = (select id from SCHSCOM.med_est_componente where cod_interno = 'Instalado'), "
			+ "	id_ubicacion = (select id_servicio from SCHSCOM.cliente where nro_cuenta = ?),"
			+ " type_ubicacion = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico',"
			+ " id_propiedad = (select id from SCHSCOM.med_propiedad where cod_propiedad = ?),"
			+ " fecha_suministro = to_date(?, 'DD/MM/YYYY')"
			+ " ,id_dynamicobject = ? "
			+ " WHERE nro_componente = ? "
			+ " AND id_modelo  = ? "
			;
	

	public static final String UPDATE_MED_COMPONENTE_INSTALAR2 = " UPDATE SCHSCOM.med_componente "   //(SCHSCOM) 
			+ "	SET id_est_componente = (select id from SCHSCOM.med_est_componente where cod_interno = 'Instalado'), "
			+ "	id_ubicacion = (select id_servicio from SCHSCOM.cliente where nro_cuenta = ?),"
			+ " type_ubicacion = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico',"
			+ " id_propiedad = (select id from SCHSCOM.med_propiedad where cod_propiedad = ?),"
			+ " fecha_suministro = to_date(?, 'DD/MM/YYYY')"
			+ " WHERE nro_componente = ? "
			+ " AND id_modelo  = ? "
			;

	public static final String SQL_SELECT_MAX_MED_HIS_COMPONENTE_INST = " SELECT max(id_his_componente)"  //(SCHSCOM)
			+ "     FROM SCHSCOM.med_his_componente"
			+ "     WHERE id_componente = ?";

	public static final String UPDATE_MED_HIS_COMPONENT_INST = "UPDATE SCHSCOM.med_his_componente"  //(SCHSCOM)
			+ "       SET fec_hasta = to_date( ?, 'DD/MM/YYYY'),"
			+ "       id_orden = ?"
			+ "     WHERE id_his_componente = ? ";

	public static final String INSERT_MED_HIS_COMPONENT_INST = "INSERT INTO SCHSCOM.MED_HIS_COMPONENTE ("  //(SCHSCOM)
			+ " id_his_componente,"
			+ " id_componente,"
			+ " id_est_componente,"
			+ " fec_desde,"
			+ " id_ubicacion,"
			+ " type_ubicacion,"
			+ " id_orden)"
			+ " SELECT"
			+ " ?,"
			+ " ?,"
			+ " (select id from  SCHSCOM.med_est_componente where cod_interno = 'Instalado'),"
			+ " to_date( ? , 'DD/MM/YYYY'),"
			+ " ?,"
			+ " 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico',"
			+ " ?"
			;

	public static final String INSERT_FWK_AUDITEVENT_INST = "INSERT INTO SCHSCOM.FWK_AUDITEVENT" //CONVERTIDO A SCOM
			+ "    (   id,"
			+ "        usecase,"
			+ "        objectref,"
			+ "        id_fk,"
			+ "        fecha_ejecucion,"
			+ "        specific_auditevent,"
			+ "        id_user)"
			+ "    VALUES"
			+ "    (   ?,"
			+ "        'Medidor.UPDATE',"
			+ "        'com.synapsis.synergia.med.domain.componente.Medidor',"
			+ "        ?,"
			+ "        now(),"
			+ "        'COMPONENTE',"
			+ "        ?)";

	public static final String SQL_SELECT_EXTRACT_ID_MED_TIPO_MAGNITUD_INST = "SELECT id_tip_magnitud " //PARA AMBOS HACER LA CONSULTA AL 4J
			+ "      FROM med_tip_magnitud WHERE cod_interno = 'Instalacion'";

	public static final String SQL_SELECT_EXTRACT_ID_FAC_EST_MAGNITUD_INST = "SELECT id_est_magnitud " //PARA AMBOS HACER LA CONSULTA AL 4J
			+ "      FROM fac_est_magnitud WHERE codigo = '01'";

//	public static final String SQL_SELECT_DATA_IN_SEL_COLOR = "SELECT des_color, id_color " //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "          FROM sel_color"
//			+ "         WHERE activo = 'S' AND cod_color = ?";
//
//	public static final String SQL_SELECT_EXTRACT_ID_SELLO = "SELECT sel.id_bolsa_sellos" //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "          FROM sel_sello sel, sel_bolsa_sellos bol"
//			+ "         WHERE sel.id_motivo_estado = 1 AND sel.id_bolsa_sellos  = bol.id_bolsa_sellos AND sel.nro_sello = ?";

//	public static final String UPDATE_SEL_SELLO_INST = "UPDATE sel_sello SET"  //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "            id_motivo_estado = ?,"
//			+ "            fec_instalacion  = SYSDATE,"
//			+ "            id_origen_inst   = (select id_origen from sel_origen where cod_origen = 'ADM'),"
//			+ "            fec_modificacion = SYSDATE,"
//			+ "            id_ubicacion     = (select id_ubicacion from sel_ubicacion where cod_ubicacion = ? and activo = 'S'),"
//			+ "            id_servicio      = ?,"
//			+ "            id_componente    = ?,"
//			+ "            id_ejec_inst     = (select id_ejecutor from com_ejecutor where cod_ejecutor = ?)"
//			+ "        WHERE nro_sello = ? "
//			+ "          AND id_motivo_estado = 1 "
//			+ "          AND id_bolsa_sellos = ? ";
//
//	public static final String INSERT_SEL_HIS_SELLO = "INSERT INTO SEL_HIS_SELLO "  //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "(  id_his_sello, "
//			+ "   id_empresa, "
//			+ "   serie, "
//			+ "   color, "
//			+ "   nro_sello, "
//			+ "   estado_sello, "
//			+ "   contratista_inst, "
//			+ "   ejecutor_inst, "
//			+ "   ubicacion, "
//			+ "   srv_electrico, "
//			+ "   tip_componente, "
//			+ "   marca_componente, "
//			+ "   modelo_componente, "
//			+ "   fec_creacion, "
//			+ "   fec_instalacion "
//			+ ")  "
//			+ " VALUES "
//			+ " (  SQHISTORIASELLO.NEXTVAL, "
//			+ "	 ?,"
//			+ "(select serie from sel_bolsa_sellos WHERE id_bolsa_sellos = ? ),"
//			+ " ?,"
//			+ " ?,"
//			+ " (select des_est_sello from sel_est_sello where cod_interno = 'Instalado'),"
//			+ " (select con.cod_contratista || ' ' || trim(np.nombre) from com_contratista con, nuc_persona np"
//			+ "  where np.id_persona = con.id_persona and con.cod_contratista = ?),"
//			+ " (select  trim(np.nombre) || ' ' || trim(np.apellido_pat) || ' ' || trim(np.apellido_mat) from com_ejecutor eje, nuc_persona np "
//			+ " where np.id_persona = eje.id_persona and eje.cod_ejecutor = ?),"
//			+ " (select des_ubicacion from sel_ubicacion where cod_ubicacion = ? and activo = 'S'),"
//			+ " (select 'Servicio Nro:' || ' ' || ns.nro_servicio from nuc_cuenta nc, nuc_servicio ns, srv_electrico se "
//			+ " where nc.id_cuenta = ns.id_cuenta and ns.id_servicio = se.id_servicio  and ns.tipo = 'ELECTRICO'  and nc.nro_cuenta = ?),"
//			+ " (select des_tip_componente from med_tip_componente where cod_tip_componente = 'ME'),"
//			+ " (select des_marca from med_marca where cod_marca = ? and activo = 'S'),"
//			+ " (select mo.des_modelo from med_modelo mo, med_marca ma "
//			+ " where mo.id_marca = ma.id_marca and mo.cod_modelo = ? and mo.activo = 'S' and ma.cod_marca = ? and ma.activo = 'S'),"
//			+ " (select s.fec_creacion from sel_bolsa_sellos bs, sel_sello s "
//			+ " where s.nro_sello = ? and bs.id_bolsa_sellos = s.id_bolsa_sellos and bs.id_color = ? and bs.id_bolsa_sellos = ? ),"
//			+ " SYSDATE)";
//
//	public static final String SQL_COUNT_SEL_SELLO = " SELECT count(*)" //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "  FROM sel_sello sel"
//			+ "  WHERE id_bolsa_sellos = ? "
//			+ "  AND id_motivo_estado IN (select id_motivo_estado from sel_motivo_estado where cod_interno in('Creado','Disponible'))";
//
//	public static final String UPDATE_SEL_BOLSA_SELLOS = "UPDATE sel_bolsa_sellos" //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "  SET id_estado   = (select ID_EST_BOLSA from SEL_EST_BOLSA where COD_INTERNO='Terminada'),"
//			+ "  id_ejecutor = (select  eje.id_ejecutor from com_ejecutor eje, nuc_persona np"
//			+ "  where np.id_persona = eje.id_persona and eje.cod_ejecutor = ? ),"
//			+ "  fec_termino = SYSDATE"
//			+ "  WHERE id_bolsa_sellos = ?";
//
//	public static final String INSERT_SEL_HIS_SELLO2 = "INSERT INTO SEL_HIS_SELLO " //(OMITIR CODIGO PARA EL CURSOR SCHSCOM DEJAR PARA EL CURSOR DE 4J)
//			+ "(  id_his_sello, "
//			+ "   id_empresa, "
//			+ "   serie, "
//			+ "   color, "
//			+ "   nro_sello, "
//			+ "   estado_sello, "
//			+ "   contratista_inst, "
//			+ "   ejecutor_inst, "
//			+ "   ubicacion, "
//			+ "   srv_electrico, "
//			+ "   tip_componente, "
//			+ "   marca_componente, "
//			+ "   modelo_componente, "
//			+ "   fec_creacion, "
//			+ "   fec_instalacion "
//			+ ")  "
//			+ " VALUES "
//			+ " (  SQHISTORIASELLO.NEXTVAL, "
//			+ "	 ?,"
//			+ "(select serie from sel_bolsa_sellos WHERE id_bolsa_sellos = ? ),"
//			+ " ?,"
//			+ " ?,"
//			+ " (select des_est_sello from sel_est_sello where cod_interno = 'Instalado'),"
//			+ " (select con.cod_contratista || ' ' || trim(np.nombre) from com_contratista con, nuc_persona np"
//			+ "  where np.id_persona = con.id_persona and con.cod_contratista = ?),"
//			+ " (select  trim(np.nombre) || ' ' || trim(np.apellido_pat) || ' ' || trim(np.apellido_mat) from com_ejecutor eje, nuc_persona np "
//			+ " where np.id_persona = eje.id_persona and eje.cod_ejecutor = ?),"
//			+ " (select des_ubicacion from sel_ubicacion where cod_ubicacion = ? and activo = 'S'),"
//			+ " (select 'Servicio Nro:' || ' ' || ns.nro_servicio from nuc_cuenta nc, nuc_servicio ns, srv_electrico se "
//			+ " where nc.id_cuenta = ns.id_cuenta and ns.id_servicio = se.id_servicio  and ns.tipo = 'ELECTRICO'  and nc.nro_cuenta = ?),"
//			+ " (select des_tip_componente from med_tip_componente where cod_tip_componente = 'ME'),"
//			+ " (select des_marca from med_marca where cod_marca = ? and activo = 'S'),"
//			+ " (select mo.des_modelo from med_modelo mo, med_marca ma "
//			+ " where mo.id_marca = ma.id_marca and mo.cod_modelo = ? and mo.activo = 'S' and ma.cod_marca = ? and ma.activo = 'S'),"
//			+ " (select s.fec_creacion from sel_bolsa_sellos bs, sel_sello s "
//			+ " where s.nro_sello = ? and bs.id_bolsa_sellos = s.id_bolsa_sellos and bs.id_color = ? and bs.id_bolsa_sellos = ? ),"
//			+ " SYSDATE)";

	public static final String QUERY_BFN_EXISTE_CODIGO_LECTURA = "SELECT count(*)"  //CONVERTIDO A SCOM
			+ "	 FROM SCHSCOM.med_medida "
			+ "	 WHERE cod_medida = ? ";

	public static final String QUERY_BFN_EXISTE_CUENTA = "SELECT 1"  //CONVERTIR A SCOM (DUDA AQUÍ - NUC_CUENTA? NO EXISTE EN SCOM) 4J SACARLO
			+ "	 FROM NUC_CUENTA"
			+ "	 WHERE id_empresa = ? "
			+ "	 AND nro_cuenta = ? ";

	public static final String QUERY_BFN_CUENTA_EN_FACTURACION = "SELECT count(*)"  //SIEMPRE 4J
			+ "	 FROM nuc_cuenta nc, cia_agrup_ciclo ac, fac_estado_ciclo ec "
			+ "	 WHERE nc.id_cuenta = ac.id_cuenta "
			+ "	 AND ac.id_estado_ciclo = ec.id_estado_ciclo "
			+ "	 AND ac.id_estado_ciclo <>18 "
			+ "	 AND nc.nro_cuenta =? ";

	public static final String QUERY_BFN_TIENE_SERVICIO_ELECTRICO = "SELECT trim(WSE.ID_STATE) AS cEstadoWkf, SE.ID_ESTADO AS lIDEstado, SE.ID_SERVICIO AS idServicio "  //(CONVERTIR A SCOM -DUDA AQUÍ ID_WORKFLOW NO SE ENCUENTRA EN TABLA CLIENTE) 4J SACARLO
			+ "	 FROM NUC_CUENTA NC, NUC_SERVICIO NS, SRV_ELECTRICO SE, WKF_WORKFLOW WSE "
			+ "	 WHERE NC.ID_CUENTA = NS.ID_CUENTA "
			+ "	 AND NS.ID_SERVICIO = SE.ID_SERVICIO "
			+ "	 AND NS.TIPO = 'ELECTRICO' "
			+ "	 AND SE.ID_WORKFLOW = WSE.ID_WORKFLOW "
			+ "	 AND NC.NRO_CUENTA = ? "
			+ "	 AND WSE.ID_STATE not in ('Retirado', 'SujetoVerificacionRetirado') ";

	public static final String QUERY_BFN_SE_VERIFICA_VENTA = "SELECT COUNT(*) "
			+ "	 FROM NUC_CUENTA NC, NUC_SERVICIO NS, SRV_ELECTRICO SE, PRD_PRODUCTO PRD, VTA_SOL_SRV_ELE SELE, VTA_SRV_VENTA SVEN, ORD_ORDEN OVEN, WKF_WORKFLOW WORD" //SIEMPRE 4J
			+ "	 WHERE NC.ID_CUENTA = NS.ID_CUENTA"
			+ "	 AND NS.ID_SERVICIO = SE.ID_SERVICIO"
			+ "	 AND NS.TIPO = 'ELECTRICO'"
			+ "	 AND SE.ID_SERVICIO = SELE.ID_SRV_ELECTRICO"
			+ "	 AND SELE.ID_SOL_SRV_ELE = SVEN.ID_SOL_SRV_ELE"
			+ "	 AND SVEN.ID_ORD_VENTA = OVEN.ID_ORDEN"
			+ "  AND SVEN.ID_PRODUCTO = PRD.ID_PRODUCTO"
			+ "  AND PRD.TIP_PRODUCTO = 'MOD'"
			+ "	 AND OVEN.ID_WORKFLOW = WORD.ID_WORKFLOW"
			+ "	 AND WORD.ID_STATE NOT IN('Anulada', 'Finalizada', 'RevisionFinal')"
			+ "	 AND NC.NRO_CUENTA = ? ";

	public static final String QUERY_BFN_VENTA_POR_MODIFICACION = "SELECT COUNT(*) "
			+ "	 FROM NUC_CUENTA NC, NUC_SERVICIO NS, SRV_ELECTRICO SE, PRD_PRODUCTO PRD, VTA_SOL_SRV_ELE SELE, VTA_SRV_VENTA SVEN, ORD_ORDEN OVEN, WKF_WORKFLOW WORD" //SIEMPRE 4J
			+ "	 WHERE NC.ID_CUENTA = NS.ID_CUENTA"
			+ "	 AND NS.ID_SERVICIO = SE.ID_SERVICIO"
			+ "	 AND NS.TIPO = 'ELECTRICO'"
			+ "	 AND SE.ID_SERVICIO = SELE.ID_SRV_ELECTRICO"
			+ "	 AND SELE.ID_SOL_SRV_ELE = SVEN.ID_SOL_SRV_ELE"
			+ "	 AND SVEN.ID_ORD_VENTA = OVEN.ID_ORDEN"
			+ "	 AND SVEN.ID_PRODUCTO = PRD.ID_PRODUCTO"
			+ "	 AND PRD.TIP_PRODUCTO = 'MOD'"
			+ "	 AND SVEN.ESTADO='Ejecutado'"
			+ "	 AND OVEN.ID_WORKFLOW = WORD.ID_WORKFLOW"
			+ "	 AND WORD.ID_STATE NOT IN('Anulada', 'Finalizada', 'RevisionFinal')"
			+ "	 AND NC.NRO_CUENTA = ? ";

	public static final String QUERY_BFN_EXISTE_CONTRATISTA = "SELECT 1 " //CONVERTIDO A SCOM
			+ "	 FROM SCHSCOM.com_contratista "
			+ "	 WHERE cod_contratista = ? "
			+ "	 AND activo = 'S' ";

	public static final String QUERY_BFN_EXISTE_EJECUTOR = "SELECT 1 " //CONVERTIDO A SCOM
			+ "	 FROM SCHSCOM.com_contratista con, SCHSCOM.com_ejecutor eje "
			+ "	 WHERE eje.cod_ejecutor = ? "
			+ "	 AND eje.activo = 'S'"
			+ "	 AND con.id = eje.id_contratista ";

	public static final String SQL_SELECT_CARGA_FECHA_1 = "SELECT TO_CHAR(NOW(),'DD/MM/YYYY HH24:MI:SS')"; //CONVERTIDO A SCOM

	public static final String SQL_SELECT_CARGA_FECHA_2 = "SELECT TO_CHAR(NOW(),'DD/MM/YYYY')"; //CONVERTIDO A SCOM

	public static final String SQL_BFN_OBTENER_FECHA_EJECUCION_VTA = "SELECT to_char(sven.fecha_ejecucion, 'DD/MM/YYYY')" //SIEMPRE 4J
			+ "	 FROM nuc_cuenta nc, nuc_servicio ns, srv_electrico se, vta_sol_srv_ele sele, vta_srv_venta sven"
			+ "	 WHERE nc.id_cuenta = ns.id_cuenta"
			+ "	 AND ns.id_servicio = se.id_servicio"
			+ "	 AND ns.tipo = 'ELECTRICO'"
			+ "	 AND se.id_servicio = sele.id_srv_electrico"
			+ "	 AND sele.id_sol_srv_ele = sven.id_sol_srv_ele"
			+ "	 AND nc.nro_cuenta = ? "
			+ "	 AND sven.estado = 'Ejecutado'";

	public static final String QUERY_BFN_EXISTE_CODIGO_PROPIEDAD_MEDIDOR = "SELECT count(*) " //CONVERTIDO A SCOM
			+ "	 FROM SCHSCOM.med_propiedad "
			+ "	 WHERE cod_propiedad = ? "
			+ "	 AND activo = 'S' ";

//	public static final String QUERY_BFN_EXISTE_NRO_SELLO = "SELECT count(*) "   //(OMITIR CODIGO PARA EL CURSOR SCHSCOM, DEJARLO PARA 4J)
//			+ "	 FROM sel_sello sel, sel_bolsa_sellos bol "
//			+ "	 WHERE sel.id_motivo_estado = 1  "
//			+ "	 AND sel.id_bolsa_sellos  = bol.id_bolsa_sellos "
//			+ "	 AND sel.nro_sello = ? ";
//
//	public static final String QUERY_BFN_EXISTE_COLOR_SELLO = "SELECT count(*) "  //(OMITIR CODIGO PARA EL CURSOR SCHSCOM, DEJARLO PARA 4J)
//			+ "	 FROM SEL_COLOR "
//			+ "	 WHERE activo = 'S' "
//			+ "	 AND cod_color = ? ";
//
//	public static final String QUERY_BFN_EXISTE_UBICACION_SELLO = "SELECT count(*) "  //(OMITIR CODIGO PARA EL CURSOR SCHSCOM, DEJARLO PARA 4J)
//			+ "	FROM sel_ubicacion "
//			+ "	WHERE cod_ubicacion = ? "
//			+ "	AND activo = 'S' ";
//	
	public static final String GET_ID_CLIENTE = "select cli.id_cliente from nuc_cliente cli, nuc_cuenta nc " 
			+ " where cli.id_cliente = nc.id_cliente and nc.nro_cuenta = ? ";

	public static final String GET_ID_CONTRATISTA = "select id_contratista from com_contratista where cod_contratista = ?";

	// R.I. Correctivo incidencia INC000110799119 19/07/2023 INICIO
	public static final String SQL_SELECT_NEXTVAL_SQMEDHISCOMPONENTE = "select NEXTVAL('sqauditevent')";
	// R.I. Correctivo incidencia INC000110799119 19/07/2023 FIN
	
	
	// INICIO - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 
	
	public static final String SQLPOSTGRES_SELECT_REGISTRO_MEDIDORES = "SELECT "
			+ "	 	   unnest(xpath('/item/numeroMedidor/text()', o.REG_XML))::text AS numeroMedidor, "
			+ "	 	   unnest(xpath('/item/marcaMedidor/text()', o.REG_XML))::text AS marcaMedidor, "
			+ "	 	   unnest(xpath('/item/modeloMedidor/text()', o.REG_XML))::text AS modeloMedidor, "
			+ "	 	   unnest(xpath('/item/factorMedidor/text()', o.REG_XML))::text AS factorMedidor, "
			+ "	 	   unnest(xpath('/item/accionMedidor/text()', o.REG_XML))::text AS accionMedidor "
			+ "		 FROM "
			+ "		 (SELECT  unnest(xpath('/medidores/item', "
			+ "				  unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores', d.REG_XML)))) AS REG_XML "
			+ "		 FROM SCHSCOM.EOR_ORD_TRANSFER t, "
			+ "		 SCHSCOM.EOR_ORD_TRANSFER_DET d "
			+ "		 WHERE t.id_ord_transfer = d.ID_ORD_TRANSFER "
			+ "		 AND COD_TIPO_ORDEN_EORDER in ( 'NCX.03', 'NCX.05') "
			+ "		 AND accion = 'RECEPCION' "
			+ "		 AND t.nro_orden_legacy = ? "
			+ "		 )o";

	public static final String SQLPOSTGRES_ACTUALIZAR_MEDIDORES = " UPDATE med_medida_medidor AS mmm   "
			+ "SET id_factor = mfm.id_factor,   "
			+ "    val_factor = mf.val_factor   "
			+ "FROM med_medida_modelo AS mmm1   "
			+ "JOIN med_fac_med_mod AS mfm ON mmm1.id = mfm.id_medida_modelo   "
			+ "JOIN med_factor AS mf ON mfm.id_factor = mf.id   "
			+ "WHERE mmm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar      "
			+ "  where mc.id_modelo = mmod.id      "
			+ "  and mmod.id_marca = mmar.id      "
			+ "  and mc.nro_componente = ?      "
			+ "  and mmod.cod_modelo = ?   "
			+ "  and mmar.cod_marca = ? )    "
			+ "    AND mmm1.id_modelo = (select mmod.id    "
			+ "from med_modelo mmod, med_marca mmar      "
			+ "  where mmod.cod_modelo = ?       "
			+ "  and mmar.cod_marca = ?      "
			+ "  and mmod.id_marca = mmar.id)   "
			+ "    AND mf.cod_factor = ?   "
			+ "    AND mmm.id_medida = mmm1.id_medida   "
			+ "    AND NOT EXISTS (   "
			+ "        SELECT 1   "
			+ "        FROM med_medida_medidor AS mm   "
			+ "        WHERE mm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar      "
			+ "		  where mc.id_modelo = mmod.id      "
			+ "		  and mmod.id_marca = mmar.id      "
			+ "		  and mc.nro_componente = ?      "
			+ "		  and mmod.cod_modelo = ?   "
			+ "		  and mmar.cod_marca = ? )    "
			+ "        AND NOT EXISTS (   "
			+ "            SELECT 1   "
			+ "            FROM med_medida_modelo AS mmm2   "
			+ "            JOIN med_fac_med_mod AS mfm2 ON mmm2.id = mfm2.id_medida_modelo   "
			+ "            JOIN med_factor AS mf2 ON mfm2.id_factor = mf2.id   "
			+ "            WHERE mmm2.id_modelo = (select mmod.id    "
			+ "			from med_modelo mmod, med_marca mmar      "
			+ "			  where mmod.cod_modelo = ?       "
			+ "			  and mmar.cod_marca = ?      "
			+ "			  and mmod.id_marca = mmar.id)   "
			+ "            AND mf2.cod_factor = ?   "
			+ "            AND mm.id_medida = mmm2.id_medida )); ";
	
	public static final String SQLPOSTGRES_SELECT_MEDIDOR_ENCONTRADO = "select distinct mf.cod_factor   "
			+ "from schscom.med_componente mc,   "
			+ "schscom.med_modelo mm,   "
			+ "schscom.med_marca mm2,   "
			+ "schscom.med_medida_medidor mmm,   "
			+ "schscom.med_factor mf   "
			+ "where mc.id_modelo = mm.id   "
			+ "and mm.id_marca = mm2.id   "
			+ "and mc.id = mmm.id_componente   "
			+ "and mmm.id_factor = mf.id   "
			+ "and mm.cod_modelo = ?    "
			+ "and mm2.cod_marca = ?   "
			+ "and mc.nro_componente = ?  ";
	
	// FIN - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 
}
