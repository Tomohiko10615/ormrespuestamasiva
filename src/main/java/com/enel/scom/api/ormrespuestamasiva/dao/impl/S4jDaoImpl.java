package com.enel.scom.api.ormrespuestamasiva.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enel.scom.api.ormrespuestamasiva.dao.I4jDAO;
import com.enel.scom.api.ormrespuestamasiva.mapper.S4jMapper;
import com.enel.scom.api.ormrespuestamasiva.mapper.SCOMMapper;

@Repository
public class S4jDaoImpl implements I4jDAO {

	@Autowired
	@Qualifier("orclTemplate")
	private JdbcTemplate jdbcORCLTemplate;
	
	@Autowired
	@Qualifier("postgreTemplate")
	private JdbcTemplate jdbcPostgreTemplate;
			
	private static final Logger logger = LoggerFactory.getLogger(S4jDaoImpl.class);

	@Override
	public Map<String, Object> getDatosdeRegistro(String cNroOrdORM) { 
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = S4jMapper.SQL_SELECT_DATOS_DE_REGISTRO;  //(SCHSCOM)
			logger.debug("Executing query S4jMapper.SQL_SELECT_DATOS_DE_REGISTRO");
			result= jdbcPostgreTemplate.queryForMap(sqlString,cNroOrdORM);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATOS_DE_REGISTRO: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATOS_DE_REGISTRO: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Transactional
	@Override
	public void updateRegistroCabecera(String vCodOperacion, String vObservacion, String vCodErrASY000, Integer cOD_EST_RECEP,
			Integer cOD_EST_RECER, long vIdOrdTransfer) {
		int rows=0;
		try {
			String sqlString = S4jMapper.SQL_ACTUALIZAR_EOR_ORD_TRANSFER;  //(SCHSCOM)
			logger.debug("Executing query S4jMapper.SQL_ACTUALIZAR_EOR_ORD_TRANSFER");
			rows = jdbcPostgreTemplate.update(sqlString,vCodOperacion,vObservacion,vCodOperacion,vCodErrASY000,cOD_EST_RECEP,cOD_EST_RECER,vIdOrdTransfer);
			logger.info("Método bfnActualizaNroOrdenLegacy, filas afectadas: {}", rows);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_ACTUALIZAR_EOR_ORD_TRANSFER: {}",ex.getMessage());
			throw ex;
		}
	}

	@Transactional
	@Override
	public void updateRegistroDetalle(String vCodOperacion, long vIdOrdTransfer, int vNroRecepciones) {
		int rows=0;
		try {
			String sqlString = S4jMapper.SQL_ACTUALIZAR_EOR_ORD_TRANSFER_DET;  //(SCHSCOM)
			logger.debug("Executing query S4jMapper.SQL_ACTUALIZAR_EOR_ORD_TRANSFER_DET");
			rows = jdbcPostgreTemplate.update(sqlString,vCodOperacion,vIdOrdTransfer,vNroRecepciones);
			logger.info("Método bfnActualizaNroOrdenLegacy, filas afectadas: {}", rows);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_ACTUALIZAR_EOR_ORD_TRANSFER_DET: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public Long getidMotNoeje(String pCodMotNoEje) { 
		try {
			String sqlString = S4jMapper.SQL_GET_ID_MOT_NO_EJE;
			logger.debug("Executing query S4jMapper.SQL_GET_ID_MOT_NO_EJE");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,pCodMotNoEje);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_GET_ID_MOT_NO_EJE: {}",ee.getMessage());
			return 0L;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_GET_ID_MOT_NO_EJE: {}",e.getMessage());
			return 0L;
		}
	}

	@Override
	public Map<String, Object> bfnExisteOrden(String pOrden) { 
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = S4jMapper.SQL_BFN_EXISTE_ORDEN; 
			logger.debug("Executing query S4jMapper.SQL_BFN_EXISTE_ORDEN");
			result= jdbcORCLTemplate.queryForMap(sqlString,pOrden);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_ORDEN: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_ORDEN: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public int bfnExisteCuentaORM(Long pCuenta) {
		try {
			String sqlString = S4jMapper.SQL_BFN_EXISTE_CUENTA_ORM;
			logger.debug("Executing query S4jMapper.SQL_BFN_EXISTE_CUENTA_ORM");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_CUENTA_ORM: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_CUENTA_ORM: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public int bfnExisteContratistaORM(String pCodContra) {
		try {
			String sqlString = S4jMapper.SQL_BFN_EXISTE_CONTRATISTA_ORM;
			logger.debug("Executing query S4jMapper.SQL_BFN_EXISTE_CUENTA_ORM");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pCodContra);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_CONTRATISTA_ORM: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_CONTRATISTA_ORM: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> bfnExisteTipoDocum(String pTipoDocum) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = S4jMapper.SQL_BFN_EXISTE_TIPO_DOCUM;
			logger.debug("Executing query S4jMapper.SQL_BFN_EXISTE_TIPO_DOCUM");
			result= jdbcORCLTemplate.queryForMap(sqlString,pTipoDocum);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_TIPO_DOCUM: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_TIPO_DOCUM: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public Map<String, Object> bfnExisteTarea(String pNroTarea) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = S4jMapper.SQL_BFN_EXISTE_TAREA;
			logger.debug("Executing query S4jMapper.SQL_BFN_EXISTE_TAREA");
			result= jdbcORCLTemplate.queryForMap(sqlString,pNroTarea);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_TAREA: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_EXISTE_TAREA: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public String bfnValidarIdentificador(String pNroTarea, String pIdentif) {
		try {
			String sqlString = S4jMapper.SQL_BFN_VALIDAR_IDENTIFICADOR;
			logger.debug("Executing query S4jMapper.SQL_BFN_VALIDAR_IDENTIFICADOR");
			return jdbcORCLTemplate.queryForObject(sqlString,String.class,pIdentif,pIdentif,pIdentif,pIdentif,pNroTarea);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_BFN_VALIDAR_IDENTIFICADOR: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_VALIDAR_IDENTIFICADOR: {}",e.getMessage());
			return "";
		}
	}

	@Override
	public long getCodigoContratista(String cCodContra) {
		try {
			String sqlString = S4jMapper.SQL_GET_CODIGO_CONTRATISTA;
			logger.debug("Executing query S4jMapper.SQL_GET_CODIGO_CONTRATISTA");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,cCodContra);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_GET_CODIGO_CONTRATISTA: {}",ee.getMessage());
		 	return 0L; 
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_GET_CODIGO_CONTRATISTA: {}",e.getMessage());
			return 0L; 
		}
	}

	@Override
	public long getCodigoEjecutor(String cNombreTec) {
		try {
			String sqlString = S4jMapper.SQL_GET_CODIGO_EJECUTOR;
			logger.debug("Executing query S4jMapper.SQL_GET_CODIGO_EJECUTOR");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,cNombreTec);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_GET_CODIGO_EJECUTOR: {}",ee.getMessage());
			return 0L;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_GET_CODIGO_EJECUTOR: {}",e.getMessage());
			return 0L;
		}
	}

	@Override
	public long getCuenta(long lNroCuenta) {
		try {
			String sqlString = S4jMapper.SQL_GET_CUENTA;
			logger.debug("Executing query S4jMapper.SQL_GET_CUENTA");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,lNroCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_GET_CUENTA: {}",ee.getMessage());
			return 0L;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_GET_CUENTA: {}",e.getMessage());
			return 0L;
		}
	}

	//@Transactional
	@Override
	public void updateOrdOrdenSetLeidoAndFechaIngresoEstadoActual(long IdOrden) {
		int rows=0;
		try {
			String sqlString = S4jMapper.SQL_ACTUALIZAR_UPDATE_ORD_ORDEN;
			logger.debug("Executing query S4jMapper.SQL_ACTUALIZAR_UPDATE_ORD_ORDEN");
			rows = jdbcORCLTemplate.update(sqlString,IdOrden);
			logger.info("Método update_ord_orden, filas afectadas: {}", rows);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_ACTUALIZAR_UPDATE_ORD_ORDEN: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public String getFechaVencimientoOrdOrden(long idOrden) {
		try {
			String sqlString = S4jMapper.SQL_GET_FECHA_VENCIMIENTO;
			logger.debug("Executing query S4jMapper.SQL_GET_FECHA_VENCIMIENTO");
			return jdbcORCLTemplate.queryForObject(sqlString,String.class,idOrden);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_GET_FECHA_VENCIMIENTO: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_GET_FECHA_VENCIMIENTO: {}",e.getMessage());
			return "";
		}
	}

	@Override
	public String getCursorDatosFechaVencimiento(long idOrden) {
		try {
			String sqlString = S4jMapper.SQL_GET_CURSOR_FECHA_VENCIMIENTO;
			logger.debug("Executing query S4jMapper.SQL_GET_CURSOR_FECHA_VENCIMIENTO");
			return jdbcORCLTemplate.queryForObject(sqlString,String.class,idOrden);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_GET_CURSOR_FECHA_VENCIMIENTO: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_GET_CURSOR_FECHA_VENCIMIENTO: {}",e.getMessage());
			return "";
		}
	}

	//@Transactional
	@Override
	public void updateOrdOrdenSetFechaVencimiento(String ccFechaVenc,long idOrden) {
		int rows=0;
		try {
			String sqlString = S4jMapper.SQL_ACTUALIZAR_UPDATE_ORD_ORDEN_FECHA_VENCIMIENTO;
			logger.debug("Executing query S4jMapper.SQL_ACTUALIZAR_UPDATE_ORD_ORDEN_FECHA_VENCIMIENTO");
			rows = jdbcORCLTemplate.update(sqlString,ccFechaVenc,idOrden);
			logger.info("Método update_ord_orden, filas afectadas: {}", rows);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_ACTUALIZAR_UPDATE_ORD_ORDEN_FECHA_VENCIMIENTO: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void updateWFKWorkflowSetIdStateAndParentTypeAndIdEmpresa(Integer idEmpresa,long idWorkflow) {
		logger.info("idEmpresa: {}", idEmpresa);
		logger.info("idWorkflow: {}", idWorkflow);
		int rows=0;
		try {
			String sqlString = S4jMapper.SQL_ACTUALIZAR_UPDATE_WORK_FLOW_SET_ID_STATE_PARENTTYPE_IDEMPRESA;
			logger.debug("Executing query S4jMapper.SQL_ACTUALIZAR_UPDATE_WORK_FLOW_SET_ID_STATE_PARENTTYPE_IDEMPRESA");
			rows = jdbcORCLTemplate.update(sqlString,idEmpresa,idWorkflow);
			logger.info("Método updateWFKWorkflow_SetIdStateAndParentTypeAndIdEmpresa, filas afectadas: {}", rows);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_ACTUALIZAR_UPDATE_WORK_FLOW_SET_ID_STATE_PARENTTYPE_IDEMPRESA: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void updateOrmOrdenSetIdContratistaAndIEjecutorAndAtendioSeguroAndIdParenNotificado(long lNronotif,
			long gIdContratista, long gIdEjecutor, long idOrden) {
		int rows=0;
		try {
			String sqlString = S4jMapper.SQL_ACTUALIZAR_UPDATE_ORM_ORDEN_SET_IDCONTRATISTA_IDEJECUTOR_ATENDIOSEGURO_IDPARENTNOTIFICADO;
			logger.debug("Executing query S4jMapper.SQL_ACTUALIZAR_UPDATE_ORM_ORDEN_SET_IDCONTRATISTA_IDEJECUTOR_ATENDIOSEGURO_IDPARENTNOTIFICADO");
			rows = jdbcORCLTemplate.update(sqlString,lNronotif,gIdContratista,gIdEjecutor,idOrden);
			logger.info("Método updateOrmOrden_SetIdContratistaAndIEjecutorAndAtendioSeguroAndIdParenNotificado, filas afectadas: {}", rows);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_ACTUALIZAR_UPDATE_ORM_ORDEN_SET_IDCONTRATISTA_IDEJECUTOR_ATENDIOSEGURO_IDPARENTNOTIFICADO: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQAUDITEVENT() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQAUDITEVENT;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQAUDITEVENT");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQAUDITEVENT: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQAUDITEVENT: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long selectNextValSQMAGNITUDIMPL() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQMAGNITUDIMPL;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQMAGNITUDIMPL");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQMAGNITUDIMPL: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQMAGNITUDIMPL: {}",e.getMessage());
			return 0;
		}
	}

	

	//@Transactional
	@Override
	public void insertFWK_AUDITEVENT(long id_audit_event, Integer gIdEmprGlob, long idOrden, Integer pIdUser) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_FWK_AUDITEVENT;
			logger.debug("Executing query S4jMapper.SQL_INSERT_FWK_AUDITEVENT");
			jdbcORCLTemplate.update(sqlString,id_audit_event,gIdEmprGlob,idOrden,pIdUser);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_FWK_AUDITEVENT: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertORDHISTORICO(long id_audit_event, long idBuzon) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORD_HISTORICO;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORD_HISTORICO");
			jdbcORCLTemplate.update(sqlString,id_audit_event,idBuzon);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORD_HISTORICO: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertORDOBSERVACION(Integer pIDEmpr, String cObservacion, String gFechaEjecLarga, Integer pIdUser,
			long idOrden) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORD_OBSERVACION;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORD_OBSERVACION");
			jdbcORCLTemplate.update(sqlString,pIDEmpr,cObservacion,gFechaEjecLarga,pIdUser,idOrden);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORD_OBSERVACION: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQRESPUESTAORDENMANTENIMIENTO() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQRESPUESTAORDENMANTENIMIENTO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQRESPUESTAORDENMANTENIMIENTO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQRESPUESTAORDENMANTENIMIENTO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQRESPUESTAORDENMANTENIMIENTO: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMRESPUESTA(long gSeqOrmIdResp, Integer pIDEmpr, long idOrden, Integer pIdUser, String gEjecuto,
			String gFechaEjecLarga, String ccFechaVenc, long lNroNotif, long gIdContratista, String cNombreTec,
			String cNombreCli, long cParent, long gIdServicio, int gIdTipDocPersona, String cNroDocum, String rOT_Ejecutada,
			long idMotiNoEjecuc) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_RESPUESTA;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_RESPUESTA");
			jdbcORCLTemplate.update(sqlString,gSeqOrmIdResp,pIDEmpr,idOrden,pIdUser,gEjecuto,gFechaEjecLarga,ccFechaVenc,lNroNotif,gIdContratista,cNombreTec,cNombreCli,cParent,cParent,
					gIdServicio,gIdTipDocPersona,cNroDocum,rOT_Ejecutada,idMotiNoEjecuc);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_RESPUESTA: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERRAIZORDENMANTENIMIENTO() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERRAIZORDENMANTENIMIENT;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERRAIZORDENMANTENIMIENT");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERRAIZORDENMANTENIMIENT: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERRAIZORDENMANTENIMIENT: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMINRAIZXML(long lid_raiz_xml, Integer pIDEmpr) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORD_IN_RAIZ_XML;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORD_OBSERVACION");
			jdbcORCLTemplate.update(sqlString,lid_raiz_xml,pIDEmpr);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORD_OBSERVACION: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertORMINTIPDOC(Integer pIDEmpr, String gTipDocCodInter) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORD_IN_TIPO_DOC;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORD_IN_TIPO_DOC");
			jdbcORCLTemplate.update(sqlString,pIDEmpr,gTipDocCodInter);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORD_IN_TIPO_DOC: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERPARENTESCO() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERPARENTESCO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERPARENTESCO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERPARENTESCO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERPARENTESCO: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMINPARENTESCO(long lid_parentesco, Integer pIDEmpr, long cParent) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORD_IN_PARENTESCO;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORD_IN_TIPO_DOC");
			jdbcORCLTemplate.update(sqlString,lid_parentesco,pIDEmpr,cParent);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORD_IN_TIPO_DOC: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERCONTRATISTA() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERCONTRATISTA;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERCONTRATISTA");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERCONTRATISTA: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERCONTRATISTA: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMINCONTRATISTA(long lid_contratista, Integer pIDEmpr, String cCodContra) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_IN_CONTRATISTA;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_IN_CONTRATISTA");
			jdbcORCLTemplate.update(sqlString,lid_contratista,pIDEmpr,cCodContra);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_IN_CONTRATISTA: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERMOTIVONOEJECUCIONORDENM() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERMOTIVONOEJECUCIONORDENM;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERMOTIVONOEJECUCIONORDENM");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERMOTIVONOEJECUCIONORDENM: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERMOTIVONOEJECUCIONORDENM: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMINMOTIVO(long lid_motivo, Integer pIDEmpr, String iMotiNoEjecuc) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_IN_MOTIVO;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_IN_MOTIVO");
			jdbcORCLTemplate.update(sqlString,lid_motivo,pIDEmpr,iMotiNoEjecuc);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_IN_MOTIVO: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERRESPUESTAORDENMANTENIMI() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERRESPUESTAORDENMANTENIMI;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERRESPUESTAORDENMANTENIMI");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERRESPUESTAORDENMANTENIMI: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERRESPUESTAORDENMANTENIMI: {}",e.getMessage());
			return 0;
		}
	}

	
	//@Transactional
	@Override
	public void insertORMINRESP(long gSeqOrmInResp, Integer pIDEmpr, String gEjecuto, String gFechaEjecLarga,
			long lNroNotif, long lid_contratista, String cNombreTec, long lid_parentesco, String cNombreCli,
			int gIdTipDocPersona, String cNroDocum, String rOT_Ejecutada, long lid_motivo) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_IN_RESP;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_IN_RESP");
			jdbcORCLTemplate.update(sqlString,gSeqOrmInResp,pIDEmpr,gEjecuto,gFechaEjecLarga,lNroNotif,lid_contratista,cNombreTec,lid_parentesco,cNombreCli,gIdTipDocPersona,
					cNroDocum,rOT_Ejecutada,lid_motivo);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_IN_RESP: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERSERVICIOASOCIADO() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERSERVICIOASOCIADO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERSERVICIOASOCIADO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERSERVICIOASOCIADO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERSERVICIOASOCIADO: {}",e.getMessage());
			return 0;
		}
	}

	
	//@Transactional
	@Override
	public void insertORMINSERVICIO(long lid_servicio, Integer pIDEmpr, long gIdServicio) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_IN_SERVICIO;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_IN_SERVICIO");
			jdbcORCLTemplate.update(sqlString,lid_servicio,pIDEmpr,gIdServicio);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_IN_SERVICIO: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERORDENMANTENIMIENTO() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORDENMANTENIMIENTO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORDENMANTENIMIENTO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORDENMANTENIMIENTO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORDENMANTENIMIENTO: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMINORDEN(long gSeqOrmIdOrden, Integer pIDEmpr, String cNroOrden, long gSeqOrmInResp, long lid_servicio, long lid_raiz_xml) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_IN_ORDEN;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_IN_ORDEN");
			jdbcORCLTemplate.update(sqlString,gSeqOrmIdOrden,pIDEmpr,cNroOrden,gSeqOrmInResp,lid_servicio,lid_raiz_xml);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_IN_ORDEN: {}",ex.getMessage());
			throw ex;
		}
	}

	@Transactional
	@Override
	public void insertORMINSERV(Integer pIDEmpr, String cObservacion, long gSeqOrmIdOrden) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_IN_SERV;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_IN_SERV");
			jdbcORCLTemplate.update(sqlString,pIDEmpr,cObservacion,gSeqOrmIdOrden);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_IN_SERV: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectCOUNT_ORMTAREAEJEC(long idTarea, long idOrden) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_COUNT_ORMTAREAEJEC;
			logger.debug("Executing query S4jMapper.SQL_SELECT_COUNT_ORMTAREAEJEC");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,idTarea,idOrden);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_COUNT_ORMTAREAEJEC: {}",ee.getMessage());
			throw ee;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_COUNT_ORMTAREAEJEC: {}",e.getMessage());
			throw e;
		}
	}

	//@Transactional
	@Override
	public void insertORMTAREAEJEC(Integer pIDEmpr, String cEjecutado, double dValor, int iCantidad, String cCambioMed,
			String cContraMed, String cCostoSis, String cCondTrabajo, long idTarea, long idOrden) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_TAREA_EJEC;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_TAREA_EJEC");
			jdbcORCLTemplate.update(sqlString,pIDEmpr,cEjecutado,dValor,iCantidad,cCambioMed,cContraMed,cCostoSis,cCondTrabajo,idTarea,idOrden);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_TAREA_EJEC: {}",ex.getMessage());
			throw ex;
		}
	}
	
	//@Transactional
	@Override
	public void updateORMTAREAEJEC(String cEjecutado, double dValor, String cCostoSis, String cCambioMed,
			String cContraMed, String cCondTrabajo, int iCantidad, long idTarea, long idOrden) {
		try {
			String sqlString = S4jMapper.SQL_UPDATE_ORM_TAREA_EJEC;
			logger.debug("Executing query S4jMapper.SQL_UPDATE_ORM_TAREA_EJEC");
			jdbcORCLTemplate.update(sqlString,cEjecutado,dValor,cCostoSis,cCambioMed,cContraMed,cCondTrabajo,iCantidad,idTarea,idOrden);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_UPDATE_ORM_TAREA_EJEC: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERCONDICIONTRABAJOORDENMA() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERCONDICIONTRABAJOORDENMA;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERCONDICIONTRABAJOORDENMA");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERCONDICIONTRABAJOORDENMA: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERCONDICIONTRABAJOORDENMA: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMINCONDTRAB(long lid_cond_trabajo, Integer pIDEmpr, String cCondTrabajo) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_IN_COND_TRAB;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_IN_COND_TRAB");
			jdbcORCLTemplate.update(sqlString,lid_cond_trabajo,pIDEmpr,cCondTrabajo);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_IN_COND_TRAB: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERTAREAORDENMANTENIMIENTO() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERTAREAORDENMANTENIMIENTO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERTAREAORDENMANTENIMIENTO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERTAREAORDENMANTENIMIENTO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERTAREAORDENMANTENIMIENTO: {}",e.getMessage());
			return 0;
		}
	}
	
	//@Transactional
	@Override
	public void insertORMINTAREA(long lid_tarea, Integer pIDEmpr, String cNroTarea) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_IN_TAREA;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_IN_TAREA");
			jdbcORCLTemplate.update(sqlString,lid_tarea,pIDEmpr,cNroTarea);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_IN_TAREA: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertORMINTAREAEJE(Integer pIDEmpr, String cEjecutado, int iCantidad, long lid_tarea,
			long lid_cond_trabajo, long gSeqOrmIdOrden) {
		try {
			String sqlString = S4jMapper.SQL_INSERT_ORM_IN_TAREA_EJE;
			logger.debug("Executing query S4jMapper.SQL_INSERT_ORM_IN_TAREA_EJE");
			jdbcORCLTemplate.update(sqlString,pIDEmpr,cEjecutado,iCantidad,lid_tarea,lid_cond_trabajo,gSeqOrmIdOrden);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_INSERT_ORM_IN_TAREA_EJE: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public int bfnValidarAIC(Long pIdOrden) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_VALIDAR_AIC;
			logger.debug("Executing query S4jMapper.SQL_SELECT_VALIDAR_AIC");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pIdOrden);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_VALIDAR_AIC: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_VALIDAR_AIC: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long getSEGUSUARIOID(long idOrden) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_GET_USUARIO_ID;
			logger.debug("Executing query S4jMapper.SQL_SELECT_GET_USUARIO_ID");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,idOrden);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_GET_USUARIO_ID: {}",ee.getMessage());
			return -1;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_GET_USUARIO_ID: {}",e.getMessage());
			return -1;
		}
	}

	@Override
	public long getSEGRESPONSABLEID(long idUserRespEtapa) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_GET_RESPONSABLE_ID;
			logger.debug("Executing query S4jMapper.SQL_SELECT_GET_RESPONSABLE_ID");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,idUserRespEtapa);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_GET_RESPONSABLE_ID: {}",ee.getMessage());
			return -1;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_GET_RESPONSABLE_ID: {}",e.getMessage());
			return -1;
		}
	}

	@Transactional
	@Override
	public void updateORDORDDERIV(long idResponsable, long idOrden) {
		try {
			String sqlString = S4jMapper.UPDATE_ORD_ORDER_DERIV;
			logger.debug("Executing query S4jMapper.UPDATE_ORD_ORDER_DERIV");
			jdbcORCLTemplate.update(sqlString,idResponsable,idOrden);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_ORD_ORDER_DERIV: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void updateORDORDDENFINAL(long idOrden) {
		try {
			String sqlString = S4jMapper.UPDATE_ORD_ORDEN_FINAL;
			logger.debug("Executing query S4jMapper.UPDATE_ORD_ORDEN_FINAL");
			jdbcORCLTemplate.update(sqlString,idOrden);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_ORD_ORDEN_FINAL: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void updateWKFWORKFLOWFINAL(long idWorkflow) {
		logger.info("idWorkflow: {}", idWorkflow);
		try {
			String sqlString = S4jMapper.UPDATE_WKFWORKFLOW_FINAL;
			logger.debug("Executing query S4jMapper.UPDATE_WKFWORKFLOW");
			jdbcORCLTemplate.update(sqlString,idWorkflow);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_WKFWORKFLOW: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertFWKAUDITEVENTFINAL(long id_audit_event, Integer gIdEmprGlob, long idOrden, Integer pIdUser) {
		try {
			String sqlString = S4jMapper.INSERT_FFWKAUDITEVENT_FINAL;
			logger.debug("Executing query S4jMapper.INSERT_FFWKAUDITEVENT_FINAL");
			jdbcORCLTemplate.update(sqlString,id_audit_event,gIdEmprGlob,idOrden,pIdUser);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_FFWKAUDITEVENT_FINAL: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertORDHISTORICOFINAL(long lid_auditevent, long idBuzon) {
		try {
			String sqlString = S4jMapper.INSERT_ORDHISTORICO_FINAL;
			logger.debug("Executing query S4jMapper.INSERT_ORDHISTORICO_FINAL");
			jdbcORCLTemplate.update(sqlString,lid_auditevent,idBuzon);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_ORDHISTORICO_FINAL: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long bfnExisteMarcaMedidor2(String chMarcaMedidorInst) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_BFN_EXISTE_MARCA_MEDIDOR_2;
			logger.debug("Executing query S4jMapper.SQL_SELECT_BFN_EXISTE_MARCA_MEDIDOR_2");
			return jdbcPostgreTemplate.queryForObject(sqlString,Long.class,chMarcaMedidorInst);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_BFN_EXISTE_MARCA_MEDIDOR_2: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_BFN_EXISTE_MARCA_MEDIDOR_2: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long bfnExisteModeloMedidor2(long pidMedMarca, String pCodModeloMed) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_BFN_EXISTE_MODELO_MEDIDOR_2;
			logger.debug("Executing query S4jMapper.SQL_SELECT_BFN_EXISTE_MODELO_MEDIDOR_2");
			return jdbcPostgreTemplate.queryForObject(sqlString,Long.class,pidMedMarca,pCodModeloMed);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_BFN_EXISTE_MODELO_MEDIDOR_2: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_BFN_EXISTE_MODELO_MEDIDOR_2: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> selectDataInNroComponente(String chMedidorInst, long idMedModeloInst, Integer pIDEmpr) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = S4jMapper.SQL_SELECT_DATA_IN_NRO_COMPONENTE;
			logger.debug("Executing query S4jMapper.SQL_SELECT_DATA_IN_NRO_COMPONENTE");
			return result= jdbcPostgreTemplate.queryForMap(sqlString,chMedidorInst,idMedModeloInst);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_NRO_COMPONENTE: {}",ee.getMessage());
			throw ee;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_NRO_COMPONENTE: {}",e.getMessage());
			throw e;
		}
	}

	//@Transactional
	@Override
	public void insertORDMEDICAMBIO(long idComponenteInst, long idOrden, Integer pIDEmpr) {
		try {
			String sqlString = S4jMapper.INSERT_ORD_MEDI_CAMBIO;
			logger.debug("Executing query S4jMapper.INSERT_ORD_MEDI_CAMBIO");
			jdbcORCLTemplate.update(sqlString,idComponenteInst,idOrden,pIDEmpr);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_ORD_MEDI_CAMBIO: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERPROPIEDADCOMPONENTE() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERPROPIEDADCOMPONENTE;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERPROPIEDADCOMPONENTE");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERPROPIEDADCOMPONENTE: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERPROPIEDADCOMPONENTE: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMINPROPCOMP(long lid_prop_comp, Integer pIDEmpr, String cCodPropMedInst) {
		try {
			String sqlString = S4jMapper.INSERT_ORD_IN_PROP_COMP;
			logger.debug("Executing query S4jMapper.INSERT_ORD_IN_PROP_COMP");
			jdbcORCLTemplate.update(sqlString,lid_prop_comp,pIDEmpr,cCodPropMedInst);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_ORD_IN_PROP_COMP: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERORMMEDIDORINSTALACION() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORINSTALACION;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORINSTALACION");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORINSTALACION: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORINSTALACION: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMINMEDINST(long lid_medidor_instalacion, Integer pIDEmpr, String chMedidorInst,
			String chModeloMedidorInst, String chMarcaMedidorInst, long gSeqOrmInResp, long lid_prop_comp) {
		try {
			String sqlString = S4jMapper.INSERT_ORM_IN_MED_INST;
			logger.debug("Executing query S4jMapper.INSERT_ORM_IN_MED_INST");
			jdbcORCLTemplate.update(sqlString,lid_medidor_instalacion,pIDEmpr,chMedidorInst,chModeloMedidorInst,chMarcaMedidorInst,gSeqOrmInResp,lid_prop_comp);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_ORM_IN_MED_INST: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertORMINMEDIDA(Integer pIDEmpr, String vcCodigo, double vdValor, long lid_medidor_instalacion) {
		try {
			String sqlString = S4jMapper.INSERT_ORM_IN_MEDIA;
			logger.debug("Executing query S4jMapper.INSERT_ORM_IN_MED_INST");
			jdbcORCLTemplate.update(sqlString,pIDEmpr,vcCodigo,vdValor,lid_medidor_instalacion);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_ORM_IN_MED_INST: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public int bfnExisteNumeroMedidor(String pNroMedidor, long pidMedModelo, Integer pIDEmpresa, long pIDServicio,
			Integer pOpc) {
		String sqlString ="";
		try {
			if(pOpc==1){
				sqlString = S4jMapper.SQL_SELECT_EXISTE_NUMERO_MEDIDOR;
				logger.debug("Executing query S4jMapper.SQL_SELECT_EXISTE_NUMERO_MEDIDOR");
				return jdbcPostgreTemplate.queryForObject(sqlString,Integer.class,pNroMedidor,pidMedModelo,pIDServicio);
			}else{
				sqlString = S4jMapper.SQL_SELECT_EXISTE_NUMERO_MEDIDOR2;
				logger.debug("Executing query S4jMapper.SQL_SELECT_EXISTE_NUMERO_MEDIDOR2");
				return jdbcPostgreTemplate.queryForObject(sqlString,Integer.class,pNroMedidor,pidMedModelo);
			}
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXISTE_NUMERO_MEDIDORX: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXISTE_NUMERO_MEDIDORX: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> selectDataInMedComponente(String chMedidorRet, long idMedModeloRet, Integer pIDEmpr,
			long gIdServicio) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE;
			logger.debug("Executing query S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE");
			return result= jdbcPostgreTemplate.queryForMap(sqlString,chMedidorRet,idMedModeloRet,gIdServicio);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE: {}",ee.getMessage());
			throw ee;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE: {}",e.getMessage());
			throw e;
		}
	}

	//@Transactional
	@Override
	public void insertORDMEDICAMBIO2(long idComponenteRet, long idOrden, Integer pIDEmpr) {
		try {
			String sqlString = S4jMapper.INSERT_ORM_IN_MEDIA_CAMBIO_2;
			logger.debug("Executing query S4jMapper.INSERT_ORM_IN_MED_INST");
			jdbcORCLTemplate.update(sqlString,idComponenteRet,idOrden,pIDEmpr);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_ORM_IN_MED_INST: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectNextValSQINNERORMMEDIDORRETIRO() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORRETIRO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORRETIRO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORRETIRO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQINNERORMMEDIDORRETIRO: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void insertORMINMEDRETIRO(long gSeqOrmInMedRet, Integer pIDEmpr, String chMedidorRet,
			String chModeloMedidorRet, String chMarcaMedidorRet, long gSeqOrmInResp) {
		try {
			String sqlString = S4jMapper.INSERT_ORM_IN_MED_RETIRO;
			logger.debug("Executing query S4jMapper.INSERT_ORM_IN_MED_RETIRO");
			jdbcORCLTemplate.update(sqlString,gSeqOrmInMedRet,pIDEmpr,chMedidorRet,chModeloMedidorRet,chMarcaMedidorRet,gSeqOrmInResp);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_ORM_IN_MED_RETIRO: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertORMINMEDIDA2(Integer pIDEmpr, String vcCodigo, double vdValor, long gSeqOrmInMedRet) {
		try {
			String sqlString = S4jMapper.INSERT_ORM_IN_MEDIDA_2;
			logger.debug("Executing query S4jMapper.INSERT_ORM_IN_MEDIDA_2");
			jdbcORCLTemplate.update(sqlString,pIDEmpr,vcCodigo,vdValor,gSeqOrmInMedRet);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_ORM_IN_MEDIDA_2: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long getIdComponenteFromMedComponente(String pNroMedidor, long pidMedModelo, Integer pIDEmpresa,long pIDServicio) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE;
			logger.debug("Executing query S4jMapper.SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE");
			return jdbcPostgreTemplate.queryForObject(sqlString,Long.class,pNroMedidor,pidMedModelo,pIDServicio);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long getIdComponenteFromMedComponente(String pNroMedidor, long pidMedModelo, Integer pIDEmpresa) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE2;
			logger.debug("Executing query S4jMapper.SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE2");
			return jdbcPostgreTemplate.queryForObject(sqlString,Long.class,pNroMedidor,pidMedModelo);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE2: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_ID_COMPONENTE_FROM_MED_COMPONENTE2: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public List<Map<String, Object>> selectCursorqLect2(long idTmpComponente) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = S4jMapper.SQL_SELECT_CURSOR_QLECT2;
			logger.debug("Executing query S4jMapper.SQL_SELECT_CURSOR_QLECT2");
			result= jdbcPostgreTemplate.queryForList(sqlString,idTmpComponente);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_CURSOR_QLECT2: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_CURSOR_QLECT2: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public long extractIdEmpresaGlobal() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_EXTRACT_ID_EMPRESA_GLOBAL;
			logger.debug("Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_EMPRESA_GLOBAL");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_EMPRESA_GLOBAL: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_EMPRESA_GLOBAL: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long extractIdMotivoEstado() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO: {}",e.getMessage());
			return 0;
		}
	}
	
	@Override
	public Map<String, Object> selectDataInMedComponente2(String cNroMedRet, long pidMedModelo, Integer pIDEmpr,
			long pIDServicio) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE_2;
			logger.debug("Executing query S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE_2");
			return jdbcPostgreTemplate.queryForMap(sqlString,cNroMedRet,pidMedModelo,pIDServicio);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE_2: {}",ee.getMessage());
			throw ee;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE_2: {}",e.getMessage());
			throw e;
		}
	}

	@Override
	public long selectNextValSQDYNAMICBUSINESSOBJECT() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_NEXTVAL_SQDYNAMICBUSINESSOBJECT;
			logger.debug("Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQDYNAMICBUSINESSOBJECT");
			return jdbcPostgreTemplate.queryForObject(sqlString,Long.class);  // R.I. Correctivo 09/11/2023
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQDYNAMICBUSINESSOBJECT: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_NEXTVAL_SQDYNAMICBUSINESSOBJECT: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public void insertDYOOBJECT(long idObject, Integer pIDEmpr) {
		try {
			String sqlString = S4jMapper.INSERT_DYO_OBJECT;
			logger.debug("Executing query S4jMapper.INSERT_DYO_OBJECT");
			jdbcPostgreTemplate.update(sqlString,idObject);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_DYO_OBJECT: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void updateMEDCOMPONENTE(long idDynamicObject, Integer medPropiedadCliente, long id_cliente, long id_contratista,
			long idObject, String gNroMedRet, long pidMedModelo, Integer pIDEmpr, long pIDServicio) {
		
		String sqlString="";
		try {
			if(idDynamicObject==-1){
				sqlString = S4jMapper.UPDATE_MED_COMPONENTE2;
				logger.debug("Executing query S4jMapper.UPDATE_MED_COMPONENTE2");
				jdbcPostgreTemplate.update(sqlString,medPropiedadCliente,id_cliente,id_contratista,medPropiedadCliente,idObject,gNroMedRet,pidMedModelo,pIDServicio);
			}else{
				sqlString = S4jMapper.UPDATE_MED_COMPONENTE1;
				logger.debug("Executing query S4jMapper.UPDATE_MED_COMPONENTE1");
				jdbcPostgreTemplate.update(sqlString,medPropiedadCliente,id_cliente,id_contratista,medPropiedadCliente,gNroMedRet,pidMedModelo,pIDServicio);
			}
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_MED_COMPONENTE: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectMAXMedHisComponete(long idComponente) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_MAX_MED_HIS_COMPONENTE;
			logger.debug("Executing query S4jMapper.SQL_SELECT_MAX_MED_HIS_COMPONENTE");
			return jdbcPostgreTemplate.queryForObject(sqlString,Long.class,idComponente);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_MAX_MED_HIS_COMPONENTE: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_MAX_MED_HIS_COMPONENTE: {}",e.getMessage());
			return 0;
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_MAX_MED_HIS_COMPONENTE: {}",ex.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void updateMEDHISPONENTE(long idHisComponente, long idOrden) {
		try {
			String sqlString = S4jMapper.UPDATE_MED_HIS_COMPONENT;
			logger.debug("Executing query S4jMapper.UPDATE_MED_HIS_COMPONENT");
			jdbcPostgreTemplate.update(sqlString,idOrden,idHisComponente);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_MED_HIS_COMPONENT: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertMEDHISCOMPONENTE(long idHisComp, long idComponente, long idPropiedad, Integer medPropiedadCliente,
			long id_cliente, long id_contratista, long idOrden) {
		String sqlString="";
		try {
			if(idPropiedad==medPropiedadCliente){
				sqlString = S4jMapper.INSERT_MED_HIS_COMPONENTE2;
				logger.debug("Executing query S4jMapper.INSERT_MED_HIS_COMPONENTE2");
				jdbcPostgreTemplate.update(sqlString,idHisComp,idComponente,id_cliente, idOrden);
			}else{
				sqlString = S4jMapper.INSERT_MED_HIS_COMPONENTE1;
				logger.debug("Executing query S4jMapper.INSERT_MED_HIS_COMPONENTE1");
				jdbcPostgreTemplate.update(sqlString,idHisComp,idComponente,id_contratista, idOrden);
			}
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.insertMEDHISCOMPONENTE: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertFWKAUDITEVENT(long idHisComp, long idComponente, Integer pIdUser) {
		// cambio cambio
		try {
			String sqlString = S4jMapper.INSERT_FWK_AUDITEVENT;
			logger.debug("Executing query S4jMapper.INSERT_FWK_AUDITEVENT");
			jdbcPostgreTemplate.update(sqlString,idHisComp,idComponente,pIdUser);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_FWK_AUDITEVENT: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long extractIdMedTipMagnitud() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_EXTRACT_ID_MED_TIP_MAGNITUD;
			logger.debug("Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MED_TIP_MAGNITUD");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MED_TIP_MAGNITUD: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MED_TIP_MAGNITUD: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long extractIdFactEstMagnitud() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_EXTRACT_ID_FACT_ES_MAGNITUD;
			logger.debug("Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_FACT_ES_MAGNITUD");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_FACT_ES_MAGNITUD: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_FACT_ES_MAGNITUD: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> selectDataInMoreTables(long idComponente, String vcCodigo) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_DATA_IN_MORE_TABLES;
			logger.debug("Executing query S4jMapper.SQL_SELECT_DATA_IN_MORE_TABLES");
			return jdbcPostgreTemplate.queryForMap(sqlString,idComponente,vcCodigo);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_MORE_TABLES: {}",ee.getMessage());
			throw ee;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_MORE_TABLES: {}",e.getMessage());
			throw e;
		}
	}

	@Override
	public Map<String, Object> selectDataInNucCuentaServicio(long lNroCuenta) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_DATA_IN_NUC_CUENTA_SERVICIO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_DATA_IN_NUC_CUENTA_SERVICIO");
			return jdbcORCLTemplate.queryForMap(sqlString,lNroCuenta);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_NUC_CUENTA_SERVICIO: {}",ee.getMessage());
			throw ee;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_NUC_CUENTA_SERVICIO: {}",e.getMessage());
			throw e;
		}
	}

	//@Transactional
	@Override
	public void updateSRV_Electrico(long vIdSecMagnitud, long xid_servicio) {
		try {
			String sqlString = S4jMapper.UPDATE_SRV_ElECTRICO;
			logger.debug("Executing query S4jMapper.UPDATE_SRV_Electrico");
			jdbcORCLTemplate.update(sqlString,vIdSecMagnitud,xid_servicio);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_SRV_Electrico: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertMEDMAGNITUD(long idMagnitud, Integer pIDEmpr, long idComponente, long pIDServicio, long vIdMedida, double vdValor,
			double vIdFactor, long vIdTipMagnitud, String cFechaLectura, long vIdTipCalculo, long vIdEstMagnitud,
			long vIdTipo_cons, long vIdSecMagnitud, long vICantEnteros, long vICantDecimales) {
		try {
			String sqlString = S4jMapper.INSERT_MED_MAGNITUD;
			logger.debug("Executing query S4jMapper.INSERT_MED_MAGNITUD");
			jdbcORCLTemplate.update(sqlString,idMagnitud,pIDEmpr,idComponente,pIDServicio,vIdMedida,vdValor,vIdFactor,vIdTipMagnitud,cFechaLectura,vIdTipCalculo,vIdEstMagnitud,vIdTipo_cons
					,vIdSecMagnitud,vICantEnteros,vICantDecimales);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_MED_MAGNITUD: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void deleteMEDLECULTIMA(long idComponente, long vIdMedida) {
		try {
			String sqlString = S4jMapper.DELETE_MED_LEC_ULTIMA;
			logger.debug("Executing query S4jMapper.DELETE_MED_LEC_ULTIMA");
			jdbcPostgreTemplate.update(sqlString,idComponente,vIdMedida);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.DELETE_MED_LEC_ULTIMA: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertMEDLECULTIMA(long idLectura, Integer pIDEmpr, long vIdMedida, long idComponente) {
		try {
			String sqlString = S4jMapper.INSERT_MED_LEC_ULTIMA;
			logger.debug("Executing query S4jMapper.INSERT_MED_LEC_ULTIMA");
			jdbcPostgreTemplate.update(sqlString,idLectura,pIDEmpr,vIdMedida,idComponente);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_MED_LEC_ULTIMA: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void updateSELSELLO(long idMotivoEstado, String cCodEjecutor, long idComponente) {
		try {
			String sqlString = S4jMapper.UPDATE_SEL_SELLO;
			logger.debug("Executing query S4jMapper.UPDATE_SEL_SELLO");
			jdbcORCLTemplate.update(sqlString,idMotivoEstado,cCodEjecutor,idComponente);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_SEL_SELLO: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long extractIdServicioInstalado(long lNroCuenta) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_EXTRACT_ID_SERVICIO_INSTALADO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_SERVICIO_INSTALADO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,lNroCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_SERVICIO_INSTALADO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_SERVICIO_INSTALADO: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long extractIdMotivoEstadoInstalado() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO_INSTALADO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO_INSTALADO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO_INSTALADO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MOTIVO_ESTADO_INSTALADO: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> selectDataInMedComponenteInstalar(String cNroMedInst, long pidMedModelo,
			Integer pIDEmpr) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE_INSTALAR;
			logger.debug("Executing query S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE_INSTALAR");
			return jdbcPostgreTemplate.queryForMap(sqlString,cNroMedInst,pidMedModelo);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE_INSTALAR: {}",ee.getMessage());
			throw ee;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_MED_COMPONENTE_INSTALAR: {}",e.getMessage());
			throw e;
		}
	}
	
	//@Transactional
	@Override
	public void updateMEDCOMPONENTE_Instalar(long idDynamicObject, long lNroCuenta, String gCodPropMedInst, String gFechaLectura,
			long idObject, String gNroMedInst, long pidMedModelo) {
		String sqlString="";
		try {
			if(idDynamicObject==-1){
				sqlString = S4jMapper.UPDATE_MED_COMPONENTE_INSTALAR1;
				logger.debug("Executing query S4jMapper.UPDATE_MED_COMPONENTE_INSTALAR1");
				jdbcPostgreTemplate.update(sqlString, lNroCuenta, gCodPropMedInst, gFechaLectura, idObject, gNroMedInst, pidMedModelo);
			}else{
				sqlString = S4jMapper.UPDATE_MED_COMPONENTE_INSTALAR2;
				logger.debug("Executing query S4jMapper.UPDATE_MED_COMPONENTE_INSTALAR1");
				jdbcPostgreTemplate.update(sqlString, lNroCuenta, gCodPropMedInst, gFechaLectura, gNroMedInst, pidMedModelo);
			}
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_MED_COMPONENTE: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long selectMAXMedHisComponente_Inst(long idComponente) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_MAX_MED_HIS_COMPONENTE_INST;
			logger.debug("Executing query S4jMapper.SQL_SELECT_MAX_MED_HIS_COMPONENTE_INST");
			return jdbcPostgreTemplate.queryForObject(sqlString,Long.class,idComponente);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_MAX_MED_HIS_COMPONENTE_INST: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_MAX_MED_HIS_COMPONENTE_INST: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void updateMEDHISPONENTE_INST(String cFechaLectura,long idHisComponente, long idOrden) {
		try {
			String sqlString = S4jMapper.UPDATE_MED_HIS_COMPONENT_INST;
			logger.debug("Executing query S4jMapper.UPDATE_MED_HIS_COMPONENT_INST");
			jdbcPostgreTemplate.update(sqlString,cFechaLectura,idOrden,idHisComponente);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_MED_HIS_COMPONENT_INST: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertMEDHISCOMPONENTE_INST(long idHisComp, long idComponente, String gFechaLectura, long idUbiServicio, long idOrden) {
		try {
			String sqlString = S4jMapper.INSERT_MED_HIS_COMPONENT_INST;
			logger.debug("Executing query S4jMapper.INSERT_MED_HIS_COMPONENT_INST");
			jdbcPostgreTemplate.update(sqlString,idHisComp,idComponente,gFechaLectura,idUbiServicio, idOrden);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_MED_HIS_COMPONENT_INST: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertFWKAUDITEVENT_INST(long idHisComp, long idComponente, Integer pIdUser) {
		// cambio cambio
		try {
			String sqlString = S4jMapper.INSERT_FWK_AUDITEVENT_INST;
			logger.debug("Executing query S4jMapper.INSERT_FWK_AUDITEVENT_INST");
			jdbcPostgreTemplate.update(sqlString,idHisComp,idComponente,pIdUser);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_FWK_AUDITEVENT_INST: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public long extractIdMedTipoMagnitud_INST() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_EXTRACT_ID_MED_TIPO_MAGNITUD_INST;
			logger.debug("Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MED_TIPO_MAGNITUD_INST");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MED_TIPO_MAGNITUD_INST: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_MED_TIPO_MAGNITUD_INST: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long extractIdFacEstMagnitud_INST() {
		try {
			String sqlString = S4jMapper.SQL_SELECT_EXTRACT_ID_FAC_EST_MAGNITUD_INST;
			logger.debug("Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_FAC_EST_MAGNITUD_INST");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_FAC_EST_MAGNITUD_INST: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_FAC_EST_MAGNITUD_INST: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> selectDataInSELCOLOR(String gColSello1) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_DATA_IN_SEL_COLOR;
			logger.debug("Executing query S4jMapper.SQL_SELECT_DATA_IN_SEL_COLOR");
			return jdbcORCLTemplate.queryForMap(sqlString,gColSello1);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_SEL_COLOR: {}",ee.getMessage());
			throw ee;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_DATA_IN_SEL_COLOR: {}",e.getMessage());
			throw e;
		}
	}
	
	@Override
	public long extractIdSelSello(long idNroSello1) {
		try {
			String sqlString = S4jMapper.SQL_SELECT_EXTRACT_ID_SELLO;
			logger.debug("Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_SELLO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,idNroSello1);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_SELLO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_SELECT_EXTRACT_ID_SELLO: {}",e.getMessage());
			return 0;
		}
	}

	//@Transactional
	@Override
	public void updateSELSELLO(long idMotivoEstado,String cUbiSello1, long idUbiServicio, long idComponente, String cCodEjecutor,
			long idNroSello1, long idBolsaSello1) {
		try {
			String sqlString = S4jMapper.UPDATE_SEL_SELLO_INST;
			logger.debug("Executing query S4jMapper.UPDATE_SEL_SELLO_INST");
			jdbcORCLTemplate.update(sqlString,idMotivoEstado,cUbiSello1,idUbiServicio,idComponente,cCodEjecutor,idNroSello1,idBolsaSello1);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_SEL_SELLO_INST: {}",ex.getMessage());
			throw ex;
		}
	}

	//@Transactional
	@Override
	public void insertSELHISSELLO(Integer pIDEmpr, long idBolsaSello1, String cDesSello1, long idNroSello1,
			String cCodContratista, String gCodEjecutor, String cUbiSello1, long lNroCuenta, String gCodMarMedInst, String gCodModMedInst,
			 long idColor1) {
		try {
			String sqlString = S4jMapper.INSERT_SEL_HIS_SELLO;
			logger.debug("Executing query S4jMapper.INSERT_SEL_HIS_SELLO");
			jdbcORCLTemplate.update(sqlString,pIDEmpr,idBolsaSello1,cDesSello1,idNroSello1,cCodContratista,gCodEjecutor,cUbiSello1,lNroCuenta,gCodMarMedInst,gCodModMedInst,gCodMarMedInst,
					idNroSello1,idColor1,idBolsaSello1);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_SEL_HIS_SELLO: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public int selectCOUNTSELSELLO(long idBolsaSello1) {
		try {
			String sqlString = S4jMapper.SQL_COUNT_SEL_SELLO;
			logger.debug("Executing query S4jMapper.SQL_COUNT_SEL_SELLO");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,idBolsaSello1);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_COUNT_SEL_SELLO: {}",ee.getMessage());
			return -1;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_COUNT_SEL_SELLO: {}",e.getMessage());
			return -1;
		}
	}

	//@Transactional
	@Override
	public void updateSELBOLSASELLOS(String cCodEjecutor, long idBolsaSello1) {
		try {
			String sqlString = S4jMapper.UPDATE_SEL_BOLSA_SELLOS;
			logger.debug("Executing query S4jMapper.UPDATE_SEL_BOLSA_SELLOS");
			jdbcORCLTemplate.update(sqlString,cCodEjecutor,idBolsaSello1);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.UPDATE_SEL_BOLSA_SELLOS: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public void insertSELHISSELLO2(Integer pIDEmpr, long idBolsaSello2, String cDesSello2, long idNroSello2,
			String cCodContratista, String gCodEjecutor, String cUbiSello2, long lNroCuenta, String gCodMarMedInst, String gCodModMedInst,
			long idColor2) {
		try {
			String sqlString = S4jMapper.INSERT_SEL_HIS_SELLO2;
			logger.debug("Executing query S4jMapper.INSERT_SEL_HIS_SELLO2");
			jdbcORCLTemplate.update(sqlString,pIDEmpr,idBolsaSello2,cDesSello2,idNroSello2,cCodContratista,gCodEjecutor,cUbiSello2,lNroCuenta,gCodMarMedInst,
					gCodModMedInst,gCodMarMedInst,idNroSello2,idColor2,idBolsaSello2);
		} catch (Exception ex) {
			logger.error("Error en Executing query S4jMapper.INSERT_SEL_HIS_SELLO2: {}",ex.getMessage());
			throw ex;
		}
	}

	@Override
	public int bfnExisteCodigoLectura(String pCodigo, Integer pIDEmpr) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_EXISTE_CODIGO_LECTURA;
			logger.debug("Executing query S4jMapper.QUERY_BFN_EXISTE_CODIGO_LECTURA");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pCodigo,pIDEmpr);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_CODIGO_LECTURA: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_CODIGO_LECTURA: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public int bfnExisteCuenta(Integer pIDEmpr, Long pCuenta) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_EXISTE_CUENTA;
			logger.debug("Executing query S4jMapper.QUERY_BFN_EXISTE_CUENTA");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pIDEmpr,pCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_CUENTA: {}",ee.getMessage());
			return 1;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_CUENTA: {}",e.getMessage());
			return 1;
		}
	}

	@Override
	public int bfnCuentaEnFacturacion(Long pCuenta) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_CUENTA_EN_FACTURACION;
			logger.debug("Executing query S4jMapper.QUERY_BFN_CUENTA_EN_FACTURACION");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_CUENTA_EN_FACTURACION: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_CUENTA_EN_FACTURACION: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> bfnTieneServicioElectrico(Long pCuenta) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_TIENE_SERVICIO_ELECTRICO;
			logger.debug("Executing query S4jMapper.QUERY_BFN_TIENE_SERVICIO_ELECTRICO");
			return jdbcORCLTemplate.queryForMap(sqlString,pCuenta);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_TIENE_SERVICIO_ELECTRICO: {}",ee.getMessage());
			throw ee;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_TIENE_SERVICIO_ELECTRICO: {}",e.getMessage());
			throw e;
		}
	}

	@Override
	public int bfnSE_VerificaVenta(Long pCuenta) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_SE_VERIFICA_VENTA;
			logger.debug("Executing query S4jMapper.QUERY_BFN_SE_VERIFICA_VENTA");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_SE_VERIFICA_VENTA: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_SE_VERIFICA_VENTA: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public int verifyVentaPorModificaciónEjecutado(Long pCuenta) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_VENTA_POR_MODIFICACION;
			logger.debug("Executing query S4jMapper.QUERY_BFN_VENTA_POR_MODIFICACION");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_VENTA_POR_MODIFICACION: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_VENTA_POR_MODIFICACION: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public int bfnExisteContratista(String pCodContra, Integer pIDEmpresa) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_EXISTE_CONTRATISTA;
			logger.debug("Executing query S4jMapper.QUERY_BFN_EXISTE_CONTRATISTA");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pCodContra,pIDEmpresa);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_CONTRATISTA: {}",ee.getMessage());
			return 1;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_CONTRATISTA: {}",e.getMessage());
			return 1;
		}
	}

	@Override
	public int bfnExisteEjecutor(String pCodEjecutor, Integer pIDEmpr) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_EXISTE_EJECUTOR;
			logger.debug("Executing query S4jMapper.QUERY_BFN_EXISTE_EJECUTOR");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pCodEjecutor,pIDEmpr);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_EJECUTOR: {}",ee.getMessage());
			return 1;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_EJECUTOR: {}",e.getMessage());
			return 1;
		}
	}

	@Override
	public String carga_FechaHora1() {
		try 
		{
        	String sqlString = S4jMapper.SQL_SELECT_CARGA_FECHA_1;
			logger.debug("Executing query OthersMapper.SQL_SELECT_CARGA_FECHA_1");
        	return jdbcORCLTemplate.queryForObject(sqlString, String.class);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  carga_FechaHora1: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  carga_FechaHora1: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public String carga_FechaHora2() {
		try 
		{
        	String sqlString = S4jMapper.SQL_SELECT_CARGA_FECHA_2;
			logger.debug("Executing query OthersMapper.SQL_SELECT_CARGA_FECHA_2");
        	return jdbcORCLTemplate.queryForObject(sqlString, String.class);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("Error en  carga_FechaHora2: {}",ee.getMessage());
            throw ee;
        } 
		catch (DataAccessException e) {
            logger.error("Error en  carga_FechaHora2: {}",e.getMessage());
            throw e;
        }
	}

	@Override
	public String bfnObtenerFechaEjecucionVta(Long pCuenta) {
		try {
			String sqlString = S4jMapper.SQL_BFN_OBTENER_FECHA_EJECUCION_VTA;
			logger.debug("Executing query S4jMapper.SQL_BFN_OBTENER_FECHA_EJECUCION_VTA");
			return jdbcORCLTemplate.queryForObject(sqlString,String.class,pCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.SQL_BFN_OBTENER_FECHA_EJECUCION_VTA: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.SQL_BFN_OBTENER_FECHA_EJECUCION_VTA: {}",e.getMessage());
			return "";
		}
	}

	@Override
	public int bfnExisteCodigoPropiedadMedidor(String pCodPropMed) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_EXISTE_CODIGO_PROPIEDAD_MEDIDOR;
			logger.debug("Executing query S4jMapper.QUERY_BFN_EXISTE_CODIGO_PROPIEDAD_MEDIDOR");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pCodPropMed);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_CODIGO_PROPIEDAD_MEDIDOR: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_CODIGO_PROPIEDAD_MEDIDOR: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public int bfnExisteNroSello(Long pNroSello) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_EXISTE_NRO_SELLO;
			logger.debug("Executing query S4jMapper.QUERY_BFN_EXISTE_NRO_SELLO");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pNroSello);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_NRO_SELLO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_NRO_SELLO: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public int bfnExisteColorSello(String pColorSello) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_EXISTE_COLOR_SELLO;
			logger.debug("Executing query S4jMapper.QUERY_BFN_EXISTE_COLOR_SELLO");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pColorSello);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_COLOR_SELLO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_COLOR_SELLO: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public int bfnExisteUbicacionSello(String pUbiSello) {
		try {
			String sqlString = S4jMapper.QUERY_BFN_EXISTE_UBICACION_SELLO;
			logger.debug("Executing query S4jMapper.QUERY_BFN_EXISTE_UBICACION_SELLO");
			return jdbcORCLTemplate.queryForObject(sqlString,Integer.class,pUbiSello);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_UBICACION_SELLO: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.QUERY_BFN_EXISTE_UBICACION_SELLO: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long getIdCliente(Long lNroCuenta) {
		try {
			String sqlString = S4jMapper.GET_ID_CLIENTE;
			logger.debug("Executing query S4jMapper.GET_ID_CLIENTE");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,lNroCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.GET_ID_CLIENTE: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.GET_ID_CLIENTE: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public long getIdContratista(String gCodContratista) {
		try {
			String sqlString = S4jMapper.GET_ID_CONTRATISTA;
			logger.debug("Executing query S4jMapper.QUERY_BFN_EXISTE_COLOR_SELLO");
			return jdbcORCLTemplate.queryForObject(sqlString,Long.class,gCodContratista);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query S4jMapper.GET_ID_CONTRATISTA: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query S4jMapper.GET_ID_CONTRATISTA: {}",e.getMessage());
			return 0;
		}
	}
	
	// INICIO - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 
	
		@Override
		public List<Map<String, Object>> selectRegistroMedidores(String nro_orden_legacy) {
			List<Map<String, Object>> result = new ArrayList<>();
			try {
				String sqlString = S4jMapper.SQLORACLE_SELECT_REGISTRO_MEDIDORES;
				logger.debug("Executing query SCOMMapper.SQLPOSTGRES_SELECT_REGISTRO_MEDIDORES");
				result = jdbcPostgreTemplate.queryForList(sqlString, nro_orden_legacy);
			} catch (EmptyResultDataAccessException ee) {
				logger.error("Error en Executing query SCOMMapper.SQLPOSTGRES_SELECT_REGISTRO_MEDIDORES: {}",
						ee.getMessage());
				throw ee;
			} catch (DataAccessException e) {
				logger.error("Error en Executing query SCOMMapper.SQLPOSTGRES_SELECT_REGISTRO_MEDIDORES: {}",
						e.getMessage());
				throw e;
			}
			return result;
		}

		@Transactional
		@Override
		public void updateRegistroMedidores(String medidorXml, String modeloXml, String marcaXml, String factorXml) {
			int rows = 0;
			try {
				String sqlString = S4jMapper.SQLORACLE_ACTUALIZAR_MEDIDORES;
				logger.debug("Executing query SCOMMapper.SQLPOSTGRES_ACTUALIZAR_MEDIDORES");
				rows = jdbcPostgreTemplate.update(sqlString,  medidorXml, modeloXml, marcaXml,
					 	modeloXml, marcaXml, factorXml, 
					 	medidorXml, modeloXml, marcaXml, 
						modeloXml, marcaXml, factorXml);
				logger.info("Método bfnActualizaNroOrdenLegacy, filas afectadas: {}", rows);
			} catch (Exception ex) {
				logger.error("Error en Executing query SCOMMapper.SQLPOSTGRES_ACTUALIZAR_MEDIDORES: {}", ex.getMessage());
				throw ex;
			}
		}
		
		@Override
		public String selectMedidorEncontrado(String v_modelo, String v_marca, String v_componente) {
			String result = "";
			try {
				String sqlString = S4jMapper.SQLORACLE_SELECT_MEDIDOR_ENCONTRADO;
				logger.debug("Executing query SCOMMapper.SQLORACLE_SELECT_MEDIDOR_ENCONTRADO");
				result = jdbcPostgreTemplate.queryForObject(sqlString, String.class, v_modelo,v_marca,v_componente  );
			} catch (EmptyResultDataAccessException ee) {
				logger.error("Error en Executing query SCOMMapper.SQLORACLE_SELECT_MEDIDOR_ENCONTRADO: {}",
						ee.getMessage());
				throw ee;
			} catch (DataAccessException e) {
				logger.error("Error en Executing query SCOMMapper.SQLORACLE_SELECT_MEDIDOR_ENCONTRADO: {}",
						e.getMessage());
				throw e;
			}
			return result;
		}
		
		// FIN - REQ 10 - Recepción de Ordenes de Mantenimiento Plan de Mantenimiento - JEGALARZA 
	
	
	
}
